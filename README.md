# eo-cohda

EO-COHDA stands for "**E**nergy Storage **O**ptimization for **COHDA**" and contains several algorithms for generating multi criteria optimized schedules for integrating storages with wide sets of objectives in a COHDA negotiation. Furthermore the repository provides an implementation of the integration into COHDA itself, designed as multi-process optimization with aiomas as agent framework and ISAAC as COHDA variant.


# Installation

The following command have to be executed for installing the project.

```bash
pip install pipenv
pipenv install
```

After that you can execute the script you like via:

```bash
pipenv run python3.7 $THE_SCRIPT_TO_EXECUTE.py
```

# Usage

In the project are several scripts for analyzing the distributed schedule optimization problem and for executing and evaluating the distributed optimization + the metaheuristic algorithms. 

* experiments/*.py: Scripts for analyzing the solution space and evaluation 
* experiments/evaluation/*.py: Scripts for evaluating the metaheuristic algorithsm
* eocohda/evaluation/**/*.py: Scripts for evaluation the distributed optimization including the metaheuristic algorithms


# License

The code is distributed under the MIT license.


# Credits

Thanks to OFFIS (DAI-group) for providing the COHDA reference code "ISAAC": https://github.com/mtroeschel/isaac.