#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='EO-COHDA',
      version='1.0',
      description='Algorithms for distributed optimization of energy storages for COHDA',
      author='Rico Schrage',
      author_email='rico.schrage@uni-oldenburg.de',
      url='...',
      packages=find_packages(where='.'))
