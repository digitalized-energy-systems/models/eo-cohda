from pathlib import Path

import numpy as np
import skimage.feature as ski 
from matplotlib import cm 
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import eocohda.core.util as util

def main():

    planning_horizon = 10
    width = 100
    height = 100

    plt.rcParams.update({'font.size': 14})

    # Plain Heatmaü
    Y, X = np.meshgrid(np.linspace(0, 1, width), np.linspace(0, planning_horizon - 1, height))
    heatmap = np.load("data/heatmap/heatmap_100000.npy")
    z_min, z_max = np.abs(heatmap).min(), np.abs(heatmap).max()
    fig = plt.figure()
    ax = plt.subplot(1, 1, 1)
    c = ax.pcolormesh(X, Y, heatmap, cmap='coolwarm', vmin=z_min, vmax=z_max)
    ax.set_xlabel("Interval")
    ax.set_ylabel("Loadstate in %")
    fig.colorbar(c, ax=ax, label="Güte",pad=0.1)
    market = util.read_solutions(Path("data/heatmap/") / "market")
    market_list = [market.ask(1, i) for i in range(planning_horizon)]
    ax2 = ax.twinx()
    ax2.tick_params(axis="y", labelcolor="g")
    ax2.set_ylabel("Euro/MWh", color="g")
    ax2.plot(market_list, linestyle=":", label="Markethistory", color="g")
    plt.margins(x=0)
    plt.savefig("heatmap-spreading.pdf")

if __name__ == "__main__":
    main()