import copy
import math
from pathlib import Path
import itertools

import numpy as np
import matplotlib.pyplot as plt

from eocohda.core.schedule import Schedule, Slot, Strategy
from eocohda.core.storage import EnergyStorage
from eocohda.core.evaluator import SpreadingEvaluator, OwnConsumptionEvaluator, PeakDemandCuttingEvaluator
from eocohda.core.market import ListMarket
import eocohda.evaluation.util as util

def fill_none(schedules, init_storage, selectable_strategies=Strategy.selectables()):
    for schedule in schedules:
        for i in range(schedule.size()):
            if schedule.slots[i] is None:
                fill_value = Slot(init_storage, 0, 1, selectable_strategies[0]) if i == 0 else copy.deepcopy(schedule.slots[i-1])
                schedule.slots[i] = fill_value

def construct_schedules(level, intervals, discret_num, init_storage=0.5, selectable_strategies=Strategy.selectables()):
    schedule_set = []
    baseline_slots = [None for i in range(intervals)]
    if level == 0:
        level0schedule = [Schedule(baseline_slots)]
        fill_none(level0schedule, init_storage, selectable_strategies=selectable_strategies)
        return level0schedule

    strategy_num = len(selectable_strategies)
    for j in range((discret_num * strategy_num) ** level):
        slots = copy.deepcopy(baseline_slots)
        for i in range(level):
            step = None
            step2 = None
            if i == level - 1:
                step = ((j // strategy_num) % discret_num) / discret_num
                step2 = j % strategy_num
            else:
                step = ((j // (((strategy_num) ** (level - i)) * (discret_num ** (level - i - 1)))) % discret_num) / discret_num
                step2 = (j // ((discret_num * strategy_num) ** (level - 1 - i))) % strategy_num

            slots[i] = Slot(step, 0, 1, selectable_strategies[step2])
        schedule_set.append(Schedule(slots))

    fill_none(schedule_set, init_storage)
    return schedule_set

def create_image_row(level_inv, fitness_list, image, size, discrete_num=20, selectable_strategies=Strategy.selectables()):
    image_row = []
    strategy_num = len(selectable_strategies)

    for i in range(size):
        image_row.append(fitness_list[i // (discrete_num*strategy_num)**level_inv])
    image.append(image_row)

def generate(evaluator, init_load, selectable_strategies=Strategy.selectables()):
    discrete_num = 5
    level = 5
    image = []
    fitness_array = []
    for i in range(level):
        level0 = construct_schedules(i, level - 1, discrete_num, init_load, selectable_strategies=selectable_strategies)
        level0fitness = [evaluator.eval(schedule) for schedule in level0]
        fitness_array.append(level0fitness)

    for i in range(level):
        create_image_row(level - i - 1, fitness_array[i], image, len(fitness_array[level-1]), discrete_num, selectable_strategies=selectable_strategies)

    np_image = np.array(image)
    return np_image

def show(np_image, title, filename, font_size=20):
    fig = plt.figure()
    fig.set_size_inches(16, 10)
    plt.rcParams.update({'font.size': font_size})
    ax = plt.subplot(1, 1, 1)
    c = ax.pcolormesh(np.rot90(np_image), cmap='coolwarm', vmin=-9999, vmax=np_image.max())
    ax.set_xlabel("Depth")
    fig.colorbar(c, ax=ax, label="Fitness")
    plt.tick_params(
        axis='y',
        which='both',
        bottom=False,
        top=False,
        left=False,
        labelleft=False)

    if filename is not None:
        plt.savefig(filename)
    else:
        plt.show()

def main():

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0) \
                .capacity(1000) \
                .charge_efficiency(np.sqrt(0.8)) \
                .discharge_efficiency(np.sqrt(0.8)) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")
    evaluator_spreading = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.CHARGE, Strategy.DISCHARGE, Strategy.OWN_DISCHARGE])

    hh_profile = np.array([500, 322, 30, 111])
    user_values = [16 for _ in itertools.repeat(None, 5)]
    user_market = ListMarket(user_values, user_values)
    evaluator_peak = PeakDemandCuttingEvaluator(user_market, storage, hh_profile, blacklist=[Strategy.CHARGE, Strategy.DISCHARGE, Strategy.SELL])

    hh_profile = np.array([111, 322, 30, 500])
    pv_profile = np.array([211, 222, 300, 50])
    user_values = [29 for _ in itertools.repeat(None, 5)]
    user_market = ListMarket(user_values, user_values)
    input_energy =  (np.array(pv_profile) - np.array(hh_profile)).clip(min=0)
    evaluator_own = OwnConsumptionEvaluator(user_market, storage, hh_profile, pv_profile, input_list=input_energy, blacklist=[Strategy.SELL, Strategy.DISCHARGE, Strategy.BUY])

    np_image_spreading = generate(evaluator_spreading, storage.load, selectable_strategies=[Strategy.BUY, Strategy.SELL])
    np_image_peak = generate(evaluator_peak, storage.load, selectable_strategies=[Strategy.BUY, Strategy.OWN_DISCHARGE])
    np_image_own = generate(evaluator_own, storage.load, selectable_strategies=[Strategy.CHARGE, Strategy.OWN_DISCHARGE])

    show(np_image_spreading, "Spreading", "tree-spreading.pdf")
    show(np_image_own, "Own Consumption Optimization", "tree-own.pdf", font_size=34)
    show(np_image_peak, "Peak Shaving", "tree-peak.pdf", font_size=34)


if __name__ == "__main__":
    main()