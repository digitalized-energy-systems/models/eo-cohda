import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

def bivariate_normal(X, Y, sigmax=1.0, sigmay=1.0,
                     mux=0.0, muy=0.0, sigmaxy=0.0):
    """
    Bivariate Gaussian distribution for equal shape *X*, *Y*.
    See `bivariate normal
    <http://mathworld.wolfram.com/BivariateNormalDistribution.html>`_
    at mathworld.
    """
    Xmu = X-mux
    Ymu = Y-muy

    rho = sigmaxy/(sigmax*sigmay)
    z = Xmu**2/sigmax**2 + Ymu**2/sigmay**2 - 2*rho*Xmu*Ymu/(sigmax*sigmay)
    denom = 2*np.pi*sigmax*sigmay*np.sqrt(1-rho**2)
    return np.exp(-z/(2*(1-rho**2))) / denom

def main():

    matplotlib.rcParams['xtick.direction'] = 'out'
    matplotlib.rcParams['ytick.direction'] = 'out'

    delta = 0.1
    x = np.arange(-5, 10, delta)
    y = np.arange(-5, 10, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = bivariate_normal(X, Y, 5, 3.0, 2, 1.0)
    Z2 = bivariate_normal(X, Y, 4, 6.5, 1, 2)
    Z = 10 * (Z2 - Z1)

    plt.figure()
    CS = plt.contour(X, Y, Z)

    plt.savefig("contour.pdf")


if __name__ == "__main__":
    main()