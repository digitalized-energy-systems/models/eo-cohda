from statistics import mean 
import argparse

import numpy as np
from matplotlib import cm 
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d
import skimage.feature as ski

from eocohda.algorithm.evo import EvoAlgorithm, EvoAlgorithmParetoNSGA, EvoAlgorithmBlackbox
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket, RandomMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy, Schedule, Slot

def main():
    parser = argparse.ArgumentParser(description='Plot solution of algorithm')
    parser.add_argument('--fitness', dest='fitness', action='store_true', default=False)
    parser.add_argument('--solution', dest='solution', action='store_true', default=False)
    parser.add_argument('--market', dest='market', action='store_true', default=False)
    parser.add_argument('--sample', dest='sample', type=int, default=0)
    args = parser.parse_args()

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(50000) \
                .max_discharge(50000) \
                .build()

    if args.solution:

        evaluator = SpreadingEvaluator(TestMarket(), storage)
        algorithm = EvoAlgorithm(20, 10, 4, 10, evaluator)

        algorithm.generate_schedules(0, 1, 96, 1)
        best = algorithm.all_solutions[-1].candidate 
        color_map = {
            Strategy.BUY : "r",
            Strategy.DISCHARGE : "b",
            Strategy.SELL: "y",
            Strategy.BLOCKED_CHARGE: "g",
            Strategy.BLOCKED_DISCHARGE: "orange",
            Strategy.OWN_DISCHARGE : "black"
        }
        fig, ax = plt.subplots()
        for i in range(best.size()):
            ax.plot([best.slots[i].start, best.slots[i].end], [storage.load if i == 0 else best.slots[i - 1].load_state, best.slots[i].load_state], label=str(best.slots[i].strategy), 
                    color=color_map[best.slots[i].strategy], linestyle='-', linewidth=2)
        handles, labels = ax.get_legend_handles_labels()
        unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
        ax.legend(*zip(*unique), loc='upper right')
        plt.show()

    if args.fitness:

        evaluator = SpreadingEvaluator(TestMarket(), storage)
        algorithm = EvoAlgorithm(20, 10, 8, 100, evaluator)

        algorithm.generate_schedules(0, 1, 10, 1)

        fitnessHistory = [solution.fitness for solution in algorithm.solutions]

        # Zeige Historyfunktionen
        plt.axhline(0, ls=":", color="g")
        plt.plot(fitnessHistory, label='fitness', color="r")

        # Fuegt eine legende an
        plt.legend(loc='upper left')
        plt.show()

    if args.market:

        evaluator = SpreadingEvaluator(TestMarket(), storage)

        # Visualisierung lokal BUY/SELL Load-State Y Zeit X Eine Lösung ist ein Graph 
        # durch den Raum
        # Resultat: Constraint-freie Visualisierung 

        Y, X = np.meshgrid(np.linspace(-100, 100, 200), np.linspace(0, 9, 10))
        Z = np.zeros((10, 200))

        for y in range(len(Y[0])):
            for x in range(len(X)):
                Z[x][y] = evaluator.eval(Schedule([Slot(np.abs(Y[0][y]), x * 15, (x + 1) * 15, Strategy.SELL if Y[0][y] > 0 else Strategy.BUY)]))

        z_min, z_max = -np.abs(Z).max(), np.abs(Z).max()
        fig, ax = plt.subplots()
        c = ax.pcolormesh(X, Y, Z, cmap='RdBu', vmin=z_min, vmax=z_max)

        ax.set_title('pcolormesh')
        ax.axis([X.min(), X.max(), Y.min(), Y.max()])
        fig.colorbar(c, ax=ax)
        plt.show()
    
    if args.sample > 0 or True:

        sample_num = 10
        planning_horizon = 10
        width = 100
        height = 100
        market = RandomMarket(planning_horizon)
        evaluator = SpreadingEvaluator(market, storage)
        solution_gen = SolutionGenerator(evaluator)

        samples = []
        fitnessValues = []
        for i in range(sample_num):
            samples.append(solution_gen.generate(planning_horizon))
            fitnessValues.append(evaluator.eval(samples[i]))

        fitness = np.array(fitnessValues)
        fitness = (fitness + np.abs(np.min(fitness)))
        fitness /= np.max(fitness)

        fig = plt.figure()
        plt.subplot(2, 2, 1)
        plt.title("Sample-Schedules")
        color_map = plt.get_cmap("coolwarm")
        for i in range(len(samples)):
            plt.plot([sample.load_state for sample in samples[i].slots], color=color_map(fitness[i]))
        plt.colorbar(cm.ScalarMappable(cmap=color_map))

        plt.subplot(2, 2, 2)
        plt.title("Market-History")
        plt.plot(market.uniform_random_values, color="black")

        Y, X = np.meshgrid(np.linspace(0, 1, width), np.linspace(0, planning_horizon - 1, height))
        Z = np.zeros((width, height))

        # Generate Heatmap with the samples as data base
        nearest_count = int(sample_num / 10)
        for y in range(len(Y[0])):
            for x in range(len(X)):
                nearest = []
                for i in range(len(samples)):
                   for j in range(samples[i].size()):
                        slot = samples[i].get_slot(j)
                        interval_num = j
                        slot_state = slot.load_state * planning_horizon
                        nearest.append((np.linalg.norm(np.array([interval_num, slot_state] - np.array([X[x][y], Y[x][y] * planning_horizon]))), fitness[i]))
                nearest.sort(key=lambda x: x[0], reverse=False)
                top_nearest = nearest[0:nearest_count]
                Z[x][y] = mean([tupel[1] for tupel in top_nearest])

        z_min, z_max = np.abs(Z).min(), np.abs(Z).max()
        ax = plt.subplot(2, 2, 3)
        c = ax.pcolormesh(X, Y, Z, cmap='coolwarm', vmin=z_min, vmax=z_max, shading='gouraud')

        ax.set_title('Heatmap')
        ax.axis([X.min(), X.max(), Y.min(), Y.max()])
        fig.colorbar(c, ax=ax)
        
        # Roughness GLCM Matrix
        ax = plt.subplot(2, 2, 4)
        c = ax.pcolormesh(X, Y, Z, cmap='coolwarm', vmin=z_min, vmax=z_max, shading='gouraud')
        ax.set_title('Roughness GLCM')
        fig.colorbar(c, ax=ax)
        plt.show()

if __name__ == "__main__":
    main()