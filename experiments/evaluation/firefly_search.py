import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import cm
import matplotlib as mpl

from eocohda.algorithm.firefly import FireFly
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket
from eocohda.core.storage import EnergyStorage

def main():

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(50000) \
                .max_discharge(50000) \
                .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    popsize = 10
    iter_num = 10
    algorithm = FireFly(evaluator, n=popsize, delta=1, iteration_count=iter_num, alpha=0.5)

    algorithm.generate_schedules(0, 1, 2, 1)
    algorithm_history = algorithm.mutation_history


    plt.rcParams.update({'font.size': 18})
    ax = plt.subplot(111)
    cmap = cm.get_cmap('rainbow')
    normalize = mpl.colors.Normalize(vmin=0, vmax=iter_num)
    colors = [cmap(normalize(i)) for i in range(iter_num+1)]
    for i in range(iter_num + 1):
        for j in range(popsize):
            solution = algorithm_history[i][j]
            x = solution.candidate[0][0]
            y = solution.candidate[0][1]
            ax.plot(x, y, "ro", color=colors[i])
    ax.set_xlabel("Intervall 1")
    ax.set_ylabel("Intervall 2")
    cax, _ = mpl.colorbar.make_axes(ax)
    cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
    cbar.set_label("Iteration")

    plt.show()

if __name__ == "__main__":
    main()