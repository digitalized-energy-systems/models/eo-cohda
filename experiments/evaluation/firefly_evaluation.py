from pathlib import Path

import numpy as np

import eocohda.evaluation.executor as executor
import eocohda.evaluation.common as evaluation
from eocohda.algorithm.firefly import FireFly
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import ListMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy
import eocohda.core.util as coreutil


def background_adaptive_alpha():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    simple_storage_fitness_list_repr = executor.execute_firefly(storage, times=100, enable_brush=True)
    simple_storage_fitness_list_without_repr2 = executor.execute_firefly(storage, times=100, enable_brush=False)
    evaluation.show_fitness_datasets([(simple_storage_fitness_list_repr, "red", "Gauß-Zufallsterm"),
                                      (simple_storage_fitness_list_without_repr2, "blue", "Original Zufallsterm")], show_deviation=True)

def y():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=0.1)
    fitness_history_list2 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=0.5)
    fitness_history_list3 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=1)
    fitness_history_list4 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=2)
    fitness_history_list5 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=4)
    fitness_history_list6 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=8)
    fitness_history_list7 = executor.execute_firefly(storage, evaluator=evaluator, times=100, y=16)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7), Path("data/parameter/firefly") / "y")

def alpha():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=0.01)
    fitness_history_list2 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=0.1)
    fitness_history_list3 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=0.5)
    fitness_history_list4 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=1)
    fitness_history_list5 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=2)
    fitness_history_list6 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=4)
    fitness_history_list7 = executor.execute_firefly(storage, evaluator=evaluator, times=100, alpha=8)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7), Path("data/parameter/firefly") / "alpha")

def delta():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=1)
    fitness_history_list2 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.99)
    fitness_history_list3 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.95)
    fitness_history_list4 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.9)
    fitness_history_list5 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.8)
    fitness_history_list6 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.5)
    fitness_history_list7 = executor.execute_firefly(storage, evaluator=evaluator, times=100, delta=0.1)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7), Path("data/parameter/firefly") / "delta")

def mu():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=1.01)
    fitness_history_list2 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=1.1)
    fitness_history_list3 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=1.2)
    fitness_history_list4 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=1.5)
    fitness_history_list5 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=2)
    fitness_history_list6 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=4)
    fitness_history_list7 = executor.execute_firefly(storage, evaluator=evaluator, times=100, mu=8)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7), Path("data/parameter/firefly") / "mu")

def popsize():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=1)
    fitness_history_list2 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=2)
    fitness_history_list3 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=4)
    fitness_history_list4 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=8)
    fitness_history_list5 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=16)
    fitness_history_list6 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=32)
    fitness_history_list7 = executor.execute_firefly(storage, evaluator=evaluator, times=100, popsize=64)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7), Path("data/parameter/firefly") / "popsize")

def evaluate():
    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7 = coreutil.read_solutions(Path("data/parameter/firefly") / "popsize")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Populationsgröße: 1"),
                                      (to_fitness(fitness_history_list2), "blue", "Populationsgröße: 2"),
                                      (to_fitness(fitness_history_list3), "green", "Populationsgröße: 4"),
                                      (to_fitness(fitness_history_list4), "purple", "Populationsgröße: 8"),
                                      (to_fitness(fitness_history_list5), "magenta", "Populationsgröße: 16"),
                                      (to_fitness(fitness_history_list6), "cyan", "Populationsgröße: 32"),
                                      (to_fitness(fitness_history_list7), "black", "Populationsgröße: 64")], name="firefly-popsize.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "1"),
                                      (to_fitness(fitness_history_list2), "blue", "2"),
                                      (to_fitness(fitness_history_list3), "green", "4"),
                                      (to_fitness(fitness_history_list4), "purple", "8"),
                                      (to_fitness(fitness_history_list5), "magenta", "16"),
                                      (to_fitness(fitness_history_list6), "cyan", "32"),
                                      (to_fitness(fitness_history_list7), "black", "64")], x_label="Populationsgröße", name="firefly-popsize-boxplot.pdf")

    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7 = coreutil.read_solutions(Path("data/parameter/firefly") / "mu")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "My: 1.01"),
                                      (to_fitness(fitness_history_list2), "blue", "My: 1.1"),
                                      (to_fitness(fitness_history_list3), "green", "My: 1.2"),
                                      (to_fitness(fitness_history_list4), "purple", "My: 1.5"),
                                      (to_fitness(fitness_history_list5), "magenta", "My: 2"),
                                      (to_fitness(fitness_history_list6), "cyan", "My: 4"),
                                      (to_fitness(fitness_history_list7), "black", "My: 8")], name="firefly-mu.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "1.01"),
                                      (to_fitness(fitness_history_list2), "blue", "1.1"),
                                      (to_fitness(fitness_history_list3), "green", "1.2"),
                                      (to_fitness(fitness_history_list4), "purple", "1.5"),
                                      (to_fitness(fitness_history_list5), "magenta", "2"),
                                      (to_fitness(fitness_history_list6), "cyan", "4"),
                                      (to_fitness(fitness_history_list7), "black", "8")], x_label="My", name="firefly-mu-boxplot.pdf")

    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7 = coreutil.read_solutions(Path("data/parameter/firefly") / "delta")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Delta: 1"),
                                      (to_fitness(fitness_history_list2), "blue", "Delta: 0.99"),
                                      (to_fitness(fitness_history_list3), "green", "Delta: 0.95"),
                                      (to_fitness(fitness_history_list4), "purple", "Delta: 0.9"),
                                      (to_fitness(fitness_history_list5), "magenta", "Delta: 0.8"),
                                      (to_fitness(fitness_history_list6), "cyan", "Delta: 0.5"),
                                      (to_fitness(fitness_history_list7), "black", "Delta: 0.1")], name="firefly-delta.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "1"),
                                      (to_fitness(fitness_history_list2), "blue", "0.99"),
                                      (to_fitness(fitness_history_list3), "green", "0.95"),
                                      (to_fitness(fitness_history_list4), "purple", "0.9"),
                                      (to_fitness(fitness_history_list5), "magenta", "0.8"),
                                      (to_fitness(fitness_history_list6), "cyan", "0.5"),
                                      (to_fitness(fitness_history_list7), "black", "0.1")], x_label="Delta", name="firefly-delta-boxplot.pdf")

    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7 = coreutil.read_solutions(Path("data/parameter/firefly") / "alpha")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Alpha: 0.01"),
                                      (to_fitness(fitness_history_list2), "blue", "Alpha: 0.1"),
                                      (to_fitness(fitness_history_list3), "green", "Alpha: 0.5"),
                                      (to_fitness(fitness_history_list4), "purple", "Alpha: 1"),
                                      (to_fitness(fitness_history_list5), "magenta", "Alpha: 2"),
                                      (to_fitness(fitness_history_list6), "cyan", "Alpha: 4"),
                                      (to_fitness(fitness_history_list7), "black", "Alpha: 8")], name="firefly-alpha.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "0.01"),
                                      (to_fitness(fitness_history_list2), "blue", "0.1"),
                                      (to_fitness(fitness_history_list3), "green", "0.5"),
                                      (to_fitness(fitness_history_list4), "purple", "1"),
                                      (to_fitness(fitness_history_list5), "magenta", "2"),
                                      (to_fitness(fitness_history_list6), "cyan", "4"),
                                      (to_fitness(fitness_history_list7), "black", "8")], x_label="Alpha", name="firefly-alpha-boxplot.pdf")

    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7 = coreutil.read_solutions(Path("data/parameter/firefly") / "y")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Absorption: 0.1"),
                                      (to_fitness(fitness_history_list2), "blue", "Absorption: 0.5"),
                                      (to_fitness(fitness_history_list3), "green", "Absorption: 1"),
                                      (to_fitness(fitness_history_list4), "purple", "Absorption: 2"),
                                      (to_fitness(fitness_history_list5), "magenta", "Absorption: 4"),
                                      (to_fitness(fitness_history_list6), "cyan", "Absorption: 8"),
                                      (to_fitness(fitness_history_list7), "black", "Absorption: 16")], name="firefly-y.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "0.1"),
                                      (to_fitness(fitness_history_list2), "blue", "0.5"),
                                      (to_fitness(fitness_history_list3), "green", "1"),
                                      (to_fitness(fitness_history_list4), "purple", "2"),
                                      (to_fitness(fitness_history_list5), "magenta", "4"),
                                      (to_fitness(fitness_history_list6), "cyan", "8"),
                                      (to_fitness(fitness_history_list7), "black", "16")], x_label="Absorption", name="firefly-y-boxplot.pdf")

def to_fitness(solution_set):
    return [[solution.fitness for solution in solutions] for solutions in solution_set]

def main():
    #background_adaptive_alpha()
    #y()
    #alpha()
    #delta()
    #mu()
    #popsize()
    evaluate()


if __name__ == "__main__":
    main()