from pathlib import Path

import numpy as np

import eocohda.evaluation.executor as executor
import eocohda.evaluation.common as evaluation
from eocohda.algorithm.gwo import GreyWolfOptimizer
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import ListMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy
import eocohda.core.util as coreutil

def mutation_background():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    simple_storage_fitness_list = executor.execute_gwo(storage, evaluator=evaluator, times=10)
    simple_storage_fitness_list_no_diff = executor.execute_gwo(storage, evaluator=evaluator, times=10, diff=False)
    simple_storage_fitness_list_no_elim = executor.execute_gwo(storage, evaluator=evaluator, times=10, elim=False)
    simple_storage_fitness_list_no_elim_diff = executor.execute_gwo(storage, evaluator=evaluator, times=10, elim=False, diff=False)
    evaluation.show_fitness_datasets([(to_fitness(simple_storage_fitness_list), "red", "DE und Eliminierung"),
                                      (to_fitness(simple_storage_fitness_list_no_diff), "blue", "Nur Eliminierung"),
                                      (to_fitness(simple_storage_fitness_list_no_elim), "green", "Nur DE"),
                                      (to_fitness(simple_storage_fitness_list_no_elim_diff), "purple", "Weder Eliminierung noch DE")], show_deviation=False, name="gwo-mutation-varianten.png")

def popsize():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=1)
    fitness_history_list2 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=2)
    fitness_history_list3 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=3)
    fitness_history_list4 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=4)
    fitness_history_list5 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=8)
    fitness_history_list6 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=16)
    fitness_history_list7 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=24)
    fitness_history_list8 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=30)
    fitness_history_list9 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=60)
    fitness_history_list10 = executor.execute_gwo(storage, evaluator=evaluator, times=100, popsize=120)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7, fitness_history_list8, fitness_history_list9,
                              fitness_history_list10), Path("data/parameter/gwo") / "popsize")

def elim_scale():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=1)
    fitness_history_list5 = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=0.75)
    fitness_history_list2 = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=0.5)
    fitness_history_list3 = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=0.25)
    fitness_history_list4 = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=0.1)
    fitness_history_list6 = executor.execute_gwo(storage, evaluator=evaluator, times=100, elim_scale=0)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6), Path("data/parameter/gwo") / "elim")

def evaluate():
    # ELIM
    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6 = coreutil.read_solutions(Path("data/parameter/gwo") / "elim")

    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Elimierungsrate: 1"),
                                      (to_fitness(fitness_history_list2), "blue", "Elimierungsrate: 0.75"),
                                      (to_fitness(fitness_history_list3), "orange", "Elimierungsrate: 0.5"),
                                      (to_fitness(fitness_history_list4), "green", "Elimierungsrate: 0.25"),
                                      (to_fitness(fitness_history_list5), "pink", "Elimierungsrate: 0.1"),
                                      (to_fitness(fitness_history_list6), "black", "Elimierungsrate: 0")], name="gwo-elim.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "1"),
                                      (to_fitness(fitness_history_list2), "blue", "0.75"),
                                      (to_fitness(fitness_history_list3), "orange", "0.5"),
                                      (to_fitness(fitness_history_list4), "green", "0.25"),
                                      (to_fitness(fitness_history_list5), "pink", "0.1"),
                                      (to_fitness(fitness_history_list6), "black", "0")], x_label="Eliminierungsrate", name="gwo-elim-boxplot.pdf")


    # POPSIZE
    fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7, fitness_history_list8, fitness_history_list9, fitness_history_list10 = coreutil.read_solutions(Path("data/parameter/gwo") / "popsize")
    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Größe der Population: 1"),
                                      (to_fitness(fitness_history_list2), "blue", "Größe der Population: 2"),
                                      (to_fitness(fitness_history_list3), "orange", "Größe der Population: 4"),
                                      (to_fitness(fitness_history_list4), "green", "Größe der Population: 8"),
                                      (to_fitness(fitness_history_list5), "pink", "Größe der Population: 12"),
                                      (to_fitness(fitness_history_list6), "black", "Größe der Population: 16"),
                                      (to_fitness(fitness_history_list7), "cyan", "Größe der Population: 24"),
                                      (to_fitness(fitness_history_list8), "magenta", "Größe der Population: 30"),
                                      (to_fitness(fitness_history_list9), "gray", "Größe der Population: 60"),
                                      (to_fitness(fitness_history_list10), "olive", "Größe der Population: 120")], name="gwo-popsize.pdf")
    evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), "red", "1"),
                                      (to_fitness(fitness_history_list2), "blue", "2"),
                                      (to_fitness(fitness_history_list3), "orange", "4"),
                                      (to_fitness(fitness_history_list4), "green", "8"),
                                      (to_fitness(fitness_history_list5), "pink", "12"),
                                      (to_fitness(fitness_history_list6), "black", "16"),
                                      (to_fitness(fitness_history_list7), "cyan", "24"),
                                      (to_fitness(fitness_history_list8), "magenta", "30"),
                                      (to_fitness(fitness_history_list9), "gray", "60"),
                                      (to_fitness(fitness_history_list10), "olive", "120")], x_label="Größe der Population", name="gwo-popsize-boxplot.pdf")

def to_fitness(solution_set):
    return [[solution.fitness for solution in solutions] for solutions in solution_set]

def main():
    #mutation_background()
    #popsize()
    #elim_scale()
    evaluate()
    print("Done")

if __name__ == "__main__":
    main()