from pathlib import Path
import itertools

import numpy as np

import eocohda.evaluation.executor as executor
import eocohda.evaluation.common as evaluation
import eocohda.evaluation.util as util
import eocohda.evaluation.cohda.local as local
import eocohda.core.util as coreutil

from eocohda.algorithm.evo import EvoAlgorithm
from eocohda.algorithm.firefly import FireFly
from eocohda.algorithm.gwo import GreyWolfOptimizer
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator, OwnConsumptionEvaluator, PeakDemandCuttingEvaluator
from eocohda.core.market import ListMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy


def spreading():
    """
    Storage: https://www.bves.de/wp-content/uploads/2017/04/Pumpspeicherwek.pdf
    Unit: MW(h)

    'Installierte Leistung: 1.060 MW
    Speicherkapazität: 8,5 GWh
    Fallhöhe: 302 m
    Nenndurchfluss: 103 m3/s
    Maschinenkaverne: Länge 137 m, Breite 15 m, Höhe 17 m
    Trafokaverne: Länge 122 m, Breite 15 m, Höhe 17 m
    Maschinensätze: zwei reversible Pumpturbinen mit konstanter Drehzahl
    zwei reversible Pumpturbinen mit variabler Drehzahl
    Oberbecken: Höhenlage 880 m ü. MH
    Inhalt 12 Millionen Kubikmeter Wasser
    Unterbecken: Höhenlage 550 m ü. MH
    Inhalt 18,9 Millionen Kubikmeter Wasser
    Wirkungsgrad: 80 % Wälzwirkungsgrad'
    """
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0) \
                .capacity(8500) \
                .charge_efficiency(np.sqrt(0.8)) \
                .discharge_efficiency(np.sqrt(0.8)) \
                .max_charge(265) \
                .max_discharge(265) \
                .build()
    energy_values = util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    evo_result, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=2, gen_size=32, popsize=64, iter_num=1000)
    firefly_result = executor.execute_firefly(storage, evaluator=evaluator, times=100, intervals=96, delta=0.99, alpha=1, y=1, popsize=32, iter_num=1000)
    #gwo_result = executor.execute_gwo(storage, evaluator=evaluator, times=100, intervals=96, elim_scale=0.5, popsize=16, iter_num=1000)

    #coreutil.write_solutions(gwo_result, Path("data/solutions") / "gwospreading")
    coreutil.write_solutions(firefly_result, Path("data/solutions") / "fireflyspreading")
    coreutil.write_solutions(evo_result, Path("data/solutions") / "evospreading")

def energy_own_consumption():
    """
    Storage: http://www.rusol.com/fileadmin/be_user/downloads/Broschuere_Energiespeichersystem_neu.pdf
    Unit: kW(h)

    Wirkungsgrad: 81%
    Ladeleistung 1650 W
    Entladeleistung 2400 W
    Kapazität 5,12 kWh

    User-Market: 0.0002947 Euro/Wh
    - basierend auf Durchschnittspreis 2018
    """
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.1) \
                .self_discharge(0) \
                .capacity(5120) \
                .charge_efficiency(np.sqrt(0.81)) \
                .discharge_efficiency(np.sqrt(0.81)) \
                .max_charge(412.5) \
                .max_discharge(600) \
                .build()
    hh_profile = util.read_household_profile([Path("data/household/") / "example.csv"], 2) # Note: the original household data must not be published, as a consequence the data is replaced with synthetic examples
    pv_profile = util.read_pv_profile_kronberg(Path("data/pv/") / "test_case_own_con.csv")
    user_values = [0.0002947 for _ in itertools.repeat(None, 96)]
    user_market = ListMarket(user_values, user_values)
    input_energy =  (np.array(pv_profile) - np.array(hh_profile)).clip(min=0)
    evaluator = OwnConsumptionEvaluator(user_market, storage, hh_profile, pv_profile, input_list=input_energy, blacklist=[Strategy.BUY, Strategy.DISCHARGE, Strategy.SELL])

    evo_result, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=2, gen_size=32, popsize=64, iter_num=1000)
    #firefly_result = executor.execute_firefly(storage, evaluator=evaluator, times=100, intervals=96, delta=0.99, alpha=5.5, y=1, popsize=32, iter_num=1000)
    #gwo_result = executor.execute_gwo(storage, evaluator=evaluator, times=100, intervals=96, elim_scale=0.5, popsize=16, iter_num=1000)

    
    #coreutil.write_solutions(gwo_result, Path("data/solutions") / "gwoown")
    #coreutil.write_solutions(firefly_result, Path("data/solutions") / "fireflyown")
    coreutil.write_solutions(evo_result, Path("data/solutions") / "evoown")

def peak_cutting():
    """
    Storage: TESVOLT Speichersystem (https://www.tesvolt.com/de/konfigurator.html)
    Data-Unit: W(h)

    Wirkungsgrad: 92%
    Ladeleistung 60 - 600 kW
    Entladeleistung 60 - 600 kW
    Kapazität 672.0 - 2688.0 kWh

    oder

    33,3 kWh/33 kW (kleines Unternehmen)
    110,7 kWh/110,7 kW (mittleres Unternehmen)
    177,7 kWh/117,6 kW (großes Unternehmen mit stärkerer Spitzenreduktion)

    User-Market: 15.86 Cent/kWh
    - basierend auf Durchschnittspreis 2014 (2.000 - 20.000 MWh/a)
    """
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.1) \
                .self_discharge(0) \
                .capacity(33300) \
                .charge_efficiency(np.sqrt(0.92)) \
                .discharge_efficiency(np.sqrt(0.92)) \
                .max_charge(8250) \
                .max_discharge(8250) \
                .build()
    hh_profile = util.read_industry_profile(Path("data/industry/") / "example.csv", 0) # Note: the original industry data must not be published, as a consequence the data is replaced with synthetic examples
    user_values = [0.0001649 for _ in itertools.repeat(None, 96)]
    user_market = ListMarket(user_values, user_values)
    evaluator = PeakDemandCuttingEvaluator(user_market, storage, hh_profile, blacklist=[Strategy.SELL, Strategy.DISCHARGE, Strategy.CHARGE])

    evo_result, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=2, gen_size=32, popsize=64, iter_num=1000)
    #firefly_result = executor.execute_firefly(storage, evaluator=evaluator, times=100, intervals=96, delta=0.99, alpha=1, y=1, popsize=32, iter_num=1000)
    #gwo_result = executor.execute_gwo(storage, evaluator=evaluator, times=100, intervals=96, elim_scale=0.5, popsize=16, iter_num=1000)
    #coreutil.write_solutions(gwo_result, Path("data/solutions") / "gwopeak")
    #coreutil.write_solutions(firefly_result, Path("data/solutions") / "fireflypeak")
    coreutil.write_solutions(evo_result, Path("data/solutions") / "evopeak")

def evaluate():
    # PEAK
    evo_result = coreutil.read_solutions(Path("data/solutions") / "evopeak")
    firefly_result = coreutil.read_solutions(Path("data/solutions") / "fireflypeak")
    gwo_result = coreutil.read_solutions(Path("data/solutions") / "gwopeak")

    evaluation.show_best_fitness_as_boxplots([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      font_size=24,
                                      name="local-benchmark-peak-boxplot.pdf")

    hh_profile = util.read_industry_profile(Path("data/industry/") / "small_industry.csv", 0)
    evaluation.show_solution(best_of(evo_result).candidate, 0, "EVO: Spitzenlastkappung",
                                name="local-benchmark-evo-peak-solution.pdf",
                                alt_data=[hh_profile],
                                alt_label=["Lastprofil"],
                                alt_y_label="Leistung in W")
    evaluation.show_solution(FireFly.from_firefly_solution(best_of(firefly_result).candidate), 0, "FireFly: Spitzenlastkappung",
                                name="local-benchmark-firefly-peak-solution.pdf",
                                alt_data=[hh_profile],
                                alt_label=["Lastprofil"],
                                alt_y_label="Leistung in W")
    evaluation.show_solution(GreyWolfOptimizer.from_gwo_solution(best_of(gwo_result).candidate), 0, "GWO: Spitzenlastkappung",
                                name="local-benchmark-gwo-peak-solution.pdf",
                                alt_data=[hh_profile],
                                alt_label=["Lastprofil"],
                                alt_y_label="Leistung in W")

    evaluation.show_fitness_datasets([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      show_deviation=False,
                                      font_size=24,
                                      name="local-benchmark-peak-cutting.pdf")

    # OWN
    evo_result = coreutil.read_solutions(Path("data/solutions") / "evoown")
    firefly_result = coreutil.read_solutions(Path("data/solutions") / "fireflyown")
    gwo_result = coreutil.read_solutions(Path("data/solutions") / "gwoown")

    evaluation.show_best_fitness_as_boxplots([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      font_size=24,
                                      name="local-benchmark-own-boxplot.pdf")

    hh_profile = util.read_household_profile([Path("data/household/") / "PL1.csv"], 2)
    pv_profile = util.read_pv_profile_kronberg(Path("data/pv/") / "test_case_own_con.csv")
    evaluation.show_solution(best_of(evo_result).candidate, 0.1, "EVO: Eigenverbrauchsoptimierung",
                                name="local-benchmark-evo-own-solution.pdf",
                                alt_data=[hh_profile, pv_profile],
                                alt_label=["Lastprofil", "PV-Profil"],
                                alt_y_label="Leistung in W")
    evaluation.show_solution(FireFly.from_firefly_solution(best_of(firefly_result).candidate), 0.1, "FireFly: Eigenverbrauchsoptimierung",
                                name="local-benchmark-firefly-own-solution.pdf",
                                alt_data=[hh_profile, pv_profile],
                                alt_label=["Lastprofil", "PV-Profil"],
                                alt_y_label="Leistung in W")
    evaluation.show_solution(GreyWolfOptimizer.from_gwo_solution(best_of(gwo_result).candidate), 0.1, "GWO: Eigenverbrauchsoptimierung",
                                name="local-benchmark-gwo-own-solution.pdf",
                                alt_data=[hh_profile, pv_profile],
                                alt_label=["Lastprofil", "PV-Profil"],
                                alt_y_label="Leistung in W")

    evaluation.show_fitness_datasets([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      show_deviation=False,
                                      font_size=24,
                                      name="local-benchmark-own.pdf")

    # SPREADING
    evo_result = coreutil.read_solutions(Path("data/solutions") / "evospreading")
    firefly_result = coreutil.read_solutions(Path("data/solutions") / "fireflyspreading")
    gwo_result = coreutil.read_solutions(Path("data/solutions") / "gwospreading")

    evaluation.show_best_fitness_as_boxplots([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      font_size=24,
                                      name="local-benchmark-spreading-boxplot.pdf")

    energy_values = util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")
    evaluation.show_solution(best_of(evo_result).candidate, 0, "EVO: Spreading",
                                name="local-benchmark-evo-spreading-solution.pdf",
                                alt_data=[energy_values],
                                alt_label=["Marktverlauf"],
                                alt_y_label="Euro/MWh")
    evaluation.show_solution(FireFly.from_firefly_solution(best_of(firefly_result).candidate), 0, "FireFly: Spreading",
                                name="local-benchmark-firefly-spreading-solution.pdf",
                                alt_data=[energy_values],
                                alt_label=["Marktverlauf"],
                                alt_y_label="Euro/MWh")
    evaluation.show_solution(GreyWolfOptimizer.from_gwo_solution(best_of(gwo_result).candidate), 0, "GWO: Spreading",
                                name="local-benchmark-gwo-spreading-solution.pdf",
                                alt_data=[energy_values],
                                alt_label=["Marktverlauf"],
                                alt_y_label="Euro/MWh")

    evaluation.show_fitness_datasets([(to_fitness(evo_result), "blue", "EVO"),
                                      (to_fitness(gwo_result), "green", "GWO"),
                                      (to_fitness(firefly_result), "red", "FireFly")],
                                      show_deviation=False,
                                      font_size=24,
                                      name="local-benchmark-spreading.pdf")

def best_of(result_set):
    best = None
    for result in result_set:
        for solution in result:
            if best is None or solution.fitness > best.fitness:
                best = solution
    return best

def to_fitness(result_set):
    fitness_set = []
    for result in result_set:
        fitness_set.append([solution.fitness for solution in result])
    return fitness_set

def main():
    spreading()
    energy_own_consumption()
    peak_cutting()
    evaluate()
    print("Evaluation finished!")

if __name__ == "__main__":
    main()