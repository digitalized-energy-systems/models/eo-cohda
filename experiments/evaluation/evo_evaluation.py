from pathlib import Path
import matplotlib.pyplot as plt

import numpy as np
from matplotlib import cm

import eocohda.evaluation.executor as executor
import eocohda.evaluation.common as evaluation
from eocohda.algorithm.evo import EvoAlgorithm
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import ListMarket, TestMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy
import eocohda.core.util as coreutil
import eocohda.evaluation.cohda.local as test

def background_evaluation():
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(500) \
                .max_discharge(500) \
                .build()
    energy_values = [100, 100, 100, 80, 50, 30, 20, 10, 20, 22, 50, 100, 100, 100, 150, 130, 200, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 100, 200, 100, 300, 50, 30, 20, 101, 102, 103, 80, 80, 80, 80, 80, 80]
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.DISCHARGE, Strategy.OWN_DISCHARGE, Strategy.CHARGE])

    fitness_history_list, _ = executor.execute_evo(storage, evaluator=evaluator, times=1000, intervals=96, parent_num=1, iter_num=1000, mutation_style=0)
    fitness_history_list2, _ = executor.execute_evo(storage, evaluator=evaluator, times=1000, intervals=96, parent_num=1, iter_num=1000, mutation_style=1)
    fitness_history_list3, _ = executor.execute_evo(storage, evaluator=evaluator, times=1000, intervals=96, parent_num=1, iter_num=1000, mutation_style=2)
    fitness_history_list4, _ = executor.execute_evo(storage, evaluator=evaluator, times=1000, intervals=96, parent_num=1, iter_num=1000, mutation_style=3)
    evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), "red", "Random mutation operation"),
                                      (to_fitness(fitness_history_list2), "blue", "Random and local search"),
                                      (to_fitness(fitness_history_list3), "green", "local search"),
                                      (to_fitness(fitness_history_list4), "pink", "no mutation")], show_deviation=False, name="mutation-varianten.pdf")

def parameter_optimization_evaluation_parent():
    (storage, evaluator) = test.spreading_test_case(blacklist_add=Strategy.DISCHARGE, n=96)
    execute_parent(storage, evaluator, "spreading")

def parameter_optimization_evaluation_parent_pc():
    (storage, evaluator) = test.peak_cutting(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_parent(storage, evaluator, "pc")

def parameter_optimization_evaluation_parent_oc():
    (storage, evaluator) = test.own_consumption_test_case(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_parent(storage, evaluator, "oc")

def execute_parent(storage, evaluator, suffix):
    fitness_history_list, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=1, iter_num=500)

    fitness_history_list2, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=2, iter_num=500)
    fitness_history_list3, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=3, iter_num=500)
    fitness_history_list4, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=4, iter_num=500)
    fitness_history_list5, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=8, iter_num=500)
    fitness_history_list6, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, parent_num=16, iter_num=500)
    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6), Path("data/parameter/evo") / f'parent{suffix}')


def parameter_optimization_evaluation_popsize():
    (storage, evaluator) = test.spreading_test_case(n=96, blacklist_add=Strategy.DISCHARGE)
    execute_popsize(storage, evaluator, "spreading")

def parameter_optimization_evaluation_popsize_pc():
    (storage, evaluator) = test.peak_cutting(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_popsize(storage, evaluator, "pc")

def parameter_optimization_evaluation_popsize_oc():
    (storage, evaluator) = test.own_consumption_test_case(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_popsize(storage, evaluator, "oc")

def execute_popsize(storage, evaluator, suffix):
    fitness_history_list, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=1, iter_num=500)
    fitness_history_list2, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=2, iter_num=500)
    fitness_history_list3, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=4, iter_num=500)
    fitness_history_list4, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=8, iter_num=500)
    fitness_history_list5, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=12, iter_num=500)
    fitness_history_list6, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=16, iter_num=500)
    fitness_history_list7, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=24, iter_num=500)
    fitness_history_list8, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=30, iter_num=500)
    fitness_history_list9, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=60, iter_num=500)
    fitness_history_list10, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, popsize=120, iter_num=500)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7, fitness_history_list8, fitness_history_list9,
                              fitness_history_list10), Path("data/parameter/evo") / f'popsize{suffix}')


def parameter_optimization_evaluation_gensize():
    (storage, evaluator) = test.spreading_test_case(n=96, blacklist_add=Strategy.DISCHARGE)
    execute_gensize(storage, evaluator, "spreading")

def parameter_optimization_evaluation_gensize_pc():
    (storage, evaluator) = test.peak_cutting(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_gensize(storage, evaluator, "pc")

def parameter_optimization_evaluation_gensize_oc():
    (storage, evaluator) = test.own_consumption_test_case(170, n=96, blacklist_add=Strategy.DISCHARGE)
    execute_gensize(storage, evaluator, "oc")


def execute_gensize(storage, evaluator, suffix):
    fitness_history_list, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=1, iter_num=500)
    fitness_history_list2, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=2, iter_num=500)
    fitness_history_list3, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=4, iter_num=500)
    fitness_history_list4, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=8, iter_num=500)
    fitness_history_list5, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=12, iter_num=500)
    fitness_history_list6, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=16, iter_num=500)
    fitness_history_list7, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=24, iter_num=500)
    fitness_history_list8, _ = executor.execute_evo(storage, evaluator=evaluator, times=100, intervals=96, gen_size=30, iter_num=500)

    coreutil.write_solutions((fitness_history_list, fitness_history_list2, fitness_history_list3,
                              fitness_history_list4, fitness_history_list5, fitness_history_list6,
                              fitness_history_list7, fitness_history_list8), Path("data/parameter/evo") / f'gensize{suffix}')


def evaluate():
    # GENSIZE
    for suffix in ['spreading', 'pc', 'oc']:
        print("Start Reading")
        fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7, fitness_history_list8 = coreutil.read_solutions(Path("data/parameter/evo") / f'gensize{suffix}')
        print("Finish Reading")
        cmap = cm.get_cmap('plasma')
        colors = [cmap(int(i * cmap.N/8)) for i in range(8)]
        suffix2 = suffix.replace("spreading", "arbitrage")
        evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), colors[0], "Size of the generation: 1", None, None),
                                        (to_fitness(fitness_history_list2), colors[1], "Size of the generation: 2", None, None),
                                        (to_fitness(fitness_history_list3), colors[2], "Size of the generation: 4", None, None),
                                        (to_fitness(fitness_history_list4), colors[3], "Size of the generation: 8", None, None),
                                        (to_fitness(fitness_history_list5), colors[4], "Size of the generation: 12", None, None),
                                        (to_fitness(fitness_history_list6), colors[5], "Size of the generation: 16", None, None),
                                        (to_fitness(fitness_history_list7), colors[6], "Size of the generation: 24", None, None),
                                        (to_fitness(fitness_history_list8), colors[7], "Size of the generation: 30", None, None)], name=f'evo-gensize-{suffix2}.pdf')
        evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), colors[0], "1"),
                                        (to_fitness(fitness_history_list2), colors[1], "2"),
                                        (to_fitness(fitness_history_list3), colors[2], "4"),
                                        (to_fitness(fitness_history_list4), colors[3], "8"),
                                        (to_fitness(fitness_history_list5), colors[4], "12"),
                                        (to_fitness(fitness_history_list6), colors[5], "16"),
                                        (to_fitness(fitness_history_list7), colors[6], "24"),
                                        (to_fitness(fitness_history_list8), colors[7], "30")], x_label="Size of the generation", name=f"evo-gensize-{suffix2}-boxplot.pdf")
        # PARENT
        fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6 = coreutil.read_solutions(Path("data/parameter/evo") / f'parent{suffix}')

        colors = [cmap(int(i * cmap.N/6)) for i in range(6)]
        evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), colors[0], "Number of parents: 1", None, None),
                                        (to_fitness(fitness_history_list2), colors[1], "Number of parents: 2", None, None),
                                        (to_fitness(fitness_history_list3), colors[2], "Number of parents: 3", None, None),
                                        (to_fitness(fitness_history_list4), colors[3], "Number of parents: 4", None, None),
                                        (to_fitness(fitness_history_list5), colors[4], "Number of parents: 8", None, None),
                                        (to_fitness(fitness_history_list6), colors[5], "Number of parents: 16", None, None)], name=f"evo-parents-{suffix2}.pdf")
        evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), colors[0], "1"),
                                        (to_fitness(fitness_history_list2), colors[1], "2"),
                                        (to_fitness(fitness_history_list3), colors[2], "3"),
                                        (to_fitness(fitness_history_list4), colors[3], "4"),
                                        (to_fitness(fitness_history_list5), colors[4], "8"),
                                        (to_fitness(fitness_history_list6), colors[5], "16")], x_label="Number of parents", name=f"evo-parents-{suffix2}-boxplot.pdf")

        # POPSIZE
        fitness_history_list, fitness_history_list2, fitness_history_list3, fitness_history_list4, fitness_history_list5, fitness_history_list6, fitness_history_list7, fitness_history_list8, fitness_history_list9, fitness_history_list10 = coreutil.read_solutions(Path("data/parameter/evo") / f'popsize{suffix}')

        colors = [cmap(int(i * cmap.N/10)) for i in range(10)]
        evaluation.show_fitness_datasets([(to_fitness(fitness_history_list), colors[0], "Size of the population: 1", None, None),
                                        (to_fitness(fitness_history_list2), colors[1], "Size of the population: 2", None, None),
                                        (to_fitness(fitness_history_list3), colors[2], "Size of the population: 4", None, None),
                                        (to_fitness(fitness_history_list4), colors[3], "Size of the population: 8", None, None),
                                        (to_fitness(fitness_history_list5), colors[4], "Size of the population: 12", None, None),
                                        (to_fitness(fitness_history_list6), colors[5], "Size of the population: 16", None, None),
                                        (to_fitness(fitness_history_list7), colors[6], "Size of the population: 24", None, None),
                                        (to_fitness(fitness_history_list8), colors[7], "Size of the population: 30", None, None),
                                        (to_fitness(fitness_history_list9), colors[8], "Size of the population: 60", None, None),
                                        (to_fitness(fitness_history_list10), colors[9], "Size of the population: 120", None, None)], name=f"evo-{suffix2}-popsize.pdf")
        evaluation.show_best_fitness_as_boxplots([(to_fitness(fitness_history_list), colors[0], "1"),
                                        (to_fitness(fitness_history_list2), colors[1], "2"),
                                        (to_fitness(fitness_history_list3), colors[2], "4"),
                                        (to_fitness(fitness_history_list4), colors[3], "8"),
                                        (to_fitness(fitness_history_list5), colors[4], "12"),
                                        (to_fitness(fitness_history_list6), colors[5], "16"),
                                        (to_fitness(fitness_history_list7), colors[6], "24"),
                                        (to_fitness(fitness_history_list8), colors[7], "30"),
                                        (to_fitness(fitness_history_list9), colors[8], "60"),
                                        (to_fitness(fitness_history_list10), colors[9], "120")], x_label="Size of the population", name=f"evo-popsize-{suffix2}-boxplot.pdf")

def to_fitness(solution_set):
    return [[solution.fitness for solution in solutions] for solutions in solution_set]

def main():
    #background_evaluation()
    #parameter_optimization_evaluation_parent()
    #parameter_optimization_evaluation_parent_oc()
    #parameter_optimization_evaluation_parent_pc()
    #parameter_optimization_evaluation_popsize()
    #parameter_optimization_evaluation_popsize_oc()
    #parameter_optimization_evaluation_popsize_pc()
    #parameter_optimization_evaluation_gensize()
    #parameter_optimization_evaluation_gensize_oc()
    #parameter_optimization_evaluation_gensize_pc()
    print("Evaluation Started!")
    evaluate()
    print("Evaluation finished!")

if __name__ == "__main__":
    main()