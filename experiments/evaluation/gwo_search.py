import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import cm
import matplotlib as mpl

from eocohda.algorithm.gwo import GreyWolfOptimizer
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket
from eocohda.core.storage import EnergyStorage

def main():
    
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(50000) \
                .max_discharge(50000) \
                .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    popsize = 5
    iter_num = 10
    algorithm = GreyWolfOptimizer(evaluator, additional_pop_size=popsize, iteration_count=iter_num, resolution=15, elim_scale=0.2, with_differential=False, with_elim=False)
    best_solution = algorithm.generate_schedules(0, 1, 2, 1)
    algorithm_history = algorithm.mutation_history

    plt.rcParams.update({'font.size': 14})
    fig = plt.figure()
    ax = plt.subplot(111)
    cmap = cm.get_cmap('rainbow')
    normalize = mpl.colors.Normalize(vmin=0, vmax=iter_num)
    colors = [cmap(normalize(i)) for i in range(iter_num+1)]
    for i in range(iter_num+1):
        for j in range(popsize+3):
            solution = algorithm_history[i][j]
            x = solution.candidate[0][0]
            y = solution.candidate[0][1]
            label = "Gamma" if j > 2 else ("Alpha" if j == 0 else ("Beta" if j == 1 else "Delta"))  
            marker = "o" if j > 2 else ("*" if j == 0 else ("p" if j == 1 else "s"))  
            ax.plot(x, y, marker + "r", label=label, color=colors[i])
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    unique[0][0].set_color('black')
    unique[1][0].set_color('black')
    unique[2][0].set_color('black')
    unique[3][0].set_color('black')

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) 
    ax.legend(*zip(*unique), loc='bottom left')
    ax.set_xlabel("Loadstate 1")
    ax.set_ylabel("Loadstate 2")
    cax, _ = mpl.colorbar.make_axes(ax)
    cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
    cbar.set_label("Iteration")
    plt.show()

if __name__ == "__main__":
    main()