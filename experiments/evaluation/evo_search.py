import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import cm

from eocohda.algorithm.evo import EvoAlgorithm
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket
from eocohda.core.storage import EnergyStorage

def main():

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(50000) \
                .max_discharge(50000) \
                .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    popsize = 30
    iter_num = 15
    algorithm = EvoAlgorithm(popsize, 15, 2, iter_num, evaluator)

    algorithm.generate_schedules(0, 1, 2, 1)
    algorithm_history = algorithm.pop_history


    plt.rcParams.update({'font.size': 14})
    fig = plt.figure()
    ax = plt.subplot(111)
    color = iter(cm.rainbow(np.linspace(0,1,iter_num+1)))
    for i in range(iter_num+1):
        c = next(color)
        for j in range(popsize):
            solution = algorithm_history[i][j]
            x = solution.candidate.slots[0].load_state
            y = solution.candidate.slots[1].load_state
            ax.plot(x, y, "ro", label="Gen %s" % i, color=c)
            #ax.annotate("(%i, %s)" % (solution.id, str(solution.parent_ids)), (x, y))
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) 
    ax.legend(*zip(*unique), loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()

if __name__ == "__main__":
    main()