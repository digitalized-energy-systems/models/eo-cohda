
from pathlib import Path
import logging
from typing import List
import numpy as np
import os
import glob
import decimal
from matplotlib import cm
import matplotlib.pyplot as plt
import re

import eocohda.core.util as util
import eocohda.evaluation.common as evaluation
import eocohda.evaluation.util as util_eval
from eocohda.evaluation.cohda.base_big import BaseBig
from eocohda.evaluation.cohda.base_small import BaseSmall
from eocohda.evaluation.cohda.evobase_normsmall import EvoNormSmallBase
from eocohda.evaluation.cohda.evobase_normbig import EvoNormBigBase
from eocohda.evaluation.cohda.evobase_paretobig import EvoParetoBigBase
from eocohda.evaluation.cohda.evobase_paretosmall import EvoParetoSmallBase
from eocohda.evaluation.cohda.evonormsmall import EvoNormSmall
from eocohda.evaluation.cohda.evonormbig import EvoNormBig
from eocohda.evaluation.cohda.evoparetobig import EvoParetoBig
from eocohda.evaluation.cohda.evoparetosmall import EvoParetoSmall
from eocohda.evaluation.cohda.gwonormbig import GWONormBig
from eocohda.evaluation.cohda.gwonormsmall import GWONormSmall
from eocohda.evaluation.cohda.gwoparetobig import GWOParetoBig
from eocohda.evaluation.cohda.gwoparetosmall import GWOParetoSmall
from eocohda.evaluation.cohda.fireflynormbig import FireFlyNormBig
from eocohda.evaluation.cohda.fireflynormsmall import FireFlyNormSmall
from eocohda.evaluation.cohda.fireflyparetobig import FireFlyParetoBig
from eocohda.evaluation.cohda.fireflyparetosmall import FireFlyParetoSmall

from eocohda.cohdarun import COHDARun

ES_LARGE = ["LARGE_INDUSTRY_FIVE", "LARGE_INDUSTRY_FOUR", "LARGE_INDUSTRY_THREE", "LARGE_INDUSTRY_TWO", "LARGE_INDUSTRY_ONE", "PSW"]
ES_SMALL = ["SMALL_HOUSE_PV_ONE", "SMALL_HOUSE_PV_TWO", "SMALL_HOUSE_PV_THREE", "SMALL_INDUSTRY"]
NAME_REPLACER = {
    "ONE" : "1",
    "TWO" : "2",
    "THREE" : "3",
    "FOUR" : "4",
    "FIVE" : "5",
    "PV" : "solar power plant",
    "PSW" : "Pump storage station",
    "LARGE INDUSTRY" : "Big industry",
    "SMALL INDUSTRY" : "Small industry",
    "SMALL HOUSE" : "Small household with",
}

def base_small(add_offset):
    base_small = COHDARun(BaseSmall(), add_port=add_offset)
    base_small.main()

def base_big(add_offset):
    base_big = COHDARun(BaseBig(), add_port=add_offset)
    base_big.main()

def base_evo_norm_small(add_offset):
    evonormsmall = COHDARun(EvoNormSmallBase(), add_port=add_offset)
    evonormsmall.main()

def base_evo_norm_big(add_offset):
    evonormbig = COHDARun(EvoNormBigBase(), add_port=add_offset)
    evonormbig.main()

def base_evo_pareto_big(add_offset):
    evoparetobig = COHDARun(EvoParetoBigBase(), add_port=add_offset)
    evoparetobig.main()

def base_evo_pareto_small(add_offset):
    evoparetosmall = COHDARun(EvoParetoSmallBase(), add_port=add_offset)
    evoparetosmall.main()

def evo_norm_small():
    evonormsmall = COHDARun(EvoNormSmall())
    evonormsmall.main()

def evo_norm_big(add_offset):
    evonormbig = COHDARun(EvoNormBig(), add_port=add_offset)
    evonormbig.main()

def evo_pareto_big():
    evoparetobig = COHDARun(EvoParetoBig())
    evoparetobig.main()

def evo_pareto_small():
    evoparetosmall = COHDARun(EvoParetoSmall())
    evoparetosmall.main()

def gwo_norm_big():
    gwonormbig = COHDARun(GWONormBig())
    gwonormbig.main()

def gwo_norm_small():
    gwonormsmall = COHDARun(GWONormSmall())
    gwonormsmall.main()

def gwo_pareto_big():
    gwoparetobig = COHDARun(GWOParetoBig())
    gwoparetobig.main()

def gwo_pareto_small():
    gwoparetosmall = COHDARun(GWOParetoSmall())
    gwoparetosmall.main()

def firefly_norm_big():
    fireflynormbig = COHDARun(FireFlyNormBig())
    fireflynormbig.main()

def firefly_norm_small():
    fireflynormsmall = COHDARun(FireFlyNormSmall())
    fireflynormsmall.main()

def firefly_pareto_big():
    fireflyparetobig = COHDARun(FireFlyParetoBig())
    fireflyparetobig.main()

def firefly_pareto_small():
    fireflyparetosmall = COHDARun(FireFlyParetoSmall())
    fireflyparetosmall.main()

def main(port_offset):
    print("Evaluation finished!")
    
    add_offset = 3*port_offset
    #for _ in range(10):
    base_small(add_offset)
    base_big(add_offset)
    base_evo_norm_small(add_offset)
    base_evo_norm_big(add_offset)
    base_evo_pareto_big(add_offset)
    base_evo_pareto_small(add_offset)
    #evo_norm_big(add_offset)


def evaluate_all():
    #print_avg_std_for(['EvoNormBig', 'EvoParetoBig', 'BaseEvoNormBig', 'BaseEvoParetoBig', "BaseBig"], ES_LARGE)
    #print_avg_std_for(['EvoNormSmall', 'EvoParetoSmall', "BaseEvoNormSmall", "BaseEvoParetoSmall", "BaseSmall"], ES_SMALL)
    
    
    generate_cohda_fitness_for("large", [("BaseBig1662404132.6536555.hdf", "Baseline Sampling"), ("BaseEvoNormBig1662412552.8784528.hdf", "Baseline EA-N"), 
                                         ("BaseEvoParetoBig1662416652.6896098.hdf", "Baseline EA-P"), ("EvoNormBig1628760875.167799.hdf", "GABHYME-N"), ("EvoParetoBig1628588596.2802796.hdf", "GABHYME-P")])
    generate_cohda_fitness_for("small", [("BaseSmall1662397825.8446531.hdf", "Baseline Sampling"), ("BaseEvoNormSmall1662408679.851365.hdf", "Baseline EA-N"), 
                                         ("BaseEvoParetoSmall1662421210.6493042.hdf", "Baseline EA-P"), ("EvoNormSmall1628682607.0532055.hdf", "GABHYME-N"), ("EvoParetoSmall1628844418.5889618.hdf", "GABHYME-P")])
    generate_solution_fitness_for("large", [("1662412552.8784528", "Baseline EA-N", "BASE_EVO_NORM"), ("1628760875.167799", "GABHYME-N", "EVO_NORM")])
    generate_solution_fitness_for("small", [("1662408679.851365", "Baseline EA-N", "BASE_EVO_NORM"), ("1628682607.0532055", "GABHYME-N", "EVO_NORM")])
    
    #generate_solution_fitness_fronts_for("large", [("1661521272.207651", "Baseline EA-P", "BASE_EVO_PARETO"), ("1628588596.2802796", "GABHYME-P", "EVO_PARETO")])
    #generate_solution_fitness_fronts_for("small", [("1661525263.360855", "Baseline EA-P", "BASE_EVO_PARETO"), ("1628844418.5889618", "GABHYME-P", "EVO_PARETO")])
    #evaluate_file_set("BASE", "BaseBig*", "large")
    #evaluate_file_set("BASE", "BaseSmall*", "small")
    #evaluate_file_set("EVO_NORM", "BaseEvoNormBig*", "large")
    #evaluate_file_set("EVO_NORM", "BaseEvoNormSmall*", "small")
    #evaluate_file_set("EVO_PARETO", "BaseEvoParetoBig*", "large")
    #evaluate_file_set("EVO_PARETO", "BaseEvoParetoSmall*", "small")
    evaluate("EVO_NORM", "EvoNormBig1628760875.167799.hdf", "large")
    evaluate("EVO_NORM", "EvoNormSmall1628682607.0532055.hdf", "small")
    evaluate("EVO_PARETO", "EvoParetoBig1628588596.2802796.hdf", "large")
    evaluate("EVO_PARETO", "EvoParetoSmall1628844418.5889618.hdf", "small")
    
    #evaluate("BASE_EVO_PARETO", "BaseEvoParetoBig1661944155.6029608.hdf", "large")
    #evaluate("BASE_EVO_PARETO", "BaseEvoParetoSmall1661948620.5574734.hdf", "small")
    #evaluate("BASE_EVO_NORM", "BaseEvoNormBig1661940091.571685.hdf", "large")
    #evaluate("BASE_EVO_NORM", "BaseEvoNormSmall1661936232.3166869.hdf", "small")
    

    evalute_rob("large", ['EvoNormBig', 'EvoParetoBig', 'BaseEvoNormBig', 'BaseEvoParetoBig', "BaseBig"])
    evalute_rob("small", ['EvoNormSmall', 'EvoParetoSmall', "BaseEvoNormSmall", "BaseEvoParetoSmall", "BaseSmall"])
    
    print("Evaluation finished!")


def print_avg_std_for(files_to_eval, agent_list):
    cs_list = []
    ts_list = []
    sl_list = []
    pattern = re.compile(r'(?<!^)(?=[A-Z])')
    for file in files_to_eval:
        cs, ts, _, sl = read_fitness(file, with_ind=True, agent_list=["_".join(pattern.sub('_', file).upper().split("_")[:-1]) + "_" + agent for agent in agent_list])
        cs_list.append(cs)
        ts_list.append(ts)
        sl_list.append((file, sl))
    ff_rates = [[evaluation.calc_ff_rate(cs_list[i][j], ts_list[i][j]) for j in range(len(cs_list[i]))] for i in range(len(cs_list))]
    for i, rate in enumerate(ff_rates):
        print(f"avg: {files_to_eval[i]} {np.average(rate)}")
        print(f"median: {files_to_eval[i]} {np.median(rate)}")
    std_devs = evaluation.standard_deviation(np.array(ff_rates).transpose())
    for i, rate in enumerate(std_devs):
        print(f"sigma: {files_to_eval[i]} {rate}")
    print(calc_sq_rates(sl_list, agent_list))

def calc_sq_rates(sl_list, agent_list):
    sq_rates = {}
    sq_rates_avg = {}
    pattern = re.compile(r'(?<!^)(?=[A-Z])')
    for agent in agent_list:
        all_fitness_agent = {}
        for file, sl in sl_list:
            agent_refined = "_".join(pattern.sub('_', file).upper().split("_")[:-1]) + "_" + agent
            avg_fitness = 0
            for agent_to_si_hdf_agents in sl:
                solution = agent_to_si_hdf_agents[agent_refined]
                avg_fitness += solution.fitness_blackbox[0] if solution.fitness_blackbox is not None else (solution.fitness[0] if isinstance(solution.fitness, list) else solution.fitness)
            avg_fitness = avg_fitness / len(sl)
            all_fitness_agent[file] = avg_fitness
        sq_rates_agent = calc_sq_rate(all_fitness_agent)
        sq_rates[agent] = sq_rates_agent
        for file, sl in sl_list:
            if file in sq_rates_avg:
                sq_rates_avg[file] += sq_rates[agent][file] / len(agent_list)
            else:
                sq_rates_avg[file] = sq_rates[agent][file] / len(agent_list)
    return sq_rates, sq_rates_avg

def calc_sq_rate(all_fitness_agent):
    result = {}
    all_fitness_agent_list = list(all_fitness_agent.values())
    all_fitness_agent_list.sort(reverse=True)
    max_v = all_fitness_agent_list[0]
    for k,v in all_fitness_agent.items():
        result[k] = v / max_v
    return result

def evalute_rob(testcase, files_to_eval):
    base_path = "robust"
    cs_list = []
    ts_list = []
    for file in files_to_eval:
        cs, ts, _, __ = read_fitness(file)
        cs_list.append(cs)
        ts_list.append(ts)
    files_label_map = {'EvoNormSmall': 'GABHYME-N', 
        'EvoParetoSmall': 'GABHYME-P', 
        'EvoNormBig': 'GABHYME-N', 
        'EvoParetoBig': 'GABHYME-P',

        'BaseEvoNormBig': 'EA-N', 
        'BaseEvoParetoBig': 'EA-P',
        'BaseEvoNormSmall': 'EA-N', 
        'BaseEvoParetoSmall': 'EA-P',
        'BaseBig': 'Sample', 
        'BaseSmall': 'Sample'}
    colors = cm.get_cmap('plasma')

    Path(base_path).mkdir(parents=True, exist_ok=True)
    evaluation.show_best_fitness_as_boxplots([([[evaluation.calc_ff_rate(cs_list[i][j], ts_list[i][j])] for j in range(len(cs_list[i]))], 
                                                colors(int(i*colors.N/len(cs_list))), 
                                                files_label_map[files_to_eval[i]]) for i in range(len(cs_list))], 
                                            x_label='method', 
                                            name=base_path + f"/cohda-robustness-{testcase}.pdf",
                                            font_size=58,
                                            y_label='fulfillment rate in %')

def evaluate_file_set(test_case_string, hdf_file_contain, test_case_type):
    base_path = test_case_string.replace("_", "-").lower() + "-" + test_case_type
    os.makedirs(base_path, exist_ok=True)
    import glob
    for i, hdf_path in enumerate(glob.glob("results/" + hdf_file_contain)):
        evaluate(test_case_string, hdf_path, test_case_type, i)


def evaluate(test_case_string, hdf_file, test_case_type, num=0):
    pareto = "PARETO" in test_case_string
    storage_list = ES_LARGE if test_case_type == "large" else ES_SMALL
    storage_list = [test_case_string + "_" + storage for storage in storage_list]
    base_path = test_case_string.replace("_", "-").lower() + "-" + test_case_type
    os.makedirs(base_path, exist_ok=True)
    
    
    for storage_name in storage_list:
        storage_path = (base_path + "/" + "-".join(storage_name.split("_")[2:]).lower()).replace("large-", "").replace("small-", "")
        storage_path_solution = storage_path + "-solution.pdf"
        storage_path_fitness = storage_path + "-fitness.pdf"
        show_solutions(storage_name, hdf_file, storage_path_solution, num)
        show_fronts(storage_name, pareto, storage_path_fitness, num)
    show_fitness_sets(storage_list, pareto, base_path, num)
    show_cs(hdf_file, base_path, num)

def generate_solution_fitness_for(scenario_type, input_tuples):
    mypath = Path("data/solutions")
    fitness_datasets = []
    cmap = plt.get_cmap("tab10")
    for i, input_tuple in enumerate(input_tuples):
        file_name = input_tuple[0]
        label_raw = input_tuple[1]
        alg_type = input_tuple[2]
        storage_list = ES_LARGE if scenario_type == "large" else ES_SMALL
        storage_list = [alg_type + "_" + storage for storage in storage_list]
        for j, storage in enumerate(storage_list):
            if not ((mypath / storage).exists()):
                solutions = read_solution(storage, 0)
            else:
                solutions = read_solution_alt(storage, id=file_name)
            solutions = list(solutions.values())[0:]
            solutions_fitness = [[max(0, solution.fitness) for solution in solutions] + [max(0, solutions[-1].fitness)]]
            color = cmap(int(j*cmap.N/len(storage_list)))
            label = " ".join(storage.split('_')).lower().replace("large", "").replace("evo", "").replace("base", "").replace("norm", "").replace("pareto", "").replace("psw", "PSP").strip()
            fitness_datasets.append((solutions_fitness, color, label, (0,(0.1,2)) if "Base" in label_raw else "-", list(range(len(solutions_fitness[0]) - 1))))
    
    max_x = max([data[4][-1] for data in fitness_datasets])
    for dataset in fitness_datasets:
        dataset[4].append(max_x)

    base = Path(scenario_type)
    base.mkdir(exist_ok=True, parents=True)
    evaluation.show_fitness_datasets(fitness_datasets, name=base / "fitness-individuals-with-baseline.pdf", legend_loc=["upper left", "lower left"], legend_bbox=(1.0, 0.75), add_width=5, sci_ticks=False, part_legend=True, lw=12)

def generate_cohda_fitness_for(scenario_type, input_tuples):
    mypath = Path("results")
    fitness_datasets = []
    cmap = plt.get_cmap("tab10")
    for i, input_tuple in enumerate(input_tuples):
        file_name = input_tuple[0]
        label = input_tuple[1]
        _, _, _, dap = util_eval.read_cs_from_hdf(mypath / file_name)
        dap.sort(key=lambda e: e[0])
        x = np.array([line['t'] for line in dap])
        x = list(x - x[0])
        y = [[line['perf'] for line in dap] + [dap[-1]['perf']]]
        fitness_datasets.append((y, cmap(int(i*cmap.N/len(input_tuples))), label, (0,(0.1,2)) if "Base" in label else "-", x))

    max_x = max([data[4][-1] for data in fitness_datasets])
    for dataset in fitness_datasets:
        dataset[4].append(max_x)

    base = Path(scenario_type)
    base.mkdir(exist_ok=True, parents=True)
    evaluation.show_fitness_datasets(fitness_datasets, name=base / "fitness-system-with-baseline.pdf", legend_loc="center left", legend_bbox=(1.0, 0.5), add_width=5, x_label="time (s)", lw=12)

def read_fitness(hdf_file, with_ind=False, agent_list=None):
    mypath = Path("results")
    all_files = glob.glob(os.path.expanduser(mypath / (hdf_file+"*")))
    hdf_raw = (''.join([i for i in hdf_file if not i.isdigit()])).replace('hdf','')
    all_matching_hdf = list(filter(lambda f: f.find(hdf_raw) != -1, all_files))
    all_matching_hdf.sort()
    best_fitnesses = []
    ts_list = []
    cs_list = []
    solutions_list = []
    for i, hdf in enumerate(all_matching_hdf):
        cs, agents, ts, dap = util_eval.read_cs_from_hdf(hdf)
        best_fitnesses.append(dap[-1]['perf'])
        cs_list.append(cs)
        ts_list.append(ts)
        if with_ind:
            solutions = {}
            for storage_name in agent_list:
                solutions[storage_name] = (read_solution(storage_name, i, hdf[[x.isdigit() for x in hdf].index(True):-4]))[schedule_of(storage_name, agents)]
            solutions_list.append(solutions)
    return cs_list, ts_list, [[fit] for fit in best_fitnesses], solutions_list

def show_cs(hdf_file, base_path, num=-1):
    cs, agents, ts, dap = util_eval.read_cs_from_hdf(Path("results") / hdf_file)
    dap.sort(key=lambda e: e[0])
    evaluation.show_cluster_schedule(cs, agents, ts, name=base_path + "/cs.pdf")
    num_str = num if num != -1 else ""
    evaluation.show_fitness([[line['perf'] for line in dap]], fitness_x=[line['t'] for line in dap], label="COHDA-performance", x_label="time", name=base_path + f"/fitness{num_str}.pdf")

def read_solution(storage_name, num, id=None):
    mypath = Path("data/solutions")    
    if ((mypath / storage_name).exists()):
        return read_solution_alt(storage_name, id)

    all_files = glob.glob(os.path.expanduser(mypath / "*"))
    sorted_by_modified = list(filter(lambda f: f.find(storage_name) != -1, sorted(all_files, key=lambda t: os.stat(t).st_mtime)))
    return util.read_solutions(sorted_by_modified[num])

def read_solution_alt(storage_name, id):
    mypath = Path("data/solutions/" + storage_name + "/" + id)
    if not mypath.exists():
        return []
    return util.read_solutions(mypath)

def show_solutions(storage_name, hdf_file, storage_path, num):
    _, agents, _, _ = util_eval.read_cs_from_hdf(Path("results") / hdf_file)
    
    solutions = read_solution(storage_name, num)

    if "PSW" in storage_name:
        energy_values = np.array(util_eval.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")) / 4000000
        solution = solutions[schedule_of(storage_name, agents)]
        evaluation.show_solution(solution.candidate, 0, preprocess_fitness(solution.fitness), name=storage_path,
                                alt_data=[energy_values],
                                alt_label=["market source"],
                                alt_y_label="Euro/MWh")
    elif "HOUSE" in storage_name:
        day = -1
        for (k, v) in {"ONE" : 50, "TWO" : 80, "THREE" : 110}.items():
            if k in storage_name:
                day = v
        hh_profile = util_eval.read_household_profile([Path("data/household/") / "PL1.csv"], day)
        pv_profile = util_eval.read_pv_kronberg_by_day(Path("data/pv/") / "kronberg.txt", day)
        solution = solutions[schedule_of(storage_name, agents)]
        evaluation.show_solution(solutions[schedule_of(storage_name, agents)].candidate, 0.1, preprocess_fitness(solution.fitness), name=storage_path,
                                alt_data=[hh_profile, pv_profile],
                                alt_label=["load profile", "SPP profile"],
                                alt_y_label="power in W")
    else:
        day = 130
        size = "large" if "LARGE" in storage_name else "small"
        for (k, v) in {"ONE" : 130, "TWO" : 131, "THREE" : 132, "FOUR" : 133, "FIVE" : 134}.items():
            if k in storage_name:
                day = v
        hh_profile = util_eval.read_industry_profile(Path("data/industry/") / (size + "_industry.csv"), day)
        solution = solutions[schedule_of(storage_name, agents)]
        evaluation.show_solution(solutions[schedule_of(storage_name, agents)].candidate, 0.1, preprocess_fitness(solution.fitness), name=storage_path,
                                alt_data=[hh_profile],
                                alt_label=["load profile"],
                                alt_y_label="power in W")

def preprocess_fitness(fitness):
    if isinstance(fitness, List):
        return [float(round(decimal.Decimal(f), 3)) for f in fitness]
    return float(round(decimal.Decimal(fitness), 3))

def schedule_of(storage_name, agents):
    for i in range(len(agents)):
        if storage_name in agents[i]['Name'].decode("ascii", "replace"):
            return agents[i]["Internal Schedule ID"]
    return None

def show_fitness_sets(storage_names, pareto, base_path, num):
    if pareto:
        return
    else:
        color_map = {0:"red",1:"blue",2:"green",3:"purple",4:"cyan",5:"orange",6:"yellow"}
        solutions_array = [list(read_solution(storage_name, num).values())[1:] for storage_name in storage_names]
        fitness_set = [([[solution.fitness for solution in solution_list]], color_map[i], " ".join(storage_names[i].split('_')).lower()
        .replace("large", "")
        .replace("evo", "")
        .replace("norm", "")
        .replace("pareto", "").replace("psw", "PSP").strip(), None, None) for (solution_list, i) in zip(solutions_array, range(len(solutions_array)))]
        evaluation.show_fitness_datasets(fitness_set, name=base_path + "/all_sol_fit.pdf")

def generate_solution_fitness_fronts_for(scenario_type, input_tuples):
    mypath = Path("data/solutions")
    datasets = []
    for i, input_tuple in enumerate(input_tuples):
        file_name = input_tuple[0]
        label_raw = input_tuple[1]
        alg_type = input_tuple[2]
        storage_list = ES_LARGE if scenario_type == "large" else ES_SMALL
        storage_list = [alg_type + "_" + storage for storage in storage_list]
        for j, storage in enumerate(storage_list):
            solutions = read_solution(storage, num)
            datasets.append((list(solutions.values())[0:len(solutions)], "D" if "Base" in label_raw else "."))

    base = Path(scenario_type)
    base.mkdir(exist_ok=True, parents=True)

    evaluation.show_fitness_fronts(datasets, "Pareto front histories", name=base / "pareto-fronts.pdf")

def show_fronts(storage_name, pareto, storage_path, num):
    solutions = read_solution(storage_name, num)
    solution_list = list(solutions.values())[1:len(solutions)]
    if pareto:
        evaluation.show_fitness_set(solution_list, to_simple_storage_name(storage_name), name=storage_path)
    else:
        evaluation.show_fitness([[solution.fitness for solution in solution_list]], fitness_x=[solution.iteration for solution in solution_list], label=to_simple_storage_name(storage_name), name=storage_path)

def to_simple_storage_name(storage_name):
    pre = " ".join(storage_name.split("_")[2:])
    for key, value in NAME_REPLACER.items():
        pre = pre.replace(key, value)
    return pre

def ts():
    cs, agents, ts, dap = util_eval.read_cs_from_hdf(Path("results") / "EvoNormBig.hdf5")
    evaluation.show_cluster_schedule(cs, agents, ts, name="ts-large.pdf")
    cs, agents, ts, dap = util_eval.read_cs_from_hdf(Path("results") / "EvoNormSmall.hdf5")
    evaluation.show_cluster_schedule(cs, agents, ts, name="ts-small.pdf")

if __name__ == "__main__":
    import sys
    num = 0
    if len(sys.argv) > 1:
        num = int(sys.argv[1])
    logging.basicConfig(level=logging.DEBUG)
    #
    #main(num)
    evaluate_all()
    #ts()