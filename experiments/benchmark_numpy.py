import numpy as np
import time

npArray = np.array(range(9600))
pyList = list(range(9600))
targetVar = 2

# Warm UP
for j in range(1000):
    for i in range(len(npArray)):
        targetVar = 2+npArray[i]
        targetVar = 1+pyList[i]+targetVar

# Test READ
ctime = time.time()
for j in range(1000):
    for i in range(len(npArray)):
        targetVar = npArray[i]
print(time.time() - ctime)

ctime = time.time()
for j in range(1000):
    for i in range(len(pyList)):
        targetVar = pyList[i]
print(time.time() - ctime)

# Test WRITE
ctime = time.time()
for j in range(1000):
    for i in range(len(npArray)):
        targetVar += 1
        npArray[i] = targetVar + 1
print(time.time() - ctime)

ctime = time.time()
for j in range(1000):
    for i in range(len(pyList)):
        targetVar += 1
        pyList[i] = targetVar + 1
print(time.time() - ctime)
