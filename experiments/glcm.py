import numpy as np
import skimage.feature as ski 
from matplotlib import cm 
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans


def blockshaped(arr, nrows, ncols):
    h, w = arr.shape
    return (arr.reshape(h//nrows, nrows, -1, ncols)
               .swapaxes(1,2)
               .reshape(-1, nrows, ncols))

def main():
    
    planning_horizon = 10
    width = 100
    height = 100
    heatmap_part_grid_cell_size = 10
    
    # Plain Heatmaü
    Y, X = np.meshgrid(np.linspace(0, 1, width), np.linspace(0, planning_horizon - 1, height))
    heatmap = np.load("data/heatmap/heatmap_100000.npy")
    z_min, z_max = np.abs(heatmap).min(), np.abs(heatmap).max()
    fig = plt.figure()
    ax = plt.subplot(1, 3, 1)
    c = ax.pcolormesh(X,Y, heatmap, cmap='coolwarm', vmin=z_min, vmax=z_max)
    #c = ax.pcolormesh(np.array([[0, 0, 0], [1, 0, 0]]), cmap='coolwarm', vmin=0, vmax=1)
    ax.set_title('Heatmap')
    fig.colorbar(c, ax=ax)

    # Partition Heatmap
    heatmaps = blockshaped(heatmap, heatmap_part_grid_cell_size, heatmap_part_grid_cell_size)
    preprocess = lambda x: (heatmap_part_grid_cell_size-1 / np.abs(heatmaps).max()) * x
    heatmaps = preprocess(heatmaps).astype(dtype=np.uint8)
    glcms = [ski.greycomatrix(heatmap_part, [3], [0], levels=heatmap_part_grid_cell_size) for heatmap_part in heatmaps]
    xs = []
    ys = []
    for glcm in glcms:
        xs.append(ski.greycoprops(glcm, 'homogeneity')[0, 0])
        ys.append(ski.greycoprops(glcm, 'ASM')[0, 0])
    points = np.array((xs, ys)).T
    kmeans = KMeans(n_clusters=3).fit(points)
    cluster_assignments = kmeans.predict(points)

    # Plot k-means
    cluster_one_x = [points[i][0] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 0]
    cluster_two_x = [points[i][0] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 1]
    cluster_one_y = [points[i][1] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 0]
    cluster_two_y = [points[i][1] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 1]
    cluster_three_x = [points[i][0] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 2]
    cluster_three_y = [points[i][1] for i in range(len(cluster_assignments)) if cluster_assignments[i] == 2]

    ax = plt.subplot(1, 3, 2)
    ax.plot(cluster_one_x, cluster_one_y, 'bo', label="cluster 1", color="b")
    ax.plot(cluster_two_x, cluster_two_y, 'bo', label="cluster 2", color="r")
    ax.plot(cluster_three_x, cluster_three_y, 'bo', label="cluster 2", color="g")
    ax.set_xlabel('GLCM Homogeneity')
    ax.set_ylabel('GLCM ASM')
    ax.legend()

    for i in range(100//heatmap_part_grid_cell_size):
        for j in range(100//heatmap_part_grid_cell_size):
            assignment_index = j + i * heatmap_part_grid_cell_size
            heatmap[i*heatmap_part_grid_cell_size:(i+1)*heatmap_part_grid_cell_size, 
                    j*heatmap_part_grid_cell_size:(j+1)*heatmap_part_grid_cell_size] = np.full((heatmap_part_grid_cell_size, heatmap_part_grid_cell_size), 
                    xs[assignment_index]*ys[assignment_index])
    ax = plt.subplot(1, 3, 3)
    c = ax.pcolormesh(X, Y, heatmap, cmap='coolwarm', vmin=0, vmax=1)
    ax.set_title('Heatmap')
    fig.colorbar(c, ax=ax)

    plt.show()

if __name__ == "__main__":
    main()