from pathlib import Path

from gekko import GEKKO
import numpy as np
import matplotlib.pyplot as plt
import eocohda.evaluation.util as util

def main():

    m = GEKKO(remote=False)  # Initialize gekko
    m.options.SOLVER=1  # APOPT is an MINLP solver
    m.options.SOLVER=2  # APOPT is an MINLP solver
    n = 30
    initial_load = m.Const(0.0)
    # Initialize variables
    solution = [m.Var(value=0.1, lb=0, ub=1) for _ in range(n)]
    # Equations
    capacity = m.Const(6000000)
    max_charge = m.Const(325000)
    max_discharge = m.Const(325000)
    eff_charge = m.Const(np.sqrt(0.8))
    eff_discharge = m.Const(np.sqrt(0.8))
    for i in range(n):
        first = initial_load if i == 0 else solution[i-1]
        second = solution[i]
        m.Equation(second <= first + (max_charge/capacity) * eff_charge)
        m.Equation(second >= first - (max_discharge/capacity) * 1/eff_discharge)

    energy_values = (np.array(util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")) / 4000000).tolist()[:n]

    obj_func = obj_spreading(solution, initial_load, energy_values, capacity, eff_charge, m)
    m.Minimize(obj_func)
    m.solve(disp=True, debug=True)  # Solve
    print("solution: " + str(solution))
    print("Objective: " + str(-m.options.objfcnval))
    fig, ax = plt.subplots()
    ax2 = ax.twinx()
    ax2.plot([0] + energy_values)
    plot_sol = [0.0 if i == 0 else solution[i-1].value.value[0] for i in range(n+1)]
    print(plot_sol)
    ax.plot(plot_sol)
    plt.show()

def obj_spreading(solution, initial_load, market, capacity, eff, m: GEKKO):
    diffs = [0] * len(market)
    for i in range(len(market)):
        first = initial_load if i == 0 else solution[i-1]
        second = solution[i]
        raw_diff = first - second
        #eff_mul = m.if3(raw_diff, 1/eff, eff)
        raw_diff_power = (raw_diff) * capacity
        diffs[i] = raw_diff_power #* eff_mul
    return -manualdot(diffs, market)

def obj_peak_shaving(solution, initial_load, max_power_cost_mul, market, household_profile, capacity, eff, m: GEKKO):
    diffs = [0] * len(market)
    for i in range(len(market)):
        first = initial_load if i == 0 else solution[i-1]
        second = solution[i]
        raw_diff = first - second
        #eff_mul = m.if3(raw_diff, 1/eff, eff)
        raw_diff_power = (raw_diff) * capacity
        diffs[i] = raw_diff_power  #* eff_mul
    buy_add_cost = sum(diffs)
    forecast = np.array(diffs) + household_profile
    return (household_profile.max() - forecast.max()) * max_power_cost_mul - buy_add_cost

def obj_oc(solution, initial_load, target_profile, market, capacity, eff, m: GEKKO):
    diffs = [0] * len(market)
    for i in range(len(market)):
        first = initial_load if i == 0 else solution[i-1]
        second = solution[i]
        raw_diff = first - second
        raw_diff_power = (raw_diff) * capacity
        diffs[i] = target_profile[i] - raw_diff_power 
    return -manualdot(diffs, market)

def manualdot(l_one, l_two):
    dot_p = 0
    for i in range(len(l_one)):
        dot_p += l_one[i] * l_two[i]
    return dot_p

def main2():
    m = GEKKO(remote=False)  # Initialize gekko
    m.options.SOLVER=1  # APOPT is an MINLP solver

    eff = 0.8
    raw_diff = m.Var(value=1)
    m.Minimize(eff ** m.if3(raw_diff, -1, 1))
    m.solve(disp=True, debug=True)  # Solve
    print(raw_diff.value.value)


if __name__ == "__main__":
    main()