from statistics import mean 
import argparse

import numpy as np
from scipy.interpolate import interp2d
import skimage.feature as ski

from eocohda.algorithm.evo import EvoAlgorithm, EvoAlgorithmParetoNSGA, EvoAlgorithmBlackbox
from eocohda.core.algorithm import SolutionGenerator
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket, RandomMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy, Schedule, Slot
import eocohda.core.util as util

def main():

    parser = argparse.ArgumentParser(description='Plot solution of algorithm')
    parser.add_argument('--sample', dest='sample', type=int, default=0)
    args = parser.parse_args()

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.5) \
                .self_discharge(0.001) \
                .capacity(50000) \
                .charge_efficiency(0.9) \
                .discharge_efficiency(0.85) \
                .max_charge(50000) \
                .max_discharge(50000) \
                .build()

    sample_num = args.sample
    planning_horizon = 10
    width = 100
    height = 100
    market = RandomMarket(planning_horizon)
    evaluator = SpreadingEvaluator(market, storage)
    solution_gen = SolutionGenerator(evaluator)

    samples = []
    fitnessValues = []
    for i in range(sample_num):
        samples.append(solution_gen.generate(planning_horizon))
        fitnessValues.append(evaluator.eval(samples[i]))

    fitness = np.array(fitnessValues)
    fitness = (fitness + np.abs(np.min(fitness)))
    fitness /= np.max(fitness)

    Y, X = np.meshgrid(np.linspace(0, 1, width), np.linspace(0, planning_horizon - 1, height))
    Z = np.zeros((width, height))

    # Generate Heatmap with the samples as data base
    nearest_count = int(sample_num / 10)
    for y in range(len(Y[0])):
        for x in range(len(X)):
            nearest = []
            for i in range(len(samples)):
                for j in range(samples[i].size()):
                    slot = samples[i].get_slot(j)
                    interval_num = j
                    slot_state = slot.load_state * planning_horizon
                    nearest.append((np.linalg.norm(np.array([interval_num, slot_state] - np.array([X[x][y], Y[x][y] * planning_horizon]))), fitness[i]))
            nearest.sort(key=lambda x: x[0], reverse=False)
            top_nearest = nearest[0:nearest_count]
            Z[x][y] = mean([tupel[1] for tupel in top_nearest])

    np.save("heatmap.npy", Z)
    util.write_solutions(market, "market")

if __name__ == "__main__":
    main()