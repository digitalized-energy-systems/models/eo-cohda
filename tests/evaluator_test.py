from pathlib import Path

from interface import implements

import eocohda.core.util as util
from eocohda.core.evaluator import SpreadingEvaluator, SpreadingEvaluatorPlugin, EvaluatorModel, OwnConsumptionEvaluatorPlugin, PeakDemandCuttingEvaluatorPlugin, SelfDischargeEvaluatorPlugin
from eocohda.core.market import UserMarket, TestMarket
from eocohda.core.schedule import Schedule, Strategy, Slot
from eocohda.core.storage import EnergyStorage
import eocohda.evaluation.cohda.local as local

def test_eval_self_discharge():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(0.7, 0, 15) \
        .add_slot_duration(0.6, 30) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0.1) \
        .capacity(1000) \
        .charge_efficiency(0.8) \
        .max_charge(300) \
        .max_discharge(300) \
        .discharge_efficiency(0.85) \
        .build()
    default_evaluator = SpreadingEvaluatorPlugin(TestMarket(), storage)
    evaluator_model = EvaluatorModel(storage, evaluator_plugins=[default_evaluator, SelfDischargeEvaluatorPlugin(storage, TestMarket())])

    # WHEN
    fitness = evaluator_model.eval(schedule)

    # THEN
    assert fitness == -133.2828741369708

def test_eval():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(0.7, 0, 15) \
        .add_slot_duration(0.6, 30) \
        .add_slot_duration(0.45, 30, Strategy.SELL) \
        .add_slot_duration(0.5, 30, Strategy.BUY) \
        .add_slot_duration(0.7, 30, Strategy.BLOCKED_CHARGE) \
        .add_slot_duration(0.3, 30, Strategy.BLOCKED_DISCHARGE) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(400) \
        .max_charge(400) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)

    # WHEN
    fitness = default_evaluator.eval(schedule)

    # THEN
    assert fitness == 64.99999999999878

def test_eval_constraint_max_charge_discharge():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(500, 30) \
        .add_slot_duration(100, 30, Strategy.SELL) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(10000) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(400) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)

    # WHEN
    fitness = default_evaluator.eval(schedule)

    # THEN
    assert fitness == -9999999999

def test_eval_constraint_load():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(100, 30) \
        .add_slot_duration(100, 30, Strategy.SELL) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(100) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(400) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)

    # WHEN
    fitness = default_evaluator.eval(schedule)

    # THEN
    assert fitness == -9999999999

def test_eval_self_consumption():
    # GIVEN
    schedule_builder = Schedule.builder() \
        .add_slot(0.5, 0, 15) \
        .add_slot_duration(0.3, 15) \
        .add_slot_duration(0.8, 15, Strategy.BUY) \
        .add_slot_duration(0.7, 15, Strategy.SELL)
    for i in range(46):
        schedule_builder.add_slot_duration(0.5, 15, Strategy.OWN_DISCHARGE)
        schedule_builder.add_slot_duration(0.7, 15, Strategy.BUY)
    schedule = schedule_builder.build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    test_market = TestMarket()
    default_evaluator = SpreadingEvaluatorPlugin(test_market, storage)
    hh_profile = util.read_profile(Path("data/household/") / "example.csv")
    der_profile = util.read_profile(Path("data/pv/") / "example.csv")
    cons_eval = OwnConsumptionEvaluatorPlugin(UserMarket(test_market), storage, hh_profile, der_profile)
    eval_model = EvaluatorModel(storage, evaluator_plugins=[cons_eval, default_evaluator])


    # WHEN
    fitness = eval_model.eval(schedule)

    # THEN
    assert fitness == -14487.999999999993

def test_eval_self_consumption_full():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(1, 0, 15) \
        .add_slot_duration(0.9, 15, Strategy.SELL) \
        .add_slot_duration(0.65, 15, Strategy.OWN_DISCHARGE) \
        .add_slot_duration(0.55, 15, Strategy.SELL) \
        .add_slot_duration(0.6, 15, Strategy.BUY) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    test_market = TestMarket()
    hh_profile = [50, 100, 300, 100, 55]
    der_profile = [100, 100, 50, 100, 100]
    cons_eval = OwnConsumptionEvaluatorPlugin(UserMarket(test_market), storage, hh_profile, der_profile)

    # WHEN
    fitness = cons_eval.eval_step(schedule, 1)

    # THEN
    assert fitness == 500.9999999999977

def test_eval_self_consumption_part():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(1, 0, 15) \
        .add_slot_duration(0.95, 15, Strategy.OWN_DISCHARGE) \
        .add_slot_duration(0.70, 15, Strategy.OWN_DISCHARGE) \
        .add_slot_duration(0.55, 15, Strategy.SELL) \
        .add_slot_duration(0.6, 15, Strategy.BUY) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    test_market = TestMarket()
    hh_profile = [50, 100, 300, 100, 55]
    der_profile = [100, 0, 50, 100, 100]
    cons_eval = OwnConsumptionEvaluatorPlugin(UserMarket(test_market), storage, hh_profile, der_profile)

    # WHEN
    fitness = cons_eval.eval_step(schedule, 1)

    # THEN
    assert fitness == 600.9999999999956

def test_penalize_not_plausible_strategy():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(1, 0, 15) \
        .add_slot_duration(0.55, 15, Strategy.SELL) \
        .add_slot_duration(0.6, 15, Strategy.SELL) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)

    # WHEN
    fitness = default_evaluator.eval(schedule)

    # THEN
    assert fitness == -9999999999

def test_penalize_not_plausible_strategy_2():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(1, 0, 15) \
        .add_slot_duration(0.55, 15, Strategy.BUY) \
        .add_slot_duration(0.6, 15, Strategy.BUY) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)

    # WHEN
    fitness = default_evaluator.eval(schedule)

    # THEN
    assert fitness == -9999999999

def test_peak_cutting_eval_plugin():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(1, 0, 15) \
        .add_slot_duration(0.95, 15, Strategy.OWN_DISCHARGE) \
        .add_slot_duration(0.70, 15, Strategy.OWN_DISCHARGE) \
        .add_slot_duration(0.55, 15, Strategy.SELL) \
        .add_slot_duration(0.60, 15, Strategy.BUY) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .max_discharge(1000) \
        .max_charge(1000) \
        .build()
    test_market = TestMarket()
    hh_profile = [50, 100, 300, 100, 55]
    cons_eval = PeakDemandCuttingEvaluatorPlugin(UserMarket(test_market), storage, hh_profile, 90)

    # WHEN
    fitness = cons_eval.eval_step(schedule, 1)

    # THEN
    assert fitness == 17451

