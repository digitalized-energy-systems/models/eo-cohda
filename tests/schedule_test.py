from eocohda.core.schedule import Schedule, Slot, Strategy

def test_builder():
    # GIVEN WHEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(300, 30) \
        .build()

    # THEN
    assert schedule.get_slot(0) == Slot(200, 0, 15)
    assert schedule.get_slot(1) == Slot(300, 15, 45)

def test_slots_in_interval():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(300, 30) \
        .build()

    # WHEN
    slots = schedule.get_slots_in_interval(1, 50)

    # THEN
    assert slots == [Slot(300, 15, 45)]

def test_slots_in_interval_overlapping():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(300, 30) \
        .build()

    # WHEN
    slots = schedule.get_slots_in_interval(1, 50, True)

    # THEN
    assert slots == [Slot(200, 0, 15), Slot(300, 15, 45)]

def test_strategy_property():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(200, 0, 15) \
        .add_slot_duration(300, 30) \
        .build()

    # WHEN
    strategy = schedule.get_slot(0).strategy

    # THEN
    assert strategy == Strategy.DISCHARGE
