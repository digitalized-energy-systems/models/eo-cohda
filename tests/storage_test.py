from eocohda.core.storage import EnergyStorage

def test_builder():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(15) \
        .load(0.5) \
        .time_constant(0.01) \
        .capacity(800) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    load = energy_storage.load

    # THEN
    assert load == 0.5

def test_charge():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(15) \
        .load(0.5) \
        .time_constant(1) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    next_load = energy_storage.charge(250)

    # THEN
    assert next_load == 0.450000015295116

def test_self_discharge():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .time_constant(1) \
        .capacity(500) \
        .charge_efficiency(1) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    energy_storage.set_self_discharge(0)
    next_load = energy_storage.charge(250)

    # THEN
    assert next_load == 0.9999999999999997

def test_discharge():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .time_constant(1) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    energy_storage.set_self_discharge(0.1)
    next_load = energy_storage.discharge(250)

    # THEN
    assert next_load == -0.10652720252711584

def test_remaining_load():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .time_constant(1) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    remaining = energy_storage.remaining_load()

    # THEN
    assert remaining == 123.67005020973188

def test_load_until_cap():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.01) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    remaining = energy_storage.load_until_cap()

    # THEN
    assert remaining == 281.9260408397623

def test_apply():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.000001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    energy = energy_storage.apply_load(0.6)

    # Then
    assert energy == 55.55586110978951

def test_apply_dis():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.000001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()

    # WHEN
    energy = energy_storage.apply_load(0.4)

    # Then
    assert energy == 42.49980874920151

def test_self_discharge_p():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.1) \
        .capacity(500) \
        .charge_efficiency(1) \
        .discharge_efficiency(1) \
        .build()

    # WHEN
    load_state = energy_storage.discharge(0)

    # THEN
    assert load_state == 0.45454545454545453

def test_discharge_eff():
    # GIVEN
    energy_storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.000001) \
        .capacity(500) \
        .charge_efficiency(0.85) \
        .discharge_efficiency(0.8) \
        .build()

    # WHEN
    load_state = energy_storage.discharge(50)

    # THEN
    assert load_state == 0.37499956249780775