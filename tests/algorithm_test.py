from eocohda.core.evaluator import SpreadingEvaluator, EvaluatorModel, OwnConsumptionEvaluatorPlugin
from eocohda.core.market import UserMarket, TestMarket
from eocohda.core.schedule import Schedule, Strategy, Slot
from eocohda.core.storage import EnergyStorage
from eocohda.core.algorithm import SolutionGenerator, SolutionRepairer


def test_generator():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(100) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(400) \
        .build()
    default_evaluator = SpreadingEvaluator(TestMarket(), storage)
    solution_generator = SolutionGenerator(default_evaluator)

    # WHEN
    solution = solution_generator.generate(5)

    # THEN
    assert solution.size() == 5
    assert default_evaluator.eval(solution) > -999999999999

def test_repair_limit():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(1.2, 0, 1), Slot(-0.1, 0, 1)]

    # WHEN
    repaired = repairer.repair_limit(Schedule(slots))

    # THEN
    assert repaired.slots[0].load_state == 1
    assert repaired.slots[1].load_state == 0

def test_repair_blocked():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    constraint_schedule = Schedule([Slot(1, 0, 1, Strategy.BLOCKED_CHARGE),
                                    Slot(1, 0, 1, Strategy.SELL),
                                    Slot(1, 0, 1, Strategy.BLOCKED_DISCHARGE)])
    evaluator = SpreadingEvaluator(TestMarket(), storage, constraint_schedule=constraint_schedule)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(0.2, 0, 1), Slot(0.1, 0, 1), Slot(0.1, 0, 1)]

    # WHEN
    repaired = repairer.repair_blocked(Schedule(slots))

    # THEN
    assert repaired == Schedule([Slot(1, 0, 1, Strategy.BLOCKED_CHARGE),
                                    Slot(0.1, 0, 1),
                                    Slot(1, 0, 1, Strategy.BLOCKED_DISCHARGE)])

def test_repair_load_states():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(0.2, 0, 1, Strategy.SELL), Slot(0.1, 0, 1, Strategy.BUY), Slot(0.4, 0, 1, Strategy.BUY)]

    # WHEN
    repaired = repairer.repair_load_state(Schedule(slots))

    # THEN
    assert repaired == Schedule([Slot(0.2, 0, 1, Strategy.SELL),
                                 Slot(0.2, 0, 1, Strategy.BUY),
                                 Slot(0.4, 0, 1, Strategy.BUY)])

def test_repair_max_power():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(0.2, 0, 1, Strategy.SELL), Slot(0.1, 0, 1, Strategy.SELL), Slot(0.4, 0, 1, Strategy.BUY)]

    # WHEN
    repaired = repairer.repair_max_power(Schedule(slots))

    # THEN
    print(repaired)
    assert repaired == Schedule([Slot(0.2643239309612102, 0, 1, Strategy.SELL),
                                 Slot(0.1, 0, 1, Strategy.SELL),
                                 Slot(0.2798101748326563, 0, 1, Strategy.BUY)])
    assert evaluator.eval(repaired) == 69.76029632080957

def test_repair_strategies():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(0.2, 0, 1, Strategy.SELL), Slot(0.1, 0, 1, Strategy.BUY), Slot(0.4, 0, 1, Strategy.BUY)]

    # WHEN
    repaired = repairer.repair_strategies(Schedule(slots))

    # THEN
    assert not repaired.slots[1].strategy.is_charge

def test_repair_strategies2():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    repairer = SolutionRepairer(evaluator)
    slots = [Slot(0.6, 0, 1, Strategy.CHARGE), Slot(0.1, 0, 1, Strategy.BUY), Slot(0.4, 0, 1, Strategy.BUY)]

    # WHEN
    repaired = repairer.repair_strategies(Schedule(slots))

    # THEN
    assert repaired.slots[0].strategy.is_charge
    assert repaired.slots[0].strategy != Strategy.CHARGE

