from eocohda.algorithm.firefly import FireFly, FireFlyBlackbox, FireFlyPareto
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket
from eocohda.core.storage import EnergyStorage
from interface import implements

def test_firefly_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .max_charge(500) \
        .max_discharge(500) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = FireFly(evaluator, n=20, y=1, iteration_count=100, alpha=0.5, delta=1)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None

def test_firefly_blackbox_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .max_charge(500) \
        .max_discharge(500) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = FireFlyBlackbox(evaluator, n=20, y=1, iteration_count=100, alpha=0.5, delta=1)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None

def test_firefly_pareto_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .max_charge(500) \
        .max_discharge(500) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = FireFlyPareto(evaluator, n=20, y=1, iteration_count=100, alpha=0.5, delta=1)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None