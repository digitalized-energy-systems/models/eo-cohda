from eocohda.algorithm.stopping import MutualDominaceRateIndicator, ThresholdDecision, KalmanEvidenceGatherer, createMGBMStoppingCriterion
from eocohda.core.algorithm import Solution


def create_solution(fitness):
    return Solution(None, 0, fitness=fitness)

def test_mutual_dominace_rate_indicator():
    # GIVEN
    popFirst = [create_solution([10, 2]) , create_solution([9, 11]), create_solution([10, 9]), create_solution([6, 2])]
    popSecond = [create_solution([20, 22]) , create_solution([1, 1]), create_solution([30, 9]), create_solution([6, 2])]
    indicator = MutualDominaceRateIndicator()

    # WHEN
    first = indicator.compute_indicator(popFirst)
    second = indicator.compute_indicator(popSecond)

    # THEN
    assert first == 1
    assert second == 0.5

def test_threshold_decision():
    # GIVEN
    decision = ThresholdDecision(0.1)

    # WHEN
    result = decision.decide_stop([0.1, 0.02, 0.1])
    result2 = decision.decide_stop([0.01, 0.009, 0.091])

    # THEN
    assert not result
    assert result2

def test_kalman_gatherer():
    # GIVEN
    gatherer = KalmanEvidenceGatherer(1)

    # WHEN
    evidence = gatherer.gather_evidence([1])
    evidence2 = gatherer.gather_evidence([0.5])
    evidence3 = gatherer.gather_evidence([1])
    evidence4 = gatherer.gather_evidence([0.1])
    evidence5 = gatherer.gather_evidence([0])
    evidence6 = gatherer.gather_evidence([0.1])
    evidence7 = gatherer.gather_evidence([0])
    evidence8 = gatherer.gather_evidence([0])

    for i in range(10):
        gatherer.gather_evidence([-1])

    evidence9 = gatherer.gather_evidence([0])

    # THEN
    assert evidence == 0.9999884359936638
    assert evidence2 == 0.8158886060535763
    assert evidence3 == 0.877764946958242
    assert evidence4 == 0.7026888794866178
    assert evidence5 == 0.5861892168074071
    assert evidence6 == 0.5198487666186332
    assert evidence7 == 0.4545788535498814
    assert evidence8 == 0.4048834413649153
    assert evidence9 == -0.3124037395094769

def test_mgbm():
    # GIVEN
    popFirst = [create_solution([10, 2]) , create_solution([9, 11]), create_solution([10, 9]), create_solution([6, 2])]
    popSecond = [create_solution([20, 22]) , create_solution([1, 1]), create_solution([30, 9]), create_solution([6, 2])]
    mgbm = createMGBMStoppingCriterion(0.1, 1)

    # WHEN
    stopResult = mgbm.stop(popFirst)
    stopResult2 = mgbm.stop(popSecond)
    stopResult3 = mgbm.stop(popFirst)
    stopResult4 = mgbm.stop(popFirst)
    stopResult5 = mgbm.stop(popFirst)
    stopResult6 = mgbm.stop(popFirst)
    stopResult7 = mgbm.stop(popFirst)
    stopResult8 = mgbm.stop(popFirst)
    stopResult9 = mgbm.stop(popFirst)

    for i in range(10):
        mgbm.stop(popFirst)

    stopResult10 = mgbm.stop(popFirst)

    # THEN
    assert not stopResult
    assert not stopResult2
    assert not stopResult3
    assert not stopResult4
    assert not stopResult5
    assert not stopResult6
    assert not stopResult7
    assert not stopResult8
    assert not stopResult9
    assert stopResult10