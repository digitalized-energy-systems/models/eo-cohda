from pathlib import Path
import os

import numpy as np

import lzma, json, io
import eocohda.evaluation.util as util
import eocohda.core.util as cutil
from eocohda.algorithm.evo import *

from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import ListMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy

SIZE_STORAGE_COEFF = 1
PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(".")))
DATA_PATH = os.path.join(PROJECT_PATH, 'data')
PLANNING_HORIZON = 96

def objective_function(cluster_schedule, ts, weights):
    # Return the negative(!) sum of all deviations, because bigger scores
    # mean better plans (e.g., -1 is better then -10).
    # print('objective_function: ')
    sum_cs = cluster_schedule.sum(axis=0)  # sum for each interval
    diff = np.abs(ts - sum_cs)  # deviation to the target schedeule
    w_diff = diff * weights  # multiply with weight vector
    result = -np.sum(w_diff)
    return result

def objective_function_os(operating_schedule, ts, weights):
    cs = np.array([operating_schedule])

    cs[0] = operating_schedule
    return objective_function(cs, ts, weights)

def read_target_schedule(path_to_target_file, n_sim_steps=PLANNING_HORIZON):
    """
    Read taget file and return target schedule and weights
    """

    # open the target file and read the header
    my_open = lzma.open if path_to_target_file.endswith('.xz') else io.open
    target_file = my_open(path_to_target_file, 'rt')
    line = next(target_file).strip()
    targets_meta = json.loads(line)
    assert targets_meta['interval_minutes'] == 15

    # create initial lists
    target_schedule = [0.0] * n_sim_steps
    weights = [1.0] * n_sim_steps
    # load the target schedule
    sum = 0
    for i in range(n_sim_steps):
        data = next(target_file).strip().split(',')
        target_schedule[i] = abs(float(data[0]))
        weights[i] = float(data[1])
        sum += abs(target_schedule[i]) * weights[i]

    print("Die Gesamtsumme des Fahrplans liegt bei %s" % (str(sum)))

    return target_schedule, weights

class TargetDiffEvaluator:

    def __init__(self, ts, weights, storage) -> None:
        self.ts = ts
        self.weights = weights
        self.storage = storage

    def blackbox_eocohda(self, schedule):
        return objective_function_os(cutil.convert_to_isaac(self.storage, schedule), self.ts, self.weights)

    def has_changed(self):
        return False

def testcase_example():
    # read target schedule
    ts_path = os.path.join(DATA_PATH, 'targets', 'scenario_big.csv')
    ts, weights = read_target_schedule(ts_path)
    
    # create local energy storage
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0) \
        .self_discharge(0) \
        .capacity(6000000 * SIZE_STORAGE_COEFF) \
        .charge_efficiency(np.sqrt(0.8)) \
        .discharge_efficiency(np.sqrt(0.8)) \
        .max_charge(325000 * SIZE_STORAGE_COEFF) \
        .max_discharge(325000 * SIZE_STORAGE_COEFF) \
        .build()

    # cohda objective
    global_eval = TargetDiffEvaluator(ts, weights, storage)
    # local objective arbitrage
    energy_values = np.array(util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv"))[:PLANNING_HORIZON] / 4000000
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.CHARGE, Strategy.OWN_DISCHARGE])
    evaluator.update_threshold(-1000)

    # generator using both objectives to generate pareto fronts
    algo = EvoAlgorithmSimpleParetoNSGA(evaluator, solution_size=8, gen_size=24, iteration_count=100)
    algo.blackbox = global_eval
    best_schedule = algo.generate_schedules(0, 15, PLANNING_HORIZON, 0)

    # best_schedule == best schedule in pareto front in terms of crowding distance
    # algo.solutions == all best solutions
    # algo.all_solutions == all solutions ever found
    assert best_schedule is not None
    assert len(algo.solutions) > 0