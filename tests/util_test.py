from pathlib import Path

from eocohda.core.schedule import Strategy, Schedule
from eocohda.core.storage import EnergyStorage
import eocohda.core.util as util

def test_convert_to_isaac():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(0.7, 0, 15) \
        .add_slot_duration(0.6, 30) \
        .add_slot_duration(0.45, 30, Strategy.SELL) \
        .add_slot_duration(0.5, 30, Strategy.BUY) \
        .add_slot_duration(0.7, 30, Strategy.BLOCKED_CHARGE) \
        .add_slot_duration(0.3, 30, Strategy.BLOCKED_DISCHARGE) \
        .build()
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.8) \
        .self_discharge(0) \
        .capacity(1000) \
        .charge_efficiency(0.8) \
        .discharge_efficiency(0.85) \
        .max_discharge(400) \
        .max_charge(400) \
        .build()

    # WHEN
    isaac_schedule = util.convert_to_isaac(storage, schedule)

    # THEN
    assert isaac_schedule.tolist() == [84.99999999999928, 84.99999999999928, 0, 0, 0, 0]

def test_read_profile():
    # GIVEN
    data_folder = Path("data/pv/")
    file_to_open = data_folder / "example.csv"

    # WHEN
    energy_list = util.read_profile(file_to_open)

    # THEN
    assert energy_list[0:3] == [123, 453, 789]

def test_write_solutions():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(0.7, 0, 15) \
        .add_slot_duration(0.6, 30) \
        .add_slot_duration(0.45, 30, Strategy.SELL) \
        .add_slot_duration(0.5, 30, Strategy.BUY) \
        .add_slot_duration(0.7, 30, Strategy.BLOCKED_CHARGE) \
        .add_slot_duration(0.3, 30, Strategy.BLOCKED_DISCHARGE) \
        .build()
    data_folder = Path("data/solutions")
    file_to_write = data_folder / "test.pickle"
    
    # WHEN
    util.write_solutions(schedule, file_to_write)

    # THEN
    assert open(file_to_write) is not None

def test_read_solutions():
    # GIVEN
    schedule = Schedule.builder() \
        .add_slot(0.7, 0, 15) \
        .add_slot_duration(0.6, 30) \
        .add_slot_duration(0.45, 30, Strategy.SELL) \
        .add_slot_duration(0.5, 30, Strategy.BUY) \
        .add_slot_duration(0.7, 30, Strategy.BLOCKED_CHARGE) \
        .add_slot_duration(0.3, 30, Strategy.BLOCKED_DISCHARGE) \
        .build()
    data_folder = Path("data/solutions")
    file_to_read = data_folder / "test.pickle"
    
    # WHEN
    solutions = util.read_solutions(file_to_read)

    # THEN
    assert solutions == schedule
