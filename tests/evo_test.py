from pathlib import Path

import numpy as np

from eocohda.algorithm.evo import EvoAlgorithm, EvoAlgorithmParetoNSGA, EvoAlgorithmBlackbox, EvoAlgorithmSimple, EvoAlgorithmSimpleBlackbox
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import ListMarket, TestMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy
from eocohda.core.unit import BlackboxGlobal
import eocohda.evaluation.util as util
import eocohda.core.util as coreutil

def test_evo_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = EvoAlgorithm(evaluator, solution_size=20, gen_size=10, parent_num=8, iteration_count=100)

    # WHEN
    best_fitness = algorithm.generate_schedules(0, 1, 96, 1)

    # THEN
    assert best_fitness is not None


def test_evo_simple_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = EvoAlgorithmSimple(evaluator, solution_size=20, gen_size=10, iteration_count=500)

    # WHEN
    best_fitness = algorithm.generate_schedules(0, 1, 96, 1)

    # THEN
    assert best_fitness is not None

def test_pareto_evo_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    energy_values = util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv")
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.OWN_DISCHARGE, Strategy.CHARGE, Strategy.SELL])
    algorithm = EvoAlgorithmParetoNSGA(evaluator, solution_size=64, gen_size=32, parent_num=2, iteration_count=100)
    algorithm.blackbox = BlackboxGlobal(lambda x: -np.sum(np.abs(np.array(range(96)) - x)), lambda x: x, storage)

    # WHEN
    best_fitness = algorithm.generate_schedules(0, 1, 96, 1)

    # THEN
    assert best_fitness is not None

def test_blackbox_evo_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(50000) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = EvoAlgorithmBlackbox(evaluator, solution_size=20, gen_size=10, parent_num=8, iteration_count=100)

    # WHEN
    best_fitness = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_fitness is not None

def test_blackbox_simple_evo_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(50000) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    evaluator.update_threshold(30000)
    algorithm = EvoAlgorithmSimpleBlackbox(evaluator, solution_size=20, gen_size=10, iteration_count=100)

    # WHEN
    best_fitness = algorithm.generate_schedules(0, 1, 96, 1)

    # THEN
    assert best_fitness is not None
    assert False