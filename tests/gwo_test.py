from eocohda.algorithm.gwo import GreyWolfOptimizer, GreyWolfOptimizerBlackbox, GreyWolfOptimizerPareto
from eocohda.core.evaluator import SpreadingEvaluator
from eocohda.core.market import TestMarket
from eocohda.core.storage import EnergyStorage
from interface import implements

def test_gwo_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = GreyWolfOptimizer(evaluator, additional_pop_size=15, iteration_count=100, resolution=15, elim_scale=0.4)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None

def test_gwo_pareto_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = GreyWolfOptimizerPareto(evaluator, additional_pop_size=15, iteration_count=100, resolution=15, elim_scale=0.4)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None

def test_gwo_blackbox_smoke():
    # GIVEN
    storage = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.001) \
        .capacity(500) \
        .charge_efficiency(0.9) \
        .discharge_efficiency(0.85) \
        .build()
    evaluator = SpreadingEvaluator(TestMarket(), storage)
    algorithm = GreyWolfOptimizerBlackbox(evaluator, additional_pop_size=15, iteration_count=100, resolution=15, elim_scale=0.4)

    # WHEN
    best_solution = algorithm.generate_schedules(0, 1, 10, 1)

    # THEN
    assert best_solution is not None