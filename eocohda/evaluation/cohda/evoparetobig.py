"""
Configuration for the EO-COHDA Unit-Models, including the paramter for the
algorithm, the market, the storage, the participating agents
"""
import os
import copy

from eocohda.core.storage import EnergyStorage
from eocohda.core.evaluator import EvaluatorModel
from eocohda.core.market import RandomMarket
from eocohda.algorithm.evo import *

import eocohda.algorithm.stopping as stopping
import eocohda.evaluation.cohda.local as local

class EvoParetoBig:

    # path to project
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(".")))

    # path to data
    DATA_PATH = os.path.join(PROJECT_PATH, 'data')

    # path to result folder
    RESULT_PATH = os.path.join(PROJECT_PATH, 'results')

    # path to flexibility folder
    FLEX_PATH = os.path.join(DATA_PATH, 'DER_schedules')

    # path to db-file
    DB_FILE = os.path.join(RESULT_PATH, 'EvoParetoBig.hdf5')

    # time zone
    LOCAL_TZ = 'Europe/Berlin'

    # optional configuration for debugging clock
    CLOCK = {
        'start': '2016-04-01T00:30:00+02:00',
        'stop': '2016-07-31T23:30:00+02:00',
        'speed_up': 100,
    }

    INTERVAL_NUMBER = 96

    (LARGE_INDUSTRY_STORAGE_ONE, LARGE_INDUSTRY_EVALUATOR_ONE) = local.peak_cutting(0, "large")
    LARGE_INDUSTRY_ALGORITHM_ONE = EvoAlgorithmParetoNSGA(LARGE_INDUSTRY_EVALUATOR_ONE,
                                                    solution_size=8,
                                                    gen_size=24,
                                                    parent_num=1,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    LARGE_INDUSTRY_ALGORITHM_ONE_PRE = EvoAlgorithm(LARGE_INDUSTRY_EVALUATOR_ONE, solution_size=8, gen_size=24, parent_num=1, iteration_count=100)

    (LARGE_INDUSTRY_STORAGE_TWO, LARGE_INDUSTRY_EVALUATOR_TWO) = local.peak_cutting(0, "large")
    LARGE_INDUSTRY_ALGORITHM_TWO = EvoAlgorithmParetoNSGA(LARGE_INDUSTRY_EVALUATOR_TWO,
                                                    solution_size=8,
                                                    gen_size=24,
                                                    parent_num=1,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    LARGE_INDUSTRY_ALGORITHM_TWO_PRE = EvoAlgorithm(LARGE_INDUSTRY_EVALUATOR_TWO, solution_size=8, gen_size=24, parent_num=1, iteration_count=100)

    (LARGE_INDUSTRY_STORAGE_THREE, LARGE_INDUSTRY_EVALUATOR_THREE) = local.peak_cutting(0, "large")
    LARGE_INDUSTRY_ALGORITHM_THREE = EvoAlgorithmParetoNSGA(LARGE_INDUSTRY_EVALUATOR_THREE,
                                                    solution_size=8,
                                                    gen_size=24,
                                                    parent_num=1,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    LARGE_INDUSTRY_ALGORITHM_THREE_PRE = EvoAlgorithm(LARGE_INDUSTRY_EVALUATOR_THREE, solution_size=8, gen_size=24, parent_num=1, iteration_count=100)

    (LARGE_INDUSTRY_STORAGE_FOUR, LARGE_INDUSTRY_EVALUATOR_FOUR) = local.peak_cutting(0, "large")
    LARGE_INDUSTRY_ALGORITHM_FOUR = EvoAlgorithmParetoNSGA(LARGE_INDUSTRY_EVALUATOR_FOUR,
                                                    solution_size=8,
                                                    gen_size=24,
                                                    parent_num=1,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    LARGE_INDUSTRY_ALGORITHM_FOUR_PRE = EvoAlgorithm(LARGE_INDUSTRY_EVALUATOR_FOUR, solution_size=8, gen_size=24, parent_num=1, iteration_count=100)

    (LARGE_INDUSTRY_STORAGE_FIVE, LARGE_INDUSTRY_EVALUATOR_FIVE) = local.peak_cutting(0, "large")
    LARGE_INDUSTRY_ALGORITHM_FIVE = EvoAlgorithmParetoNSGA(LARGE_INDUSTRY_EVALUATOR_FIVE,
                                                    solution_size=8,
                                                    gen_size=24,
                                                    parent_num=1,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    LARGE_INDUSTRY_ALGORITHM_FIVE_PRE = EvoAlgorithm(LARGE_INDUSTRY_EVALUATOR_FIVE, solution_size=8, gen_size=24, parent_num=1, iteration_count=100)

    (PSW_STORAGE, PSW_EVALUATOR) = local.spreading_test_case(0.1)
    PSW_ALGORITHM = EvoAlgorithmParetoNSGA(PSW_EVALUATOR,
                                                    solution_size=30,
                                                    gen_size=30,
                                                    parent_num=4,
                                                    stopping=stopping.createMGBMStoppingCriterion(0.01, 1))
    PSW_ALGORITHM_PRE = EvoAlgorithm(PSW_EVALUATOR, solution_size=30, gen_size=30, parent_num=4, iteration_count=100)

    UNIT_MODEL_CONFIG_LIST = [
        {
            'algorithm' : LARGE_INDUSTRY_ALGORITHM_ONE,
            'storage' : LARGE_INDUSTRY_STORAGE_ONE,
            'name' : "EVO_PARETO_LARGE_INDUSTRY_ONE",
            'start_alg' : LARGE_INDUSTRY_ALGORITHM_ONE_PRE
        },
        {
            'algorithm' : LARGE_INDUSTRY_ALGORITHM_TWO,
            'storage' : LARGE_INDUSTRY_STORAGE_TWO,
            'name' : "EVO_PARETO_LARGE_INDUSTRY_TWO",
            'start_alg' : LARGE_INDUSTRY_ALGORITHM_TWO_PRE
        },
        {
            'algorithm' : LARGE_INDUSTRY_ALGORITHM_THREE,
            'storage' : LARGE_INDUSTRY_STORAGE_THREE,
            'name' : "EVO_PARETO_LARGE_INDUSTRY_THREE",
            'start_alg' : LARGE_INDUSTRY_ALGORITHM_THREE_PRE
        },
        {
            'algorithm' : LARGE_INDUSTRY_ALGORITHM_FOUR,
            'storage' : LARGE_INDUSTRY_STORAGE_FOUR,
            'name' : "EVO_PARETO_LARGE_INDUSTRY_FOUR",
            'start_alg' : LARGE_INDUSTRY_ALGORITHM_FOUR_PRE
        },
        {
            'algorithm' : LARGE_INDUSTRY_ALGORITHM_FIVE,
            'storage' : LARGE_INDUSTRY_STORAGE_FIVE,
            'name' : "EVO_PARETO_LARGE_INDUSTRY_FIVE",
            'start_alg' : LARGE_INDUSTRY_ALGORITHM_FIVE_PRE
        },
        {
            'algorithm' : PSW_ALGORITHM,
            'storage' : PSW_STORAGE,
            'name' : "EVO_PARETO_PSW",
            'start_alg' : PSW_ALGORITHM_PRE
        },
        {
            'get_schedules_from_files': True,
            'schedule_dir': FLEX_PATH,
            'name': 'EVO_PARETO_PV_ONE',
            'schedule_files': ['big_one.csv'],
            'weight': 1
        },
        {
            'get_schedules_from_files': True,
            'schedule_dir': FLEX_PATH,
            'name': 'EVO_PARETO_PV_TWO',
            'schedule_files': ['big_two.csv'],
            'weight': 1
        }
    ]

    UNIT_MODEL_LIST = [
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.isaac.sim_models.simmodels:DER',
        'eocohda.isaac.sim_models.simmodels:DER'
    ]

    # controller and observer configs
    CTRL_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'negotiation_single_start': True,
        'negotiation_timeout': 60*60,           # seconds
        'topology_manager': 'eocohda.isaac.controller.core.management:TopologyManager',
        'topology_phi': 1,          # We'll get a ring topology plus *at most* n_agents * phi connections.
        'topology_seed': 1,      # random seed used for topology creation
        'scheduling_res': 15 * 60,  # resolution in seconds
        'scheduling_period': INTERVAL_NUMBER * 15 * 60   # one day
    }

    OBS_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'log_dbcls': 'eocohda.isaac.observer.core.monitoring:Monitoring',
        'log_dbfile': DB_FILE,
        'termcls': 'eocohda.isaac.observer.core.termination:MessageCounter'
    }

    PLANNER_CLS = 'eocohda.isaac.core.planning:Planner'
    PLANNER_CONFIG = {'check_inbox_interval': .1}

    # negotiations
    NEGOTIATIONS = [
                    {
                        'date': '2017-07-05T00:00:00+00:00',
                        'target': os.path.join(DATA_PATH, 'targets', 'scenario_big.csv')
                    }
    ]

    # container configs
    CTRL_OBS_CONTAINER = {'host': 'localhost', 'port': 5555}
    # container for unit agents
    AGENT_CONTAINER = [
                        {'host': 'localhost', 'port': 5556},
                        {'host': 'localhost', 'port': 5557}
    ]