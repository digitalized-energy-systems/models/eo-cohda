"""
Configuration for the EO-COHDA Unit-Models, including the paramter for the
algorithm, the market, the storage, the participating agents
"""
import os
import copy

from eocohda.core.storage import EnergyStorage
from eocohda.core.evaluator import EvaluatorModel
from eocohda.core.market import RandomMarket
from eocohda.algorithm.gwo import GreyWolfOptimizerPareto

import eocohda.algorithm.stopping as stopping
import eocohda.evaluation.cohda.local as local

class GWOParetoSmall:

    # path to project
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(".")))

    # path to data
    DATA_PATH = os.path.join(PROJECT_PATH, 'data')

    # path to result folder
    RESULT_PATH = os.path.join(PROJECT_PATH, 'results')

    # path to flexibility folder
    FLEX_PATH = os.path.join(DATA_PATH, 'DER_schedules')

    # path to db-file
    DB_FILE = os.path.join(RESULT_PATH, 'GWOParetoSmall.hdf5')

    # time zone
    LOCAL_TZ = 'Europe/Berlin'

    # optional configuration for debugging clock
    CLOCK = {
        'start': '2016-04-01T00:30:00+02:00',
        'stop': '2016-07-31T23:30:00+02:00',
        'speed_up': 100,
    }

    INTERVAL_NUMBER = 96

    (SMALL_HOUSE_PV_ONE_STORAGE, SMALL_HOUSE_PV_ONE_EVALUATOR) = local.own_consumption_test_case(0)
    SMALL_HOUSE_PV_ONE_ALGORITHM = GreyWolfOptimizerPareto(SMALL_HOUSE_PV_ONE_EVALUATOR,
                                                        additional_pop_size=16,
                                                        elim_scale=0.5,
                                                        stopping=stopping.createMGBMStoppingCriterion(0.01, 1))

    (SMALL_HOUSE_PV_TWO_STORAGE, SMALL_HOUSE_PV_TWO_EVALUATOR) = local.own_consumption_test_case(0)
    SMALL_HOUSE_PV_TWO_ALGORITHM = GreyWolfOptimizerPareto(SMALL_HOUSE_PV_TWO_EVALUATOR,
                                                        additional_pop_size=16,
                                                        elim_scale=0.5,
                                                        stopping=stopping.createMGBMStoppingCriterion(0.01, 1))

    (SMALL_HOUSE_PV_THREE_STORAGE, SMALL_HOUSE_PV_THREE_EVALUATOR) = local.own_consumption_test_case(0)
    SMALL_HOUSE_PV_THREE_ALGORITHM = GreyWolfOptimizerPareto(SMALL_HOUSE_PV_THREE_EVALUATOR,
                                                          additional_pop_size=16,
                                                          elim_scale=0.5,
                                                          stopping=stopping.createMGBMStoppingCriterion(0.01, 1))

    (SMALL_INDUSTRY_STORAGE, SMALL_INDUSTRY_EVALUATOR) = local.peak_cutting(0)
    SMALL_INDUSTRY_ALGORITHM = GreyWolfOptimizerPareto(SMALL_INDUSTRY_EVALUATOR,
                                                     additional_pop_size=16,
                                                     elim_scale=0.5,
                                                     stopping=stopping.createMGBMStoppingCriterion(0.01, 1))

    UNIT_MODEL_CONFIG_LIST = [
        {
            'algorithm' : SMALL_HOUSE_PV_ONE_ALGORITHM,
            'storage' : SMALL_HOUSE_PV_ONE_STORAGE,
            'name' : "GWO_PARETO_SMALL_HOUSE_PV_ONE"
        },
        {
            'algorithm' : SMALL_HOUSE_PV_TWO_ALGORITHM,
            'storage' : SMALL_HOUSE_PV_TWO_STORAGE,
            'name' : "GWO_PARETO_SMALL_HOUSE_PV_TWO"
        },
        {
            'algorithm' : SMALL_HOUSE_PV_THREE_ALGORITHM,
            'storage' : SMALL_HOUSE_PV_THREE_STORAGE,
            'name' : "GWO_PARETO_SMALL_HOUSE_PV_THREE"
        },
        {
            'algorithm' : SMALL_INDUSTRY_ALGORITHM,
            'storage' : SMALL_INDUSTRY_STORAGE,
            'name' : "GWO_PARETO_SMALL_INDUSTRY"
        },
        {
            'get_schedules_from_files': True,
            'schedule_dir': FLEX_PATH,
            'name': 'GWO_PARETO_PV_ONE',
            'schedule_files': ['small_one.csv'],
        },
        {
            'get_schedules_from_files': True,
            'schedule_dir': FLEX_PATH,
            'name': 'GWO_PARETO_PV_TWO',
            'schedule_files': ['small_two.csv'],
        }
    ]

    UNIT_MODEL_LIST = [
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.core.unit:EnergyStorageUnitModel',
        'eocohda.isaac.sim_models.simmodels:DER',
        'eocohda.isaac.sim_models.simmodels:DER'
    ]

    # controller and observer configs
    CTRL_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'negotiation_single_start': True,
        'negotiation_timeout': 60*60,           # seconds
        'topology_manager': 'eocohda.isaac.controller.core.management:TopologyManager',
        'topology_phi': 1,          # We'll get a ring topology plus *at most* n_agents * phi connections.
        'topology_seed': 1,      # random seed used for topology creation
        'scheduling_res': 15 * 60,  # resolution in seconds
        'scheduling_period': INTERVAL_NUMBER * 15 * 60   # one day
    }

    OBS_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'log_dbcls': 'eocohda.isaac.observer.core.monitoring:Monitoring',
        'log_dbfile': DB_FILE,
        'termcls': 'eocohda.isaac.observer.core.termination:MessageCounter'
    }

    PLANNER_CLS = 'eocohda.isaac.core.planning:Planner'
    PLANNER_CONFIG = {'check_inbox_interval': .1}

    # negotiations
    NEGOTIATIONS = [
                    {
                        'date': '2017-07-05T00:00:00+00:00',
                        'target': os.path.join(DATA_PATH, 'targets', 'scenario_small.csv')
                    }
    ]

    # container configs
    CTRL_OBS_CONTAINER = {'host': 'localhost', 'port': 5555}
    # container for unit agents
    AGENT_CONTAINER = [
                        {'host': 'localhost', 'port': 5556},
                        {'host': 'localhost', 'port': 5557}
    ]