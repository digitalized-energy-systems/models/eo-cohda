import csv
import itertools

import h5py

def read_smard_market(file):
    """
    Read the market values from SMARD
    """
    market_data = []
    with open(file) as file_handle:
        csv_rows = csv.reader(file_handle, delimiter=';')
        for _ in itertools.repeat(None, 1):
            next(csv_rows)
        for row in csv_rows:
            # Data = Value for 1 hour, Model = Value for 15min
            for _ in range(4):
                market_data.append(float(row[15].replace(",", ".")))

    return market_data

def read_pv_profile_kronberg(file):
    """
    Read the pv profile (Watt)
    """
    pv_data = []
    with open(file) as file_handle:
        for line in file_handle:
            pv_data.append(int(line))
    return pv_data

def read_pv_kronberg_by_day(file, day):
    """
    Read the pv profile (Watt) (original)
    """
    pv_data = []
    starting_line = day * 96
    end_line = (day + 1) * 96
    with open(file) as file_handle:
        lines = file_handle.readlines()[starting_line:end_line]
        for line in lines:
            pv_data.append(int(line.split("\t")[1].replace("\n", "")))
    return pv_data

def read_household_profile(pls, day):
    """
    Household profile (Watt)
    """
    starting_line = day * 24 * 60
    end_line = (day + 1) * 24 * 60
    hh_data = [0 for _ in itertools.repeat(None, end_line - starting_line)]

    for pl in pls:
        with open(pl) as file_handle:
            lines = file_handle.readlines()[starting_line:end_line]
            for i in range(len(lines)):
                hh_data[i] += int(lines[i].split(",")[0])

    # aggregate to 15min intervals
    return [sum(hh_data[i:i+15]) for i in range(0, len(hh_data), 15)]

def read_industry_profile(profile, day):
    """
    Industry profile (Watt)
    """
    starting_line = day * 96
    end_line = (day + 1) * 96

    hh_data = []
    with open(profile) as file_handle:
        lines = file_handle.readlines()[starting_line:end_line]
        for line in lines:
            hh_data.append(int(line))

    return hh_data

def read_cs_from_hdf(file):
    """
    Read Cluster-Schedule from hdf-file
    """
    with h5py.File(file, 'r') as f:
        # Get the data
        data = list(f['dap']['2017-07-05T00:00:00+00:00']['cs'])
        agent_details = list(f['dap']['2017-07-05T00:00:00+00:00']['Agent details'])
        ts = list(f['dap']['2017-07-05T00:00:00+00:00']['ts'])
        dap = list(f['dap']['2017-07-05T00:00:00+00:00']['dap_data'])
        return data, agent_details, ts, dap
    raise Exception