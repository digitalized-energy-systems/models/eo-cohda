import locale

import math

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib as mpl

from eocohda.core.schedule import Strategy
import eocohda.evaluation.util as util
from pathlib import Path
import matplotlib.ticker as ticker
from collections import OrderedDict

# global settings
mpl.rcParams['xtick.major.size'] = 10
mpl.rcParams['xtick.major.width'] = 2
mpl.rcParams['xtick.minor.size'] = 5
mpl.rcParams['xtick.minor.width'] = 2

mpl.rcParams['ytick.major.size'] = 10
mpl.rcParams['ytick.major.width'] = 2
mpl.rcParams['ytick.minor.size'] = 5
mpl.rcParams['ytick.minor.width'] = 2

mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

DEFAULT_LW = 8

def array_average(fitness_history_list):
    fitness_vector_sum = np.zeros(len(fitness_history_list[0]))
    for history in fitness_history_list:
        fitness_vector_sum += history

    return fitness_vector_sum / len(fitness_history_list)

def standard_deviation(fitness_history_list):
    fitness_vector_variance = []
    average = array_average(fitness_history_list)
    for i in range(len(fitness_history_list[0])):
        variance = 0
        for j in range(len(fitness_history_list)):
            fitness = fitness_history_list[j][i]
            variance += (fitness - average[i]) ** 2
        variance = variance / len(fitness_history_list)
        fitness_vector_variance.append(math.sqrt(variance))

    return fitness_vector_variance

def show_best_fitness_as_boxplots(fitness_datasets, font_size=58, name=None, x_label=None, y_label='fitness'):
    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots()
    y_formatter = mpl.ticker.ScalarFormatter(useOffset=False)
    ax.yaxis.set_major_formatter(y_formatter)

    boxplots = ax.boxplot([[fitness_set[-1] for fitness_set in dataset] for (dataset, color, label) in fitness_datasets], labels=[label for (dataset, color, label) in fitness_datasets])
    plt.xticks(rotation=30)
    ax.set_ylabel(y_label)
    if x_label is not None:
        ax.set_xlabel(x_label)

    for box in boxplots['boxes']:
        box.set(color='#757575', linewidth=2)
        box.set(mfc='#26a69a')

    for whisker in boxplots['whiskers']:
        whisker.set(color='#3f51b5', linewidth=2)

    for cap in boxplots['caps']:
        cap.set(color='#3f51b5', linewidth=2)

    for median in boxplots['medians']:
        median.set(color='#b2df8a', linewidth=2)

    for flier in boxplots['fliers']:
        flier.set(marker='o', color='#e7298a', alpha=0.5)

    fig.set_size_inches(24, 16)


    if name is not None:
        plt.tight_layout()
        plt.savefig(name)

def show_fitness_datasets(fitness_datasets, show_deviation=False, font_size=54, points=None, ylimit=None, name=None, legend_bbox=None, legend_loc='lower right', add_width=0, x_label="iteration", sci_ticks=True, part_legend=False, lw=DEFAULT_LW):
    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots()
    for (dataset, color, label, linestyle, x_array) in fitness_datasets:
        show_fitness(dataset,
                        fitness_x=x_array,
                         show_deviation=show_deviation,
                         font_size=font_size,
                         shared_ax=ax,
                         color=color,
                         lw=lw,
                         label=label,
                         linestyle=linestyle,
                         ylimit=ylimit)

    if points is not None:
        for point_list in points:
            for point in point_list:
                ax.plot(point[0], point[1], "o", color="black")

    ax.set_xlabel(x_label)
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]

    linestyles = list(OrderedDict.fromkeys([linestyle for _, __, ___, linestyle, ____ in fitness_datasets]))
    labels = list(OrderedDict.fromkeys([label for _, __, label, ___, ____ in fitness_datasets]))

    legend1 = None
    if part_legend:
        dummy_lines = []
        for i in range(len(linestyles)):
            dummy_lines.append(ax.plot([],[], c="black", linewidth=lw, ls = linestyles[i], dash_capstyle="round" if linestyles[i] == (0,(0.1,2)) else None)[0])
        lines = ax.get_lines()
        legend1 = plt.legend([lines[i + len(labels)] for i in range(len(labels))], labels, loc=legend_loc[0], bbox_to_anchor=legend_bbox)
        legend2 = plt.legend([dummy_lines[i] for i in [0,1]], ["Baseline EA", "GABHYME"], loc=legend_loc[1], bbox_to_anchor=legend_bbox)
        ax.add_artist(legend1)
    else:
        ax.legend(*zip(*unique), loc=legend_loc, bbox_to_anchor=legend_bbox)
    ax.set_ylabel("fitness")

    if sci_ticks:
        ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.locator_params(axis='y', nbins=5)

    if ylimit is not None:
        ax.set_ylim([ylimit[0], ylimit[1]])

    fig.set_size_inches(24 + add_width, 16)
    plt.tight_layout()
    plt.grid()

    if name is not None:
        if legend1 is not None:
            plt.savefig(name, bbox_extra_artists=(legend1,), bbox_inches='tight')
        else:
            plt.savefig(name)

def show_fitness(fitness_history,
                 fitness_x=None,
                 show_deviation=False,
                 font_size=39,
                 shared_ax=None,
                 color="r",
                 linestyle=None,
                 marker=None,
                 markerfacecolor=None,
                 markersize=None,
                 x_label="iteration",
                 label="average fitness",
                 ylimit=None,
                 lw=4,
                 name=None):
    if shared_ax is None:
        plt.clf()

    fitness_curve = array_average(fitness_history)
    deviation_curve = standard_deviation(fitness_history)
    fitness_add_deviation = fitness_curve + deviation_curve
    fitness_sub_deviation = fitness_curve - deviation_curve

    plt.rcParams.update({'font.size': font_size})

    ax = shared_ax
    if ax is None:
        ax = plt.subplot(111)

    if fitness_x is None:
        ax.plot(fitness_curve, color=color, label=label, linewidth=lw, marker=marker, linestyle=linestyle, markerfacecolor=markerfacecolor, markersize=markersize,
        dash_capstyle="round" if linestyle == (0,(0.1,2)) else None)
    else:
        ax.plot(fitness_x, fitness_curve, color=color, label=label, linewidth=lw, marker=marker, linestyle=linestyle, markerfacecolor=markerfacecolor, markersize=markersize,
        dash_capstyle="round" if linestyle == (0,(0.1,2)) else None)

    if (show_deviation):
        ax.plot(fitness_add_deviation, ":", color=color, linewidth=lw, marker=marker, linestyle=linestyle, markerfacecolor=markerfacecolor, markersize=markersize)
        ax.plot(fitness_sub_deviation, ":", color=color, linewidth=lw, marker=marker, linestyle=linestyle, markerfacecolor=markerfacecolor, markersize=markersize)

    if shared_ax is None:
        ax.set_xlabel(x_label)
        ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax.set_yticks(ax.get_yticks()[::2])
        handles, labels = ax.get_legend_handles_labels()
        unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
        ax.legend(*zip(*unique), loc='lower right')

        ax.set_ylabel("fitness")

        if ylimit is not None:
            ax.set_ylim([ylimit[0], ylimit[1]])

        plt.tight_layout()
        plt.grid()
    if shared_ax is None and name is not None:
        plt.savefig(name)

    if fitness_history and fitness_history[0]: 
        print("Die beste Fitness ist %s mit einer Standardabweichung von %s" % (str(max([max(history) for history in fitness_history if history])), str(deviation_curve[len(deviation_curve) - 1])))

def show_fitness_fronts(solutions_list, title, font_size=52, ylimit=None, name=None):
    plt.clf()

    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots()
    cmap_list = [cm.get_cmap(cmap_name) for cmap_name in ['rainbow', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
                      'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
                      'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn']]
    
    for i, solutions_dataset in enumerate(solutions_list):
        solutions = solutions_dataset[0]
        marker = solutions_dataset[1]
        normalize = mpl.colors.Normalize(vmin=0, vmax=1 + max([solution.iteration for solution in solutions]))
        colors = [cmap_list[i](normalize(solution.iteration)) for solution in solutions]
        for j in range(len(solutions)):
            fitness = solutions[j].fitness
            plt.scatter(fitness[0], fitness[1], color=colors[j], marker=marker)

        #cax, _ = mpl.colorbar.make_axes(ax, pad=i)
        #cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap_list[i], norm=normalize)
        #cbar.set_label("iteration")

    ax.set_title(title)
    ax.set_xlabel("local fitness")
    ax.set_ylabel("global fitness")

    if ylimit is not None:
        ax.set_ylim([ylimit[0], ylimit[1]])

    fig.set_size_inches(60, 16)
    #plt.tight_layout()

    plt.grid()
    if name is not None:
        plt.savefig(name)


def show_fitness_set(solution_list, title, font_size=52, ylimit=None, name=None):
    if not solution_list:
        return
    plt.clf()

    plt.rcParams.update({'font.size': font_size})
    ax = plt.subplot(111)
    cmap = cm.get_cmap('plasma')
    normalize = mpl.colors.Normalize(vmin=0, vmax=1 + max([solution.iteration for solution in solution_list]))
    colors = [cmap(normalize(solution.iteration)) for solution in solution_list]
    for i in range(len(solution_list)):
        fitness = solution_list[i].fitness
        plt.scatter(fitness[0], fitness[1], color=colors[i])

    cax, _ = mpl.colorbar.make_axes(ax)
    cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
    cbar.set_label("iteration")

    ax.set_title(title)
    ax.set_xlabel("local fitness")
    ax.set_ylabel("global fitness")

    if ylimit is not None:
        ax.set_ylim([ylimit[0], ylimit[1]])

    plt.grid()
    if name is not None:
        plt.savefig(name)

def set_xmargin(ax, left, right):
    ax.set_xmargin(0)
    ax.autoscale_view()
    lim = ax.get_xlim()
    delta = np.diff(lim)
    left = lim[0] - delta*left
    right = lim[1] + delta*right
    ax.set_xlim(left, right)

def show_solution(schedule, init_load, title, font_size=57, name=None, alt_data=None, alt_label=None, alt_colors=["g", "orange"], alt_y_label=None):

    color_map = {
        Strategy.CHARGE : "cyan",
        Strategy.BUY : "r",
        Strategy.DISCHARGE : "b",
        Strategy.SELL: "y",
        Strategy.BLOCKED_CHARGE: "g",
        Strategy.BLOCKED_DISCHARGE: "orange",
        Strategy.OWN_DISCHARGE : "orchid"
    }
    strat_map = {
        Strategy.CHARGE : "charge",
        Strategy.BUY : "buy",
        Strategy.DISCHARGE : "discharge",
        Strategy.SELL: "sell",
        Strategy.BLOCKED_CHARGE: "x",
        Strategy.BLOCKED_DISCHARGE: "y",
        Strategy.OWN_DISCHARGE : "local usage"
    }

    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots()

    if alt_data is not None:
        ax2 = ax.twinx()
        y_x_axis = range(schedule.size()+1)
        for d in range(len(alt_data)):
            y1_for_fill = np.zeros(schedule.size()+1)
            y1_for_fill[0] = alt_data[d][0]
            for i in range(schedule.size()):
                y1_for_fill[i+1] = alt_data[d][i]
            ax2.plot(y_x_axis, y1_for_fill, color=alt_colors[d], label=alt_label[d], alpha=0.3, linewidth=DEFAULT_LW)
            ax2.fill_between(y_x_axis, y1_for_fill, np.zeros(schedule.size()+1), color=alt_colors[d], alpha=0.2)
        ax2.tick_params(axis="y", labelcolor="g")
        set_xmargin(ax2,0,0)
        ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

        ax2.set_ylabel(alt_y_label, color="g")
        handles, labels = ax2.get_legend_handles_labels()
        unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
        ax2.legend(*zip(*unique), loc='upper left')

    for i in range(schedule.size()):
        ax.plot([i, i+1], [init_load if i == 0 else schedule.slots[i - 1].load_state, schedule.slots[i].load_state], label=strat_map[schedule.slots[i].strategy],
                color=color_map[schedule.slots[i].strategy], linestyle='-', linewidth=DEFAULT_LW, zorder=1)
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique), loc='upper right')
    ax.set_title(title)
    ax.set_xlabel("interval")
    ax.set_ylabel("state of charge")

    #fig.tight_layout()
    fig.set_size_inches(24, 16)
    plt.tight_layout()

    if name is not None:
        plt.savefig(name)

def calc_ff_rate(cs, ts):
    np_ts = np.array([ts_el[0] for ts_el in ts])
    ts_sum = np_ts.sum(axis=0)
    result = (1 - (np.sum(np.abs((np_ts - np.array(cs).sum(axis=0)))) / ts_sum)) * 100
    if result > 100:
        raise Exception()
    return result

def show_cluster_schedule(cs, agent_details, ts, font_size=39, name=None):

    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots(1,1)

    # ts (correct height)
    plt.plot([pair[0] for pair in ts], color="black", label="target", linewidth=6)
    print(f'CS perc of best {calc_ff_rate(cs, ts)}')

    # cs
    color = cm.get_cmap('plasma')(np.linspace(0, 1, len(agent_details)))
    ax = plt.gca()
    ax.stackplot(range(96), np.vstack(cs),
                                colors=color,
                                labels=[(" ".join(agent_details[i][0].decode("ascii", "replace").split("_")[2:])).lstrip().replace("LARGE ", "").replace("SMALL", "").lower().replace("small", "").replace("psw", "PSP") for i in range(len(agent_details))],
                                alpha=0.8,
                                linewidth=0.5,
                                edgecolor="white")
    ax.set_xlabel("interval")
    ax.set_ylabel("power in W")
    ax.legend(loc='upper right', ncol=2)
    ax.margins(0)
    fig.set_size_inches(16, 12)
    plt.tight_layout()

    if name is not None:
        plt.savefig(name)

def main():
    show_fitness([np.array([0, 1, 2]), np.array([0, 2, 4]), np.array([0, 3, 4]), np.array([0, 3, 4])], show_deviation=True)

if __name__ == "__main__":
    main()