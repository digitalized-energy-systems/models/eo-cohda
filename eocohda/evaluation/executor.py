import numpy as np

from eocohda.core.evaluator import SpreadingEvaluator
import eocohda.evaluation.common as common
from eocohda.core.market import TestMarket
from eocohda.algorithm.evo import EvoAlgorithm
from eocohda.algorithm.gwo import GreyWolfOptimizer
from eocohda.algorithm.firefly import FireFly

def execute_evo(storage, popsize=20, iter_num=100, gen_size=10, parent_num=2, intervals=10, times=1, mutation_style=1, evaluator=None):
    new_eval = evaluator
    if new_eval is None:
        new_eval = SpreadingEvaluator(TestMarket(), storage)

    solution_history_list = []
    restart_point_list = []

    for _ in range(times):
        algorithm = EvoAlgorithm(new_eval, solution_size=popsize, gen_size=gen_size, parent_num=parent_num, iteration_count=iter_num, mutation_style=mutation_style)

        best = algorithm.generate_schedules(0, 1, intervals, 1)
        algorithm_history = algorithm.best_history
        solution_history_list.append(np.array(algorithm_history))
        restart_point_list.append(algorithm.restart_points)

    return (solution_history_list, restart_point_list)

def execute_firefly(storage, popsize=20, iter_num=500, y=1, alpha=0.5, delta=0.99, intervals=30, times=1, enable_brush=True, mu=1.22, enable_adaptive_theta=True, evaluator=None):
    new_eval = evaluator
    if new_eval is None:
        new_eval = SpreadingEvaluator(TestMarket(), storage)

    solution_history_list = []

    for _ in range(times):
        algorithm = FireFly(new_eval, n=popsize, y=y, iteration_count=iter_num, alpha=alpha, delta=delta, enable_brush=enable_brush, mu=mu, enable_adaptive_theta=enable_adaptive_theta)

        algorithm.generate_schedules(0, 1, intervals, 1)
        algorithm_history = algorithm.best_history
        solution_history_list.append(np.array(algorithm_history))

    return solution_history_list

def execute_gwo(storage, popsize=20, iter_num=500, elim_scale=0.1, intervals=30, times=1, diff=True, elim=True, evaluator=None):
    new_eval = evaluator
    if new_eval is None:
        new_eval = SpreadingEvaluator(TestMarket(), storage)

    solution_history_list = []

    for _ in range(times):
        algorithm = GreyWolfOptimizer(new_eval, additional_pop_size=popsize, iteration_count=iter_num, resolution=15, elim_scale=elim_scale, with_differential=diff, with_elim=elim)

        algorithm.generate_schedules(0, 1, intervals, 1)
        algorithm_history = algorithm.best_history
        solution_history_list.append(np.array(algorithm_history))

    return solution_history_list