"""
Implementation of an optimization algorithm for the multi objective local optimization using
the Grey Wolf heuristic.
"""

import itertools
import copy

import numpy as np
from overrides import overrides

from eocohda.core.algorithm import Algorithm, AlgorithmBlackbox, AlgorithmPareto, Solution, SolutionGenerator, SolutionRepairer
from eocohda.core.schedule import Slot, Schedule, Strategy
from eocohda.algorithm.stopping import createDefaultStopping
import eocohda.algorithm.common as common
import eocohda.core.util as util

class GreyWolfOptimizer(Algorithm):
    """
    Implementation of the GWO Algorithm for the multicriteria problem of optimizing
    local flexibility and global target schedule.
    """

    def __init__(self,
                evaluator,
                additional_pop_size=16,
                resolution=15,
                elim_scale=0.1,
                with_differential=True,
                with_elim=True,
                stopping=None,
                iteration_count=1000,
                **options):
        """
        :param additional_pop_size: additional size of the population
        :param iter_num: number of iterations
        :param resolution: interval resolution
        :param evaluator: evaluator
        """
        super().__init__(evaluator, **options)

        self.__resolution = resolution
        self.__pop_size = additional_pop_size + 3
        self.__elim_scale = elim_scale
        self._mut_history = []
        self.__repairer = SolutionRepairer(evaluator)
        self.__with_diff = with_differential
        self.__with_elim = with_elim
        self.__stopping = createDefaultStopping(stopping, iteration_count)
        self.__iter_count = iteration_count

    def __eval(self, grey_wolf):
        return self._eval(grey_wolf, solution_map=GreyWolfOptimizer.from_gwo_solution)

    def __generate_eval(self, generator, intervals, iteration_num):
        solution_gen = SolutionGenerator(self.evaluator)
        solution = self._create_solution(GreyWolfOptimizer.to_gwo_solution(solution_gen.generate(intervals)), iteration_num=iteration_num)
        solution.fitness = self.__eval(solution)
        return solution

    def __transfrom_solution(self, solution):
        solution.candidate = GreyWolfOptimizer.from_gwo_solution(solution.candidate)
        return solution

    @staticmethod
    def from_gwo_solution(gwo_solution):
        slots = []
        for i in range(len(gwo_solution[0])):
            element = gwo_solution[0][i]
            strategy_id = gwo_solution[1][i]
            slots.append(Slot(element,
                              i * 15,
                              (i + 1) * 15,
                              Strategy.from_id(strategy_id)))

        return Schedule(slots)

    @staticmethod
    def to_gwo_solution(norm_solution):
        gwo_solution = []
        gwo_solution_strategy = []
        for slot in norm_solution.slots:
            gwo_solution.append(slot.load_state)
            gwo_solution_strategy.append(slot.strategy.id)
        return (np.array(gwo_solution), gwo_solution_strategy)

    def __recombine(self, candidate, gradient):
        new_candidate = copy.deepcopy(candidate)

        # Define search range
        search_range = int(len(candidate) / 2)
        search_mid = search_range
        start_search = search_mid - int(search_range / 2)
        end_search = max(search_mid + int(search_range / 2), start_search + 1)

        # Search for the index of the element with the lowest difference in the search space
        cutting_point = start_search + \
                        self.__search_closest_number(candidate[0][start_search:end_search + 1],
                                                     gradient[start_search:end_search + 1])

        # Merge list at cutting point[i]
        new_candidate[0][cutting_point:] = gradient[cutting_point:len(gradient)]

        return new_candidate

    def __search_closest_number(self, first_list, second_list):
        return np.argmin(np.absolute(first_list - second_list))

    @overrides
    def generate_schedules(self, start, res, intervals, state):
        a_max = 2

        # Init population and a, A and C
        grey_wolf_pop = self._generate_starting_solutions(intervals, self.__pop_size, GreyWolfOptimizer.to_gwo_solution, GreyWolfOptimizer.from_gwo_solution)
        a = a_max

        # Use NSGA-II method to select 'best' three wolfes
        solutions = common.select_solutions(grey_wolf_pop, self.__pop_size)
        x_alpha = copy.deepcopy(solutions[0])
        x_beta = copy.deepcopy(solutions[1])
        x_delta = copy.deepcopy(solutions[2])

        old_best_fitness = x_alpha.fitness

        print("Initial alpha %s" % str(old_best_fitness))
        self._mut_history.append(copy.deepcopy(solutions + [x_alpha, x_beta, x_delta]))

        loop_iteration_count = self.__iter_count
        i = 0
        i_all = 0

        while not self.__stopping.stop(solutions):

            # Update a
            a = a_max - (a_max / loop_iteration_count) * i

            for x in range(self.__pop_size - 1, -1, -1):

                self.__mutate_strategy(solutions[x].candidate[0])

                for y in range(intervals):

                    ### LOAD_STATE_PART

                    # PART X1
                    r1 = np.random.random()
                    A1 = 2 * a * r1 - a
                    r2 = np.random.random()
                    C1 = 2 * r2

                    D_alpha = abs(C1 * x_alpha.candidate[0][y] - solutions[x].candidate[0][y])
                    X1 = x_alpha.candidate[0][y] - A1 * D_alpha

                    # PART X2
                    r1 = np.random.random()
                    A2 = 2 * a * r1 - a
                    r2 = np.random.random()
                    C2 = 2 * r2

                    D_beta = abs(C2 * x_beta.candidate[0][y] - solutions[x].candidate[0][y])
                    X2 = x_beta.candidate[0][y] - A2 * D_beta

                    # PART X3
                    r1 = np.random.random()
                    A3 = 2 * a * r1 - a
                    r2 = np.random.random()
                    C3 = 2 * r2

                    D_delta = abs(C3 * x_delta.candidate[0][y] - solutions[x].candidate[0][y])
                    X3 = x_delta.candidate[0][y] - A3 * D_delta

                    solutions[x].candidate[0][y] = (X1 + X2 + X3) / 3
                    solutions[x].id = self._next_id()
                    solutions[x].iteration = i_all

            if self.__with_diff:
                # Evolution Operation
                F = (loop_iteration_count - (i - 1)) / loop_iteration_count
                v = x_alpha.candidate[0] +  F * (x_beta.candidate[0] - x_delta.candidate[0])
                for x in range(self.__pop_size):
                    grey_wolf = solutions[x].candidate
                    grad_wolf = self._create_solution(v, iteration_num=i_all)
                    new_wolf = self.__recombine(grey_wolf, v)
                    solutions.append(self._create_solution(new_wolf, [solutions[x], grad_wolf], iteration_num=i_all))

            # Repair solution
            for x in range(len(solutions)):
                for y in range(intervals):
                    solutions[x].candidate[0][y] = np.clip(solutions[x].candidate[0][y], 0, 1)
                solutions[x].candidate = GreyWolfOptimizer.to_gwo_solution(self.__repairer.repair_strategies(self.__repairer.repair_max_power(GreyWolfOptimizer.from_gwo_solution(solutions[x].candidate))))

            # Calc fitness
            for grey_wolf in solutions:
                grey_wolf.fitness = self.__eval(grey_wolf)

            # Sort by fitness and select best three known positions
            solutions = common.select_solutions(solutions + [x_alpha, x_beta, x_delta], self.__pop_size + 3)
            best = copy.deepcopy(solutions[0])
            second = copy.deepcopy(solutions[1])
            third = copy.deepcopy(solutions[2])
            if x_alpha in solutions:
                solutions.remove(x_alpha)
            else:
                solutions.pop()
            if x_beta in solutions:
                solutions.remove(x_beta)
            else:
                solutions.pop()
            if x_delta in solutions:
                solutions.remove(x_delta)
            else:
                solutions.pop()
            x_alpha = best
            x_beta = second
            x_delta = third

            self._update_solutions(copy.deepcopy([x_alpha, x_beta, x_delta]), self.__transfrom_solution)

            # Update solution set with best solution
            if x_alpha.fitness > old_best_fitness:
                print("New best solution: " + str(x_alpha.fitness))
                print("New best solution (blackbox): " + str(x_alpha.fitness_blackbox))
                old_best_fitness = x_alpha.fitness

            self._add_best(x_alpha)

            i = (i + 1) % loop_iteration_count

            if self.__with_elim:
                # Elimination
                R = int(np.random.random() * len(solutions) * self.__elim_scale)
                solutions = solutions[0:len(solutions) - R]
                solution_gen = SolutionGenerator(self.evaluator)
                for _ in itertools.repeat(None, R):
                    solutions.append(self.__generate_eval(solution_gen, intervals, i_all))

            i_all += 1

        print("Terminiere!")
        return x_alpha

    def __mutate_strategy(self, strategies):
        """
        Mutate strategies vector.
        """
        selection_index = int(np.random.random() * len(strategies))
        strategies[selection_index] = int(np.random.random() * len(Strategy.selectables()))

    @property
    def mutation_history(self):
        return self._mut_history


class GreyWolfOptimizerBlackbox(AlgorithmBlackbox, GreyWolfOptimizer):
    """
    Blackbox implementation of the proposed GWO
    """

class GreyWolfOptimizerPareto(AlgorithmPareto, GreyWolfOptimizer):
    """
    Pareto implementation of the proposed GWO
    """