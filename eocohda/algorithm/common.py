"""
Common optimization methods: Contains for example NSGA-II Methods for Metaheuristic optimization
algorithms. Ideal for solving multicriteria problems with two criteria.
"""

def select_solutions(population, target_size):
    """
    Adapted source code from:
    'https://github.com/aarongarrett/inspyred/blob/master/inspyred/ec/replacers.py'.
    """
    if not isinstance(population[0].fitness, list):
        population.sort(key=lambda solution: solution.fitness, reverse=True)
        for solution in population:
            solution.front_num = 0
        return population[0:target_size]

    survivors = []

    fronts = []
    pop = set(range(len(population)))
    i = 0
    while pop:
        front = []
        for first in pop:
            dominated = False
            for second in pop:
                if population[first] < population[second]:
                    dominated = True
                    break
            if not dominated:
                front.append(first)
                population[first].front_num = i
        fronts.append([dict(individual=population[front_elem], index=front_elem) for front_elem in front])
        pop = pop - set(front)
        i += 1

    for _, front in enumerate(fronts):
        if len(survivors) + len(front) > target_size:
            # Determine the crowding distance.
            distance = [0 for _ in range(len(population))]
            individuals = list(front)
            num_individuals = len(individuals)
            num_objectives = len(individuals[0]['individual'].fitness)
            for obj in range(num_objectives):
                individuals.sort(key=lambda x: x['individual'].fitness[obj])
                distance[individuals[0]['index']] = float('inf')
                distance[individuals[-1]['index']] = float('inf')
                for j in range(1, num_individuals-1):
                    distance[individuals[j]['index']] = (distance[individuals[j]['index']] +
                                                        (individuals[j+1]['individual'].fitness[obj] -
                                                        individuals[j-1]['individual'].fitness[obj]))

            crowd = [dict(dist=distance[front_elem['index']], index=front_elem['index']) for front_elem in front]
            crowd.sort(key=lambda x: x['dist'], reverse=True)
            last_rank = [population[c['index']] for c in crowd]
            rank = 0
            num_added = 0
            num_left_to_add = target_size - len(survivors)
            while rank < len(last_rank) and num_added < num_left_to_add:
                if last_rank[rank] not in survivors:
                    survivors.append(last_rank[rank])
                    num_added += 1
                rank += 1
            if len(survivors) == target_size:
                break
        else:
            for front_elem in front:
                if front_elem['individual'] not in survivors:
                    survivors.append(front_elem['individual'])
    return survivors

def calcNondominatedSolutions(population):
    """
    Calculates the nondominated solution of the population.

    :param population: the population of solutions
    """
    front = []
    for first in population:
        dominated = False
        for second in population:
            if first < second:
                dominated = True
                break
        if not dominated:
            front.append(first)

    return front

def calcDominateNumberFor(nondominatedSolutions, population):
    """
    Calculated the number of dominated solutions in the population by the
    nonDominatedSolutions
    """
    num = 0
    for first in population:
        dominated = False
        for second in nondominatedSolutions:
            if first < second:
                dominated = True
                break
        if dominated:
            num += 1

    return num

def calcDominateNumber(populationOne, populationTwo):
    """
    Calc the number of solutions which are dominated in populationTwo w.r.t the
    nondominated solutions in populationOne
    :param populationOne: first population
    :param populationTwo: second population
    """
    return calcDominateNumberFor(calcNondominatedSolutions(populationOne), populationTwo)