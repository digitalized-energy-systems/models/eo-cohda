"""
Implementations of the optimization algorithms.
"""

import itertools
import copy
import random
from statistics import mean
import math

import numpy as np
from overrides import overrides

from eocohda.core.schedule import Schedule, Strategy
from eocohda.core.algorithm import Algorithm, SolutionRepairer, AlgorithmBlackbox, AlgorithmPareto
import eocohda.algorithm.common as common
from eocohda.algorithm.stopping import createDefaultStopping

class EvoAlgorithm(Algorithm):
    """
    Evolutionary algorithm for solving the energy scheduling problem for
    energystorage-based systems.
    """

    def __init__(self,
                 evaluator,
                 solution_size=64,
                 gen_size=32,
                 parent_num=2,
                 mutation_style=1,
                 stopping=None,
                 iteration_count=100,
                 **options):
        """
        Initializes the algorithm with some algorithm-specific parameter. Also
        passes a dictionary with domain-specific parameter.
        """
        super().__init__(evaluator, **options)
        self._parent_num = parent_num
        self._gen_size = gen_size
        self._solution_size = solution_size
        self.__stopping = createDefaultStopping(stopping, iteration_count)
        self.repairer = SolutionRepairer(evaluator)
        self.pop_history = []
        self.restart_points = []
        self.mutation_style = mutation_style

    def __mutate_solution(self, old_solution):
        """
        Mutation-Operation: simple random mutation of a single value in the schedule
        """

        mutation_size = old_solution.step_size * np.exp(0.22*np.random.normal(0, 1))
        old_solution.step_size = mutation_size
        if np.random.random() < 0.5:
            if self.mutation_style <= 1:

                # Chose slot which is not part of the constraint_schedule
                selection_slot = None
                while selection_slot is None or not old_solution.candidate.slots[selection_slot].strategy.selectable:
                    selection_slot = int(np.random.random() * old_solution.candidate.size())
                storage = copy.copy(self.evaluator.get_storage())
                if selection_slot > 0:
                    storage.load = old_solution.candidate.get_slot(selection_slot - 1).load_state
                possible_load_interval = storage.possible_load_one_step_at(selection_slot)

                # Mutate with gaussian brush
                theta = int(max(1, np.random.normal(0, 1) * old_solution.candidate.size()))
                gauss_brush = abs(possible_load_interval[1] - possible_load_interval[0]) * mutation_size * np.array([random.gauss(0, 1) for _ in range(int(theta))])
                for i in range(selection_slot, min(selection_slot + theta, old_solution.candidate.size())):
                    old_solution.candidate.slots[i].load_state += gauss_brush[i-selection_slot]
                old_solution.candidate = self.repairer.repair_strategies(self.repairer.repair_max_power(old_solution.candidate))

                # Random strategy
                selection_slot = None
                while selection_slot is None or not old_solution.candidate.slots[selection_slot].strategy.selectable:
                    selection_slot = int(np.random.random() * old_solution.candidate.size())
                old_slot = old_solution.candidate.slots[selection_slot]
                storage = copy.copy(self.evaluator.get_storage())
                if selection_slot > 0:
                    storage.load = old_solution.candidate.get_slot(selection_slot - 1).load_state
                old_slot.strategy = self.repairer.select_strategy(storage.load, old_slot, selection_slot, only_random=True)

            old_solution.fitness = self._eval(old_solution)
        else:
            old_solution.fitness = self._eval(old_solution)
            if self.mutation_style >= 1 and self.mutation_style <= 2:
                # Perform additional local search
                old_solution = self.__local_search(old_solution)

        return old_solution

    def __annealing_func(self, x):
        return 1 / (1 + np.exp(-(1.5*x-5)))
        #return 1

    def __local_search(self, old_solution):
        # Choose slot which is not part of the constraint_schedule
        selection_slot = None
        while selection_slot is None or not old_solution.candidate.slots[selection_slot].strategy.selectable:
            selection_slot = int(np.random.random() * old_solution.candidate.size())
        select_fitness = int(np.random.random() * (len(old_solution.fitness) if isinstance(old_solution.fitness, list) else 1))

        storage = self.evaluator.get_storage().copy()
        if selection_slot > 0:
            storage.load = old_solution.candidate.get_slot(selection_slot - 1).load_state
        possible_load_interval = storage.possible_load_one_step_at(
            selection_slot)
        best_solution = old_solution
        original_slot = old_solution.candidate.get_slot(selection_slot)

        # Search for best with resolution of 1/10
        for i in range(5):
            for s in possible_load_interval:
                new_load = original_slot.load_state + self.__annealing_func(i) * (s-original_slot.load_state)
                new_solution = old_solution.copy()
                mutate_slot = new_solution.candidate.get_slot(selection_slot)
                mutate_slot.load_state = new_load
                if selection_slot + 1 < old_solution.candidate.size():
                    self.repairer.offset_following(new_solution.candidate, selection_slot + 1, new_load - original_slot.load_state)
                new_solution.candidate = self.repairer.repair_strategies(self.repairer.repair_max_power(new_solution.candidate))
                new_solution.fitness = self._eval(new_solution, check_valid=False)
                if select_fitness == 0:
                    if best_solution.fitness < new_solution.fitness:
                        best_solution = new_solution
                elif best_solution.fitness[select_fitness] < new_solution.fitness[select_fitness]:
                    best_solution = new_solution

        return best_solution

    def _select_solution(self, population):
        """
        Selection-Operation: selects the best solutions
        """
        fitness_map = list(map(lambda solution: [solution, solution.fitness], population))
        fitness_map.sort(key=lambda x: x[1], reverse=True)
        sorted_pop = list(map(lambda solution: solution[0], fitness_map))
        if self._solution_size > 1:
            select = sorted_pop[:self._solution_size - 1] + [sorted_pop[-1]]
            return select
        else:
            return sorted_pop[:self._solution_size]

    def __recombine(self, solutions, iteration_num):
        """
        Crossover-Operation:
        * first searches for intersections in the load-state arrays of the parents
        * cuts the schedules and tries to combine the resulting parts
        """
        # If only one parent just copy it
        if len(solutions) == 1:
            return self._create_solution(solutions[0].copy().candidate, solutions,
                                            iteration_num=iteration_num,
                                            step_size=solutions[0].step_size)

        load_states = list(map(lambda solution: [slot.load_state for slot in solution.candidate.slots], solutions))
        cutting_points = []
        current_states = copy.deepcopy(load_states[0])
        for i in range(1, len(load_states)):
            merge_target = load_states[i]
            # Define search range
            search_range = int(len(load_states[0]) / len(solutions))
            search_mid = search_range * i
            start_search = search_mid - int(search_range / 2)
            end_search = max(search_mid + int(search_range / 2), start_search + 1)

            # Search for the index of the element with the lowest difference in the search space
            cutting_points.append(start_search + \
                                  self.__search_closest_number(current_states[start_search:end_search + 1], \
                                                               merge_target[start_search:end_search + 1]))

            # Merge list at cutting point[i]
            current_states[cutting_points[i - 1]:len(merge_target)] = merge_target[cutting_points[i - 1]:len(merge_target)]

        slot_list = []

        # Apply cutting_points on the parent solutions
        for i in range(len(cutting_points) + 1):
            start = 0 if i == 0 else cutting_points[i - 1]
            end = len(current_states) if i == len(cutting_points) else cutting_points[i]
            extend_list = solutions[i].candidate.slots_copy()[start:end]
            if slot_list and extend_list:
                extend_list[0].strategy = self.repairer.select_strategy(slot_list[-1].load_state, extend_list[0], 0)
            slot_list.extend(extend_list)

        new_candidate = Schedule(slot_list)
        self.repairer.repair_max_power(new_candidate)
        self.repairer.repair_strategies(new_candidate)

        new_step_size = mean([solution.step_size for solution in solutions])

        return self._create_solution(new_candidate, solutions, iteration_num=iteration_num, step_size=new_step_size)

    def __search_closest_number(self, first_list, second_list):
        """
        Searches the closest number in the integer list
        :param first_list: first list for comparison
        :param second_list: second list for comparison
        :returns: index of the closest number
        """
        return np.argmin(np.absolute(np.array(first_list) - np.array(second_list)))

    def __select_parents(self, pop, parent_num):
        np.random.shuffle(pop)
        select_num = parent_num if len(pop) >= parent_num else len(pop)
        return pop[0:select_num]

    @overrides
    def generate_schedules(self, start, res, intervals, state):
        """
        Start the algorithm.
        """
        if start == 0 or not isinstance(start, list):
            pop = self._generate_starting_solutions(intervals, self._solution_size)
        else:
            pop = start
            for i in range(len(pop)):
                solution = pop[i]
                solution.fitness = self._eval(solution)

        self.pop_history.append(pop)

        best = None
        old_best = None
        restart_counter = -1
        last_fitness = None
        progress = 99999999
        i = 0

        while not self.__stopping.stop(pop):
            if self.blackbox.has_changed():
                for i in range(len(pop)):
                    solution = pop[i]
                    solution.fitness = self._eval(solution)
            
            new_pop = []

            for _ in range(0, self._gen_size):
                parents = self.__select_parents(pop, self._parent_num)
                solution = self.__recombine(parents, i)
                solution = self.__mutate_solution(solution)
                new_pop.append(solution)

            combined = []
            combined.extend(new_pop)
            combined.extend(pop)

            pop = self._select_solution(combined)
            best = pop[0]

            self._update_solutions(pop)

            if old_best is None or old_best.fitness < best.fitness:
                print("New dominating fitness: " + str(best.fitness))
                print("New dominating fitness (original): " + str(best.fitness_blackbox))
                print("New dominating fitness (mutation): " + str(best.step_size))

            self._add_best(best)
            old_best = best

            if (restart_counter > 100 or restart_counter == -1) and self._solution_size > 1:
                new_progress = None
                if last_fitness is not None:
                    if isinstance(best.fitness, list):
                        new_progress = np.array(best.fitness) - np.array(last_fitness)
                    else:
                        new_progress = np.array([best.fitness - last_fitness])

                # restart if progress too slow
                if progress is not None and  restart_counter != -1 and (np.all(new_progress) == 0 or np.all(progress / new_progress <= 0.01)):
                    print("Restart population!")
                    restart_size = int(self._solution_size / 2)
                    pop = pop[0:restart_size]
                    pop.extend(self._generate_starting_solutions(intervals, restart_size))
                    self.restart_points.append((i, best.fitness))

                # update inidicators
                restart_counter = 0
                progress = new_progress
                last_fitness = best.fitness

            restart_counter += 1

            self.pop_history.append(pop)

            i += 1

        return best


class EvoAlgorithmSimple(Algorithm):
    """
    Evolutionary algorithm for solving the energy scheduling problem for
    energy-storage-based systems.
    """

    def __init__(self,
                 evaluator,
                 solution_size=64,
                 gen_size=32,
                 stopping=None,
                 iteration_count=100,
                 **options):
        """
        Initializes the algorithm with some algorithm-specific parameter. Also
        passes a dictionary with domain-specific parameter.
        """
        super().__init__(evaluator, **options)
        self._gen_size = gen_size
        self._solution_size = solution_size
        self.__stopping = createDefaultStopping(stopping, iteration_count)
        self.repairer = SolutionRepairer(evaluator)
        self.pop_history = []

    def __mutate_solution(self, solution, iter):
        """
        Mutation-Operation: simple random mutation of a single value in the schedule
        """
        new_solution = self._create_solution(solution.copy().candidate, iteration_num=iter, parents=[solution],
                                            step_size=solution.step_size)
        mutation_size = new_solution.step_size * np.exp(0.22*np.random.normal(0, 1))
        new_solution.step_size = mutation_size
        storage = copy.copy(self.evaluator.get_storage())
        theta = max(1, min(int(new_solution.candidate.size() * mutation_size * random.random()), new_solution.candidate.size()))
        start = max(0, min((int(new_solution.candidate.size() * random.random()) - int(theta/2)), new_solution.candidate.size() - theta))
        if start > 0:
            storage.load = new_solution.candidate.get_slot(start - 1).load_state
        possible_load_interval = storage.possible_load_one_step_at(start)

        # Mutate with random-sized schedule
        random_array = abs(possible_load_interval[1] - possible_load_interval[0]) * mutation_size * np.array([random.random() - 0.5 for _ in range(int(theta))])
        for i in range(theta):
            new_solution.candidate.slots[i + start].load_state += random_array[i]

        # Chose slot which is not part of the constraint_schedule
        for i in range(new_solution.candidate.size()):
            if random.random() < mutation_size:
                old_slot = new_solution.candidate.slots[i]
                old_slot.strategy = self.repairer.select_strategy(self.evaluator.get_storage().load if i == 0 else new_solution.candidate.slots[i-1].load_state, old_slot, i, only_random=True)

        new_solution.candidate = self.repairer.repair_strategies(self.repairer.repair_max_power(new_solution.candidate))
        new_solution.fitness = self._eval(new_solution)

        return new_solution

    def _select_solution(self, population):
        """
        Selection-Operation: selects the best solutions
        """
        fitness_map = list(map(lambda solution: [solution, solution.fitness], population))
        fitness_map.sort(key=lambda x: x[1], reverse=True)
        sorted_pop = list(map(lambda solution: solution[0], fitness_map))
        if self._solution_size > 1:
            select = sorted_pop[:self._solution_size - 1] + [sorted_pop[-1]]
            return select
        else:
            return sorted_pop[:self._solution_size]


    def __select_mutation_target(self, pop):
        np.random.shuffle(pop)
        return pop[0]

    @overrides
    def generate_schedules(self, start, res, intervals, state):
        """
        Start the algorithm.
        """
        if start == 0 or not isinstance(start, list):
            pop = self._generate_starting_solutions(intervals, self._solution_size)
        else:
            pop = start
            for i in range(len(pop)):
                solution = pop[i]
                solution.fitness = self._eval(solution)

        self.pop_history.append(pop)

        best = None
        old_best = None
        i = 0

        while not self.__stopping.stop(pop):
            if self.blackbox.has_changed():
                for i in range(len(pop)):
                    solution = pop[i]
                    solution.fitness = self._eval(solution)
            
            new_pop = []

            for _ in range(0, self._gen_size):
                sel_solution = self.__select_mutation_target(pop)
                solution = self.__mutate_solution(sel_solution, i)
                new_pop.append(solution)

            combined = []
            combined.extend(new_pop)
            combined.extend(pop)

            pop = self._select_solution(combined)
            best = pop[0]

            self._update_solutions(pop)

            if old_best is None or old_best.fitness < best.fitness:
                print("New dominating fitness: " + str(best.fitness))
                print("New dominating fitness (original): " + str(best.fitness_blackbox))
                print("New dominating fitness (mutation): " + str(best.step_size))

            self._add_best(best)
            old_best = best

            self.pop_history.append(pop)

            i += 1

        return best


class EvoAlgorithmSimpleBlackbox(AlgorithmBlackbox, EvoAlgorithmSimple):
    """
    Implementation of the blackbox-cohda-variant. Defines a new
    evaluation method with normalized local and global fitness-values
    """

class EvoAlgorithmSimpleParetoNSGA(AlgorithmPareto, EvoAlgorithmSimple):
    """
    Implementation of the Pareto-Front-cohda-variant. Defines a
    new selection method based on NSGA-II
    """

    @overrides
    def _select_solution(self, population):
        return common.select_solutions(population, self._solution_size)


class EvoAlgorithmBlackbox(AlgorithmBlackbox, EvoAlgorithm):
    """
    Implementation of the blackbox-cohda-variant. Defines a new
    evaluation method with normalized local and global fitness-values
    """

class EvoAlgorithmParetoNSGA(AlgorithmPareto, EvoAlgorithm):
    """
    Implementation of the Pareto-Front-cohda-variant. Defines a
    new selection method based on NSGA-II
    """

    @overrides
    def _select_solution(self, population):
        return common.select_solutions(population, self._solution_size)
