"""
Implementation of an optimization algorithm for the multi objective local optimization using
the FireFly heuristic.

@inproceedings{yang2009firefly,
  title={Firefly algorithms for multimodal optimization},
  author={Yang, Xin-She},
  booktitle={International symposium on stochastic algorithms},
  pages={169--178},
  year={2009},
  organization={Springer}
}
"""
import copy
import random

from scipy.sparse import rand
import numpy as np
import math
from overrides import overrides

from eocohda.core.simulator import ScheduleEnergySimulator, LoadStateEnergySimulator
from eocohda.core.algorithm import Algorithm, Solution, SolutionRepairer, AlgorithmPareto, AlgorithmBlackbox
from eocohda.core.schedule import Slot, Schedule, Strategy
from eocohda.algorithm.stopping import MutualDominaceRateIndicator
from eocohda.algorithm.stopping import createDefaultStopping
import eocohda.algorithm.common as common


class FireFly(Algorithm):
    """
    Implementation of the FireFly heuristic modified and extended for the energy storage scheduling.
    """

    MIN_ATTRACTIVENESS = 0

    def __init__(self,
                evaluator,
                n=32,
                y=1,
                alpha=1,
                delta=0.99,
                mu=1.22,
                enable_brush=True,
                enable_adaptive_theta=True,
                stopping=None,
                iteration_count=1000,
                **options):
        """
        :param evaluator: objective function represented as Evaluator object
        :param n: population size
        :param y: light absoption coefficient
        :param max_generation: number of iterations
        :param alpha: starting value of alpha, which is the weight of the random part of the update equation
        :param delta: decreasing factor for alpha
        :param options: options
        """
        super().__init__(evaluator, **options)

        self.__n = n
        self.__y = y
        self.__alpha = alpha
        self.__delta = delta
        self.__stopping = createDefaultStopping(stopping, iteration_count)
        self.__repairer = SolutionRepairer(evaluator)
        self._mut_history = []
        self.__enable_brush = enable_brush
        self.__mu = mu
        self.__enable_adaptive_theta = enable_adaptive_theta

    def __transfrom_solution(self, solution):
        solution.candidate = FireFly.from_firefly_solution(solution.candidate)
        return solution

    @staticmethod
    def from_firefly_solution(firefly_solution):
        slots = []
        for i in range(len(firefly_solution[0])):
            element = firefly_solution[0][i]
            strategy_id = firefly_solution[1][i]
            slots.append(Slot(element,
                              i * 15,
                              (i + 1) * 15,
                              Strategy.from_id(strategy_id)))

        return Schedule(slots)

    @staticmethod
    def to_firefly_solution(norm_solution):
        firefly_solution = []
        firefly_solution_strategy = []
        for slot in norm_solution.slots:
            firefly_solution.append(slot.load_state)
            firefly_solution_strategy.append(slot.strategy.id)
        return [np.array(firefly_solution), firefly_solution_strategy]

    def __fitness_len(self, fitness):
        if (isinstance(fitness, list)):
            return len(fitness)
        return 1

    def __generate_zero_solution(self, intervals):
        zero_solution = self._create_solution(FireFly.to_firefly_solution(Schedule([Slot(0, 0, 0, strategy=Strategy.DISCHARGE) for i in range(intervals)])))
        zero_solution.fitness = self._eval(zero_solution, solution_map=FireFly.from_firefly_solution)
        return zero_solution

    @overrides
    def generate_schedules(self, start, res, intervals, state):
        y = self.__y
        P = self._generate_starting_solutions(intervals, self.__n - 1, solution_map=FireFly.to_firefly_solution, solution_backmap=FireFly.from_firefly_solution)
        P.append(self.__generate_zero_solution(intervals))
        P = common.select_solutions(P, len(P))
        alpha = self.__alpha
        theta = 10
        best = None
        dominance_indicator = MutualDominaceRateIndicator()
        old_best = None
        improvement = 0
        success_rate = 1
        mutation_rate = self.__mu
        g = 0

        print("New Optimization!")

        self._mut_history.append(copy.deepcopy(P))

        while not self.__stopping.stop(P):
            i_value = dominance_indicator.compute_indicator(P)

            P_copy = copy.deepcopy(P)

            if self.__enable_adaptive_theta:
                if success_rate > 1/5:
                    theta = theta * mutation_rate
                else:
                    theta = theta / mutation_rate

            for i in range(self.__n):

                for j in range(i):

                    first = P[i].candidate[0]
                    second_copy = P_copy[j].candidate[0]
                    r = self.__distance(first, second_copy)

                    if P[i].fitness < P[j].fitness:
                        # MOVE
                        P[i].candidate[0] = self.__calc_update(first, second_copy, y, r, alpha, intervals, theta)
                        self.__mutate_strategy(P[i].candidate[1])
                        P[i].id = self._next_id()
                        P[i].iteration = g

            if i_value <= 0:
                random_weights = np.random.random(self.__fitness_len(P[0].fitness))
                random_weights_one = random_weights / np.sum(random_weights)
                best_index = np.argmax(np.array([firefly.fitness * random_weights_one for firefly in P]))
                best = P[best_index].candidate
                best_new = copy.deepcopy(best)
                best_new[0] = best_new[0] + alpha * (np.random.random(len(best_new[0])) - 0.5)
                P[self.__n - 1] = self._create_solution(best_new, iteration_num=g)
                self.__cut_to_range(P[self.__n - 1])
            else:
                improvement += 1

            for i in range(self.__n):
                self.__cut_to_range(P[i])
                P[i].candidate = FireFly.to_firefly_solution(self.__repairer.repair_strategies(self.__repairer.repair_max_power(FireFly.from_firefly_solution(P[i].candidate))))
                P[i].fitness = self._eval(P[i], FireFly.from_firefly_solution)

            alpha *= self.__delta

            # Searches for the best and remember it
            P = common.select_solutions(P, len(P))
            best = P[0]
            self._update_solutions(copy.deepcopy(P), self.__transfrom_solution)

            # Update solution set with best solution
            if old_best is None or best.fitness > old_best.fitness:
                print("New best solution: " + str(best.fitness))
                print("New best solution: " + str(best.fitness_blackbox))
                old_best = copy.copy(best)

            self._add_best(old_best)

            g += 1

            success_rate = improvement/g

        return best

    def __mutate_strategy(self, strategies):
        """
        Mutate strategies vector.
        """
        selection_index = int(np.random.random() * len(strategies))
        strategies[selection_index] = int(np.random.random() * len(Strategy.selectables()))

    def __calc_update(self, first, second, y, r, alpha, intervals, theta):
        """
        Calculates the movement the first firefly should do toward to the second firefly.

        :param first: vector describing the position of the first firefly
        :param second: vector describing the position of the second firefly
        :param y: light absorption
        :param r: distance between first and second
        :param alpha: impact of the random part of the update equation
        """
        attr = self.__attractiveness(r, y)
        return first * (1 - attr) + attr * second + alpha * (self.__gen_random(intervals, theta))

    def __gen_random(self, intervals, theta):
        if self.__enable_brush:
            theta = min(max(theta, 1), intervals)
            start_index = int(np.random.random() * intervals)
            gauss_brush = [random.gauss(0, 0.5) for _ in range(int(theta))]
            random_brush = np.zeros(intervals)
            random_brush[start_index:min(start_index+int(theta), intervals)] = gauss_brush[0:min(intervals-start_index, int(theta))]
            return random_brush
        else:
            return np.random.random(intervals) - 0.5

    def __attractiveness(self, r, y):
        """
        Calculates the attractiveness with the distance r and light absorption y

        :param r: distance
        :param y: light absorption coefficient
        """
        if r == 0:
            return 1

        return (self.__attractiveness(0, y) - FireFly.MIN_ATTRACTIVENESS) * math.exp(-y * r**2) + FireFly.MIN_ATTRACTIVENESS

    def __distance(self, first_solution, second_solution):
        """
        Calculates the distance using the 2-norm

        :param first_solution: first solution
        :param second_solution: second solution
        """
        return np.linalg.norm(first_solution - second_solution)

    def __cut_to_range(self, solution):
        """
        Clips every value in the solution candidate vector to the range of allowed values.

        :param solution: solution object
        """

        min_value = 0
        max_value = 1

        for i in range(len(solution.candidate[0])):
            solution.candidate[0][i] = np.clip(solution.candidate[0][i], min_value, max_value)

    @property
    def mutation_history(self):
        return self._mut_history

class FireFlyBlackbox(AlgorithmBlackbox, FireFly):
    """
    Blackbox implementation of Firefly.
    """

class FireFlyPareto(AlgorithmPareto, FireFly):
    """
    Firefly searching for pareto-front with multiple objectives.
    """

