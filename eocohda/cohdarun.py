"""Main process module of Smart Trading Service."""
import asyncio
import time

from os.path import isfile
import io
import json
import logging
import lzma
import os
import inspect

import aiomas

import eocohda.isaac.isaac_util.debug as debug
import eocohda.isaac.isaac_util.util as util

from eocohda.isaac.core.unit import UnitAgent
from eocohda.isaac.controller.controller import ControllerAgent
from eocohda.isaac.observer.observer import ObserverAgent

class COHDARun:

    def __init__(self, config, add_port=0):
        self.__config = config
        self.__add_port = add_port

    def main(self):
        """Open VPP multi-agent system."""
        start_time = time.time()
        try:
            # set log_level
            logging.getLogger('').setLevel('DEBUG')
            # set file handler
            logging.getLogger('').addHandler(util.get_log_file_handler('isaac_standalone.log'))
            # set console handler
            logging.getLogger('').addHandler(util.get_log_console_handler())
            print('[main] starting aiomas')
            aiomas.run(until=self.__run())
        except KeyboardInterrupt as e:
            print('[main] Interrupting execution.')
            raise e
        finally:
            print('[main] Simulation done')
            print('[main] Execution Time: %s.' % (str(time.time() - start_time)))


    def __read_target_schedule(self, path_to_target_file, n_sim_steps=96):
        """
        Read taget file and return target schedule and weights
        """
        assert isfile(path_to_target_file)
        # open the target file and read the header
        my_open = lzma.open if path_to_target_file.endswith('.xz') else io.open
        target_file = my_open(path_to_target_file, 'rt')
        line = next(target_file).strip()
        targets_meta = json.loads(line)
        assert targets_meta['interval_minutes'] == 15

        # create initial lists
        target_schedule = [0.0] * n_sim_steps
        weights = [1.0] * n_sim_steps
        # load the target schedule
        sum = 0
        for i in range(n_sim_steps):
            data = next(target_file).strip().split(',')
            target_schedule[i] = abs(float(data[0]))
            weights[i] = float(data[1])
            sum += abs(target_schedule[i]) * weights[i]

        print("Die Gesamtsumme des Fahrplans liegt bei %s" % (str(sum)))

        return target_schedule, weights


    async def __run(self):

        """Process handling."""
        # DEBUG: create container specific clock
        start = self.__config.CLOCK['start']
        stop = self.__config.CLOCK['stop']
        speed_up = self.__config.CLOCK['speed_up']
        clock = debug.DebuggingClock(start, stop, speed_up)

        # Details for controller / observer
        my_controller_config = self.__config.CTRL_CONFIG

        my_observer_cls = ObserverAgent
        my_observer_config = self.__config.OBS_CONFIG

        # Details for unitModel and Planner
        my_unit_model_cls = self.__config.UNIT_MODEL_LIST
        unit_model_config_list = self.__config.UNIT_MODEL_CONFIG_LIST

        my_planner_cls = self.__config.PLANNER_CLS
        my_planner_config = self.__config.PLANNER_CONFIG

        # create local container for controller and observer
        print('[run] Creating one container for Controller / Observer... ', end='')
        host = self.__config.CTRL_OBS_CONTAINER['host'] 
        port = self.__config.CTRL_OBS_CONTAINER['port'] + self.__add_port
        container_1 = await aiomas.Container.create(
            (host, port), codec=aiomas.MsgPack, clock=clock,
            extra_serializers=util.get_extra_serializers(), as_coro=True)
        print('Done!')

        # create local container for unitAgents
        print('[run] Creating %s container for unitAgents... ' % len(self.__config.AGENT_CONTAINER), end='')
        container_config = self.__config.AGENT_CONTAINER
        n_container = len(container_config)
        agent_container = []
        for i in range(n_container):
            host = container_config[i]['host']
            port = container_config[i]['port'] + self.__add_port
            container = await aiomas.Container.create(
                (host, port), codec=aiomas.MsgPack, clock=clock,
                extra_serializers=util.get_extra_serializers(), as_coro=True)
            agent_container.append(container)
        print('Done!')
        # instantiate controller / observer
        print('[run] Initializing Controller / Observer... ', end='')
        ctrl, obs = await ControllerAgent.factory(
            container_1,
            my_controller_config,
            my_observer_cls,
            my_observer_config
        )
        print('Done!')

        # instantiate UnitAgents
        print('[run] Initializing UnitAgents.')

        for i in range(len(unit_model_config_list)):
            # get details from general_unit_model_config set in config
            agents_unit_model_config = unit_model_config_list[i]

            # create uni_agent
            unit_agent = await UnitAgent.factory(
                agent_container[i % n_container],
                ctrl_agent_addr=ctrl.addr,
                obs_agent_addr=obs.addr,
                unit_model=(my_unit_model_cls[i], agents_unit_model_config),
                unit_if=None,
                planner=(my_planner_cls, my_planner_config),
                unit_name=unit_model_config_list[i]['name']
            )
            del unit_agent

        # run agents and services until manually cancelled or stop time is reached
        try:
            print('[run] Wait for all agents to be registered... ', end='')
            await ctrl._agents_registered
            await ctrl._observer_registered
            print('Done')

            print('[run] Call controller to run negotiation...')

            neg_counter = 0
            for negotiation in sorted(self.__config.NEGOTIATIONS, key=lambda x: x['date']):
                neg_counter += 1
                target_file_path = negotiation['target']
                startdate = negotiation['date']
                # read the target schedule
                target_schedule, weights = self.__read_target_schedule(target_file_path)
                print('\n**********  %s. NEGOTIATION ***********' % neg_counter)
                print('Date: %s\nTarget: %s\n' % (startdate, target_file_path))
                await ctrl.run_negotiation(startdate, target_schedule, weights)

            print('\n**********  END NEGOTIATIONS ***********\n')

        except KeyboardInterrupt:
            print('[run] Interrupting execution.')
        finally:
            # gracefully cancel async processes
            print('[run] Stopping planner tasks if still running... ', end='')
            futs = [a.stop() for a in ctrl._agents.keys()]
            await asyncio.gather(*futs)
            print('Done!')

            print('[run] Stopping controller and observer tasks... ', end='')
            await ctrl.stop()
            await obs.stop()
            print('Done!')

            # close all connections and shutdown server
            print('[run] Shutting down containers... ', end='')
            await container_1.shutdown(as_coro=True)
            for i in range(n_container):
                await agent_container[i].shutdown(as_coro=True)
            print('Done!')
