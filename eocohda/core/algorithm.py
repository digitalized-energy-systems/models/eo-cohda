"""
Core-Classes for the algorithms.
"""
from abc import ABC, abstractmethod
import copy
import itertools

from overrides import overrides

import numpy as np

from eocohda.core.unit import BlackboxGlobal
from eocohda.core.schedule import Slot, Strategy, Schedule

class Solution:
    """
    Wraps a candidate object together with its fitness-value(s).
    """

    def __init__(self, candidate, id, parent_ids=None, fitness=None, front_num=None, fitness_blackbox=None, iteration=0, step_size=np.exp(0.22*np.random.normal(0, 1))):
        self.__candidate = candidate
        self.__fitness = fitness
        self.__id = id
        self.__parent_ids = parent_ids
        self.__front_num = front_num
        self.__fitness_blackbox = fitness_blackbox
        self.__iteration = iteration
        self.__step_size = step_size

    def __str__(self):
        return str(self.__candidate) + "Fitness: (%s)" % self.__fitness

    @property
    def step_size(self):
        """
        :returns step_size
        """
        return self.__step_size

    @step_size.setter
    def step_size(self, step_size):
        """
        Set step size
        :param step_size: step_size to set
        """
        self.__step_size = step_size

    @property
    def iteration(self):
        """
        :returns: iteration number the solution were created
        """
        return self.__iteration

    @iteration.setter
    def iteration(self, iteration):
        """
        :param iteration: new iteration
        """
        self.__iteration = iteration

    @property
    def fitness_blackbox(self):
        """
        :returns: current fitness_blackbox
        """
        return self.__fitness_blackbox

    @fitness_blackbox.setter
    def fitness_blackbox(self, fitness_blackbox):
        """
        :param fitness_blackbox: new fitness_blackbox
        """
        self.__fitness_blackbox = fitness_blackbox

    @property
    def front_num(self):
        """
        :returns: current front_num
        """
        return self.__front_num

    @front_num.setter
    def front_num(self, front_num):
        """
        :param front_num: new front_num
        """
        self.__front_num = front_num

    @property
    def candidate(self):
        """
        :returns: hold candidate
        """
        return self.__candidate

    @candidate.setter
    def candidate(self, candidate):
        """
        :param candidate: new candidate
        """
        self.__candidate = candidate

    @property
    def fitness(self):
        """
        :returns: current fitness of candidate
        """
        return self.__fitness

    @fitness.setter
    def fitness(self, fitness):
        """
        Set fitness of the solution
        """
        self.__fitness = fitness#TODO

    @property
    def id(self):
        """
        :returns: id of the solution
        """
        return self.__id

    @id.setter
    def id(self, id):
        """
        Set the id of the solution
        """
        self.__id = id

    @property
    def parent_ids(self):
        """
        :returns: ids of the parents
        """
        return self.__parent_ids

    @parent_ids.setter
    def parent_ids(self, parent_ids):
        """
        Set the parent ids of the solution
        """
        self.__parent_ids = parent_ids

    def __hash__(self):
        return self.__id

    def copy(self):
        candidate_copy = None
        if isinstance(self.__candidate, Schedule):
            candidate_copy = self.__candidate.copy()
        else:
            candidate_copy = copy.deepcopy(self.__candidate)

        return Solution(candidate_copy, self.__id, copy.copy(self.__parent_ids), self.__fitness, self.__front_num, self.__fitness_blackbox, self.__iteration, self.__step_size)

    def __eq__(self, other):
        return self.__id == other.id

    def __deepcopy__(self, add):
        return self.copy()

    def __lt__(self, other):
        if not isinstance(self.__fitness, list):
            return self.__fitness < other.__fitness
        return any([self.__fitness[i] < other.fitness[i] for i in range(len(self.__fitness))]) \
                and all([self.__fitness[i] <= other.fitness[i] for i in range(len(self.__fitness))])


class SolutionGenerator:
    """
    Generates a valid solution with fitness-value > 0
    """

    def __init__(self, evaluator):
        self.__evaluator = evaluator

    def generate(self, planning_horizon, constraint_schedule=None):
        """
        Generate a valid solution

        :returns: new random, but valid solution
        """
        if constraint_schedule is not None and constraint_schedule.size() != planning_horizon:
            raise ValueError("Constraint-Schedule length != planning_horizon")

        slots = []
        storage = copy.deepcopy(self.__evaluator.get_storage())
        for i in range(planning_horizon):
            constraint_slot = None if constraint_schedule is None else constraint_schedule.get_slot(i)
            if constraint_slot is not None:
                # add constrainted slot
                slots.append(Slot(constraint_slot.load_state, constraint_slot.start,
                                          constraint_slot.end, constraint_slot.strategy))

                # simulate
                self.__simulate_storage(constraint_slot.load_state, constraint_slot.strategy, storage)
            else:
                # generate new slot
                strategies = list(set(Strategy.selectables()) - set(self.__evaluator.blacklist()))
                np.random.shuffle(strategies)
                random_strategy = strategies[0]
                random_energy = 0
                if random_strategy.is_charge:
                    random_energy = min(storage.max_charge_at(i),
                        storage.load_until_cap_at(i))
                else:
                    random_energy = min(storage.max_discharge_at(i),
                        storage.remaining_load_at(i))
                random_energy = np.random.random() * random_energy
                if random_strategy == Strategy.CHARGE and (self.__evaluator.get_input_capacity_list() is None or random_energy > self.__evaluator.get_input_capacity_list()[i]):
                    random_energy = 0

                # simulate
                new_load = self.__simulate_storage(random_strategy, random_energy, storage)
                slots.append(Slot(new_load, i * 15, i * 15 + 15, random_strategy))

        return Schedule(slots)

    def __simulate_storage(self, strategy, energy, storage):
        # simulate
        if strategy.is_charge:
            return storage.charge(energy)
        else:
            return storage.discharge(energy)


class Algorithm(ABC):
    """
    Algorithm for optimizing energy storage scheduling.
    """

    def __init__(self, evaluator, **options):
        """
        Initializes Algorithm with an empty solution list
        """
        self._solutions = set()
        self._all_solutions = []
        self._initial_solutions = None
        self._blackbox = BlackboxGlobal(lambda x: 0, lambda x: 0, evaluator.get_storage())
        self._evaluator = evaluator
        self._start_solutions = options.pop('start_solutions', [])
        self._history = []
        self._best_history = []
        self._update_listener = lambda x: x
        self.__id_counter = 0

    @property
    def update_listener(self):
        return self._update_listener

    @update_listener.setter
    def update_listener(self, listener):
        self._update_listener = listener

    @property
    def history(self):
        """
        :returns: history of the algorithm
        """
        return self._history

    @property
    def best_history(self):
        """
        :returns: history of best solutions of the algorithm
        """
        return self._best_history

    def _add_best(self, best):
        """
        Add best of the current iteration
        """
        self._best_history.append(best)

    def _to_ids(self, parents):
        """
        :returns: ids of the solutions
        """
        if parents is None:
            return None
        return [parent.id for parent in parents]

    def _create_solution(self, schedule, parents=None, iteration_num=0, step_size=np.exp(0.22*np.random.normal(0, 1))):
        """
        :returns: new solution
        """
        solution = Solution(schedule, self._next_id(), self._to_ids(parents), iteration=iteration_num, step_size=step_size)
        self._history.append(solution)
        return solution

    def _next_id(self):
        """
        :returns: next valid id
        """
        self.__id_counter += 1
        return self.__id_counter - 1

    def _generate_starting_solutions(self,
                                    planning_horizon,
                                    size,
                                    solution_map=lambda x: x,
                                    solution_backmap=lambda x: x,
                                    exclude_initial=False):
        """
        Initializing-Operation: generates a number of !valid! solutions.
        If there are initial solutions, they are added additionally.

        :returns: array of valid solutions with the length size + start_solutions
        """
        pop = []
        solution_gen = SolutionGenerator(self.evaluator)
        for _ in itertools.repeat(None, size):
            solution = self._create_solution(solution_map(solution_gen.generate(planning_horizon)))
            solution.fitness = self._eval(solution, solution_map=solution_backmap)
            pop.append(solution)
        if not exclude_initial:
            pop.extend(self._start_solutions)
        return pop

    def _eval(self, solution, solution_map=lambda x: x, check_valid=True):
        """
        Evaluates the given solution.

        :param solution: solution to evaluate
        :returns: fitness of the solution
        """
        return self._evaluator.eval(solution_map(solution.candidate), check_valid=check_valid)

    @property
    def all_solutions(self):
        """
        :returns: all solutions (not only acceptables)
        """
        return self._all_solutions

    @property
    def evaluator(self):
        """
        :returns: evaluator
        """
        return self._evaluator

    @property
    def blackbox(self):
        """
        :returns: blackbox
        """
        return self._blackbox

    @blackbox.setter
    def blackbox(self, blackbox):
        """
        Set the blackbox-objective
        """
        self._blackbox = blackbox

    @property
    def solutions(self):
        """
        Get the current solutions of the algorithm
        """
        return self._solutions

    def _update_solutions(self, population, solution_map=lambda x: x):
        """
        Update the solution set, only adds a solution if
        it is acceptable (fitness > 0)
        """
        for solution in population:
            if self._local_accecptable(solution.fitness):
                self._solutions.add(solution_map(solution))
            self._all_solutions.append(solution)

        self._update_listener(self._solutions)

    def _local_accecptable(self, fitness):
        return self._evaluator.acceptable(fitness)

    @property
    def initial_solutions(self):
        """
        Get initial solutions of the algorithm
        """
        return self._initial_solutions

    @initial_solutions.setter
    def initial_solutions(self, initial_solutions):
        """
        Set the initials solutions as starting point of the
        optimization. Might be used to pre-generate schedules.

        :param initial_solutions: list of initial generated schedules (Schedule)
        """
        self._initial_solutions = initial_solutions

    @abstractmethod
    def generate_schedules(self, start, res, intervals, state):
        """
        Generates a number of schedules.

        :param start: starting time
        :param res: temporal resolution
        :param intervals: number of intervals
        :param state: state of the unit agent
        :returns: [id, utility, data]
        """
        pass

    def prepare(self, algorithm, start, res, intervals, state):
        """
        Prepares with generating good starting schedules and setting the threshold in case
        of overall system optimization.

        :param start: starting time
        :param res: temporal resolution
        :param intervals: number of intervals
        :param state: state of the unit agent
        :returns: [id, utility, data]
        """
        algorithm.generate_schedules(start, res, intervals, state)
        best_solution = algorithm.best_history[-1]
        new_threshold = best_solution.fitness * 0.5
        # Add best solution, to always ensure finding a valid solution
        best_solution.fitness = self._eval(best_solution)
        self._solutions.add(best_solution)
        self._all_solutions.append(best_solution)
        self._update_listener(self._solutions)
        print(f'Prepare, solution finished {new_threshold}')
        self._evaluator.update_threshold(new_threshold)

class AlgorithmPareto(Algorithm, ABC):
    """
    Base Class for ParetoOptimization Algorithms
    """

    def _eval(self, schedule, solution_map=lambda x: x, check_valid=True):
        return [super()._eval(schedule, solution_map, check_valid=check_valid), self._blackbox.blackbox_eocohda(solution_map(schedule.candidate))]

    @overrides
    def _update_solutions(self, population, solution_map=lambda x: x):
        for solution in population:
            if self._evaluator.acceptable(solution.fitness[0]) and solution.front_num == 0:
                self._solutions.add(solution_map(solution))
            self._all_solutions.append(solution)

        self._update_listener(self._solutions)

    @overrides
    def _local_accecptable(self, fitness):
        return self._evaluator.acceptable(fitness[0])


class AlgorithmBlackbox(Algorithm, ABC):
    """
    Base Class for BlackboxOptimization Algorithms
    """

    @overrides
    def _eval(self, solution, solution_map=lambda x: x, check_valid=True):
        local_fitness = super()._eval(solution, solution_map, check_valid=check_valid)
        local_fitness_normalized = self.conv(local_fitness / (self.evaluator.threshold() + 0.01))
        global_fitness = self._blackbox.blackbox_eocohda(solution_map(solution.candidate))
        global_fitness_normalized = self._blackbox.blackbox_eocohda_weighted(global_fitness)
        solution.fitness_blackbox = (local_fitness, global_fitness, global_fitness_normalized, local_fitness_normalized)
        return global_fitness_normalized + local_fitness_normalized

    def conv(self, x):
        return min(1, x)

    @overrides
    def _update_solutions(self, population, solution_map=lambda x: x):
        if self._evaluator.acceptable(population[0].fitness_blackbox[0]):
            self._solutions.add(solution_map(population[0]))
        for solution in population:
            self._all_solutions.append(solution)

        self._update_listener(self._solutions)

class SampleAlgorithm(Algorithm):
    """
    Random Sampling algorithm.
    """

    def __init__(self,
                 evaluator,
                 solution_size=10000,
                 **options):
        """
        Initializes the algorithm with some algorithm-specific parameter. Also
        passes a dictionary with domain-specific parameter.
        """
        super().__init__(evaluator, **options)
        self._solution_size = solution_size


    @overrides
    def generate_schedules(self, start, res, intervals, state):
        """
        Start the algorithm.
        """
        sample_set = self._generate_starting_solutions(intervals, self._solution_size)
        fitness_map = list(map(lambda solution: [solution, solution.fitness], sample_set))
        fitness_map.sort(key=lambda x: x[1], reverse=True)
        sorted_pop = list(map(lambda solution: solution[0], fitness_map))
        if self._solution_size > 1:
            select = sorted_pop[:self._solution_size - 1] + [sorted_pop[-1]]
        else:
            select = sorted_pop[:self._solution_size]

        if select[0].fitness <= 0:
            self._evaluator.update_threshold(select[0].fitness-1)

        self._update_solutions(sample_set)
        return sample_set[0]

class SolutionRepairer:
    """
    Sometimes you have a good solution which is not in the solution space
    of valid solutions. In this situation you often want to repair this
    solution without losing that much quality. This class implements some
    repair strategies used by the different algorithms.
    """

    def __init__(self, evaluator):
        self._blocked_schedule = evaluator.get_blocked_schedule()
        self._max_energy_charge_list = evaluator.get_input_capacity_list()
        self._storage = evaluator.get_storage()
        self._copy_storage = evaluator.get_storage().copy()
        self._blacklist = evaluator.blacklist()
        self._init_load_state = self._storage.load

    def repair(self, solution):
        """
        Repair solution with all available repair-strategies.
        :param solution: the solution you want to repair
        :returns: the solution itself
        """
        self.repair_limit(solution)
        self.repair_blocked(solution)
        self.repair_strategies(solution)
        self.repair_max_power(solution)
        return solution

    def repair_limit(self, solution):
        """
        Clip load state to interval [0, 1]

        :param solution: solution you want to repair
        :returns: solution
        """
        for slot in solution.slots:
            slot.load_state = np.clip(slot.load_state, 0, 1)
        return solution

    def repair_blocked(self, solution):
        """
        Search for divergencies between constraint schedule and solution.
        If it finds something it will set the solution to the constrainted slot.
        Note: this might break the solution regarding the strategy plausi. So to get
        a fully repaired solution you should call repair_load_state/repair_strategies
        afterwards

        :param solution: solution you want to repair
        :returns: solution
        """
        blocked_schedule = self._blocked_schedule
        if blocked_schedule is not None:
            for i in range(blocked_schedule.size()):
                slot = blocked_schedule.slots[i]
                if not slot.strategy.selectable:
                    solution.slots[i] = slot
        return solution

    def repair_load_state(self, solution):
        """
        Repairs load states when they does not fit to the strategies. The state will be
        set to the closest valid value.

        :param solution: solution you want to repair
        :returns: solution
        """
        for i in range(1, solution.size()):
            prev = solution.slots[i-1]
            slot = solution.slots[i]
            self.select_load_state(slot, prev)

        return solution

    def select_load_state(self, slot, prev):
        """
        Select valid load state for the slot.

        :param slot: slot you want to select a new slot for
        :param prev: previous slot
        """
        if prev is None:
            return
        if slot.strategy.is_charge and slot.load_state < prev.load_state:
            slot.load_state = prev.load_state
        if not slot.strategy.is_charge and slot.load_state > prev.load_state:
            copy_storage = self._storage
            copy_storage.load = prev.load_state
            slot.load_state = prev.load_state - copy_storage.min_load_one_step_discharge()

    def offset_following(self, candidate, start, offset):
        """
        Increase all slot load_states with the given offset, starting at 'start'
        :param candidate: schedule
        :param start: start index
        :apam offset: offset to increase
        """
        for i in range(start, candidate.size()):
            candidate.slots[i].load_state = np.clip(candidate.slots[i].load_state + offset, 0, 1)

    def repair_max_power(self, solution):
        """
        Repair load states with respect to the max charge/discharge constraint.

        :param solution: solution you want to repair
        :returns: solution
        """
        storage_copy = self._copy_storage
        for i in range(solution.size()):
            prev = self._storage.load if i == 0 else solution.slots[i - 1].load_state
            current = solution.slots[i].load_state
            storage_copy.load = prev
            load_min_max = storage_copy.possible_load_one_step()
            solution.slots[i].load_state = np.clip(current, load_min_max[0], load_min_max[1])
        return solution

    def repair_strategies(self, solution):
        """
        Repairs strategies when they does not fit to the load states. For every invalid
        strategy this method selects a random valid one.

        :param solution: solution you want to repair
        :returns: solution
        """
        prev_slot = None
        num = 0
        # Repair strategy of all slots if necessary
        for slot in solution.slots:
            if prev_slot is not None:
                slot.strategy = self.select_strategy(prev_slot.load_state, slot, num)
            else:
                slot.strategy = self.select_strategy(self._init_load_state, slot, num)
            prev_slot = slot
            num += 1
        return solution

    def select_strategy(self, prev_load, slot, num, only_random=False):
        """
        Select valid strategy for slot when previous slot is set to prev_load

        :param prev_load: load of the previous slot
        :param slot: target slot
        :returns: new strategy
        """
        copy_storage = self._copy_storage
        copy_storage.load = prev_load
        energy = copy_storage.apply_load(slot.load_state, lambda x: x)
        if (energy > 0):
            return self.__find_valid_charge_strategy(num, prev_load, slot, only_random)
        else:
            if not only_random and not slot.strategy.is_charge and slot.strategy not in self._blacklist:
                return slot.strategy
            strategies = [strategy for strategy in Strategy.filter(False, True) if strategy not in self._blacklist]
            np.random.shuffle(strategies)
            return strategies[0]

    def __find_valid_charge_strategy(self, num, prev_load, slot, only_random):
        current_strategy = slot.strategy
        current_load = slot.load_state
        if not only_random and current_strategy.is_charge and current_strategy != Strategy.CHARGE and current_strategy not in self._blacklist:
            return current_strategy

        strategies = [strategy for strategy in Strategy.filter(True, True) if strategy not in self._blacklist]
        storage = self._copy_storage
        storage.load = prev_load
        if self._max_energy_charge_list is None or self._max_energy_charge_list[num] <= storage.apply_load(current_load):
            if len(strategies) == 1 and strategies[0] == Strategy.CHARGE and self._max_energy_charge_list is not None:
                storage.load = prev_load
                storage.charge(self._max_energy_charge_list[num])
                slot.load_state = storage.load
            elif Strategy.CHARGE in strategies:
                strategies.remove(Strategy.CHARGE)
        elif current_strategy == Strategy.CHARGE:
            return Strategy.CHARGE
        np.random.shuffle(strategies)
        return strategies[0]
