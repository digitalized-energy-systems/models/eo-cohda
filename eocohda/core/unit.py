"""
EoCohda core classes and main entry-point of the application. Manages the
configuration and the different parts of the optimization process.
"""
import asyncio
import multiprocessing as mp
import queue as queue
from threading import Thread
from pathlib import Path
import time

import numpy as np
from overrides import overrides
from aiomas import expose

import eocohda.core.util as util
from eocohda.isaac.core.unit import UnitModel

class BlackboxGlobal:
    """
    Convenience Tool to calculate the global objective fitness of an eocohda-Schedule
    """

    def __init__(self, obj_func, weight_func, storage):
        self._storage = storage
        self._isaac_blackbox = obj_func
        self._weight = weight_func

    def blackbox_eocohda(self, candidate):
        """
        Applies the global objective to the solution (Solution)
        """
        return self._isaac_blackbox(util.convert_to_isaac(self._storage, candidate))

    def blackbox_eocohda_weighted(self, global_fitness):
        """
        Calculates the adaptive weighted sum of the local fitness and the global fitness
        """
        return self._weight(global_fitness)

    def has_changed(self):
        return False

class QueueBlackboxGlobal(BlackboxGlobal):
    """
    Convenience Tool to calculate the global objective fitness of an eocohda-Schedule
    """

    def __init__(self, initial_wm, queue, storage):
        super().__init__(initial_wm.objective_function_os, initial_wm.adaptive_weight, storage)

        self.__queue = queue
        self._has_changed = False

    def __update(self):
        try:
            new_wm = self.__queue.get(False)
            self._has_changed = True
        except queue.Empty:
            new_wm = None

        if new_wm is not None:
            self._isaac_blackbox = new_wm.objective_function_os
            self._weight = new_wm.adaptive_weight

    @overrides
    def blackbox_eocohda(self, candidate):
        """
        Applies the global objective to the solution (Solution)
        """
        self.__update()
        return super().blackbox_eocohda(candidate)

    @overrides
    def blackbox_eocohda_weighted(self, global_fitness):
        """
        Calculates the adaptive weighted sum of the local fitness and the global fitness
        """
        self.__update()
        return super().blackbox_eocohda_weighted(global_fitness)
    
    def has_changed(self):
        has_changed_now = self._has_changed
        self._has_changed = False
        return has_changed_now


class EnergyStorageUnitModel(UnitModel):
    """
    Unit Model of an energy storage.
    """

    def __init__(self, algorithm, storage, name, start_alg = None):
        """
        Initializes EnergyStorageUnitModel with an algorithm from the algorithm module
        implementing Algorithm.
        """
        super().__init__()
        self.__raw_solutions = algorithm.solutions
        self.__algorithm = algorithm
        self.__start_alg = start_alg
        self.__storage = storage
        self.__algorithm_task = None
        self.__schedule_cache = {}
        self.__name = name
        self.__rec_queue = None
        self.__send_queue = None

    @overrides
    def get_schedule(self, schedule_id):
        """
        Override
        """
        return self.__schedule_cache[schedule_id]

    def __update_solutions(self):
        try:
            new_solutions = self.__rec_queue.get(False)
        except queue.Empty:
            new_solutions = None

        if new_solutions is not None:
            self.__raw_solutions = new_solutions

    @overrides
    def get_schedules(self):
        """
        Override
        """
        self.__update_solutions()

        for solution in self.__raw_solutions:
            if solution not in self.__schedule_cache.values():
                self.__schedule_cache[len(self.__schedule_cache)] = solution

        return self.__convert_all_to_isaac_tripel()

    def __convert_all_to_isaac_tripel(self):
        return [self.__convert_to_isaac_tripel(key, value) for key, value in self.__schedule_cache.items()]

    def __convert_to_isaac_tripel(self, id, solution):
        return (id, solution.fitness, util.convert_to_isaac(self.__storage, solution.candidate))

    @overrides
    def on_terminate(self):
        """
        Override
        """
        self._running = False
        self.__process.join(1)
        self.__process.terminate()
        print("[run] Terminate: %s." % (self.__name))

        sol_path = Path("data/solutions/" + self.__name)
        sol_path.mkdir(parents=True, exist_ok=True)
        util.write_solutions(self.__schedule_cache, sol_path / str(self.save_time))

    @overrides
    def send(self, wm):
        self.__send_queue.put(wm, False)

    async def generate_schedules(self, start, res, intervals, state):
        """
        Override
        """
        print("Start: " + self.__name)

        # Queue for communication, sending new WorkingMemory
        # and receiving new solutions generated by algorithm
        self.__rec_queue = mp.Queue()
        self.__send_queue = mp.Queue()

        # Remove not serializable parts of the algorithm
        # (Sending data to another process requires pickling -> no lambdas)
        self.__algorithm.blackbox = None
        self.__algorithm.update_listener = None

        # Create subprocess and start
        self.__process = mp.Process(target=start_algorithm, args=(self.__algorithm,
                    self.__start_alg,
                    self.__rec_queue,
                    self.__send_queue,
                    start,
                    res,
                    intervals,
                    state,
                    self.wm,
                    self.__storage,
                    self.__name))
        self.__process.daemon = True
        self.__process.start()
        self._running = True

        # Wait until first usable schedule created -> without COHDA can't run
        while len(self.__raw_solutions) == 0 and self.__process.is_alive():
            self.__update_solutions()
            await asyncio.sleep(1)
            continue

        print("Init Done: " + self.__name)

        # (id, utility, schedule)
        return self.get_schedules()


def start_algorithm(algorithm,
                    prepare_algo,
                    send_queue,
                    rec_queue,
                    start,
                    res,
                    intervals,
                    state,
                    wm,
                    storage,
                    name):
    """
    Start the algorithm as process connected via two queues
    """
    print("Prepare")
    algorithm.update_listener = lambda x: send_queue.put(x, False)
    algorithm.blackbox = QueueBlackboxGlobal(wm, rec_queue, storage)
    if prepare_algo is not None:
        algorithm.prepare(prepare_algo, start, res, intervals, state)
    print('Prepare, solution finished')
    algorithm.generate_schedules(start, res, intervals, state)
    print("Stop-Kriterium: " + name)


class UnitModelStorageExtender(UnitModel):
    """
    Able to create a coupling between a master UnitModel
    and a set of Child-UnitModel's.

    Markierungen im EnergyStorage Schedule für InputStrom
    """

    def __init__(self, parent_unit_model, child_unit_models):
        """
        Override

        :param parent_unit_model: (EnergyStorageUnitModel)
        :param child_unit_models: [UnitModel]
        """
        super().__init__()
        self.__parent_unit_model = parent_unit_model
        self.__child_unit_models = child_unit_models

    @overrides
    def get_schedule(self, schedule_id):
        """
        Override
        """
        return self.__parent_unit_model.get_schedule(schedule_id)

    @overrides
    def get_schedules(self):
        """
        Override
        """
        return self.__parent_unit_model.get_schedules()

    @overrides
    def on_terminate(self):
        """
        Override
        """
        return self.__parent_unit_model.on_terminate()

    async def generate_schedules(self, start, res, intervals, state):
        """
        Override
        """
        power_plant_schedule = np.zeros(intervals)
        for unit_model in self.__child_unit_models:
            power_plant_schedule += unit_model.generate_schedules(self, start, res, intervals, state)

        self.__parent_unit_model.evaluator.set_input_capacity_list(power_plant_schedule)

        return self.__parent_unit_model.generate_schedules(self, start, res, intervals, state)
