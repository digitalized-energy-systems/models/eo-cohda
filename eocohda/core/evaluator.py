"""
Contains a number of Evaluators and EvaluatorPlugins for the Algorithm. Also contains
everything to evaluate a schedule.
"""
import copy

from interface import implements, Interface
from overrides import overrides
import numpy as np

from eocohda.core.schedule import Strategy, Schedule, Slot
from eocohda.core.simulator import SelfDischargeStorageSimulator, ConstraintCheckingScheduleSimulator, ScheduleEnergySimulator


class Evaluator(Interface):
    """
    Key-part of the optimization-process. It's responsible for creating a rating for a given solution.
    The rating must be an integer value, the scale does not matter. It's just important that
    the values are good quality measures.
    """

    def get_storage(self):
        """
        Return storage model used for the evaluation.
        :returns: storage (Storage)
        """
        pass

    def get_blocked_schedule(self):
        """
        Return model with blocked slots.
        :returns: schedule containing all blocked slots
        """
        pass

    def set_input_capacity_list(self, input_list):
        """
        Return list with energy limits the storage can get as input
        :returns: list of max energies which can be used with Strategy CHARGE
        """
        pass

    def get_input_capacity_list(self):
        """
        Return list with energy limits the storage can get as input
        :returns: list of max energies which can be used with Strategy CHARGE
        """
        pass

    def eval(self, solution, check_valid=True):
        """
        Evaluates the given solution an returns a rating (higher is better).

        :param solution: the solution to rate
        :param check_valid: if the validity have to get checked.
        :returns: a rating of the solution
        """
        pass

    def acceptable(self, local_fitness):
        """
        Checks whether a solution is acceptable
        :param local_fitness: fitness of the solution
        :returns: True if solution is acceptable, otherwise False
        """
        pass

    def threshold(self):
        """
        :returns: threshold
        """
        pass

    def blacklist(self):
        """
        :returns: strategy-blacklist
        """
        pass


class EvaluatorPlugin(Interface):
    """
    Plugin for the Evaluator. Generally the Evaluator is responsible for the Evaluation
    but if you add an EvaluatorPlugin you can add another evaluation-step to this process.
    Useful if you want to extend the rating process instead of replacing it.
    """

    def eval_step(self, solution, prior_rating):
        """
        Evaluates the given solution. Therefor this method can use the old rating
        of the Main-Evaluator to not fully replace the main Evaluator but to add
        a new evaluation step.
        :param solution: the solution to rate
        :returns: extended ration of the solution
        """
        pass


class EvaluatorModel(implements(Evaluator)):
    """
    Wrapper for an domain evaluator plugins. Calculates the penalty for violating constraints.
    """

    NOT_VALID = -9999999999
    THRESHOLD = 0

    def __init__(self, storage, constraint_schedule=None, evaluator_plugins=None, input_list=None, blacklist=[]):
        self._storage = storage
        self.__constraint_schedule = constraint_schedule
        self._input_list = input_list
        self.__evaluator_plugins = evaluator_plugins
        self._blacklist = blacklist
        self.__threshold = EvaluatorModel.THRESHOLD

    @overrides
    def get_storage(self):
        """
        Override
        """
        return self._storage

    @overrides
    def eval(self, solution, check_valid=True):
        """
        Override, see parent method
        """
        fitness = 0

        if check_valid and not self._perform_constraint_check(solution):
            return EvaluatorModel.NOT_VALID

        if self.__evaluator_plugins is not None:
            for i in range(len(self.__evaluator_plugins)):
                fitness = self.__evaluator_plugins[i].eval_step(solution, fitness)
        return fitness

    def __merge_to(self, solution, constraint_schedule):
        for i in range(solution.size()):
            if constraint_schedule.get_slot(i) is not None:
                solution.slots[i] = constraint_schedule.get_slot(i)
        return solution

    def _perform_constraint_check(self, solution):
        """
        Performs a constraint check.

        :returns: true if valid, false otherwise
        """
        input_list = np.zeros(solution.size()) if self._input_list is None else self._input_list
        simulator = ConstraintCheckingScheduleSimulator(self._storage, input_list, self._blacklist)
        return simulator.simulate(solution.slots)

    @overrides
    def acceptable(self, local_fitness):
        """
        Override
        """
        return local_fitness - self.threshold() > 0

    @overrides
    def threshold(self):
        """
        Override
        """
        return self._storage.operating_cost + self.__threshold

    def update_threshold(self, new_threshold):
        self.__threshold = new_threshold

    @overrides
    def get_blocked_schedule(self):
        """
        Override
        """
        return self.__constraint_schedule

    @overrides
    def set_input_capacity_list(self, input_list):
        """
        Override
        """
        self._input_list = input_list

    @overrides
    def get_input_capacity_list(self):
        """
        Override
        """
        return self._input_list

    @overrides
    def blacklist(self):
        """
        Override
        """
        return self._blacklist


class SelfDischargeEvaluatorPlugin(implements(EvaluatorPlugin)):

    def __init__(self, storage, energy_market):
        self.__energy_market = energy_market
        self.__storage = storage

    @overrides
    def eval_step(self, solution, prior_rating):
        simulator = SelfDischargeStorageSimulator(self.__storage, self.__energy_market)
        return prior_rating - abs(simulator.simulate(solution.slots))


class OwnConsumptionEvaluatorPlugin(implements(EvaluatorPlugin)):
    """
    Evaluates the solution due to the own consumption repsectively
    the money you would safe via consuming the own produces energy.
    """

    def __init__(self, user_market, storage, household_profile, der_profile):
        self.__energy_market = user_market
        self.__storage = storage
        self.__target_profile = (np.array(household_profile) - np.array(der_profile)).clip(min=0)

    @overrides
    def eval_step(self, solution, prior_rating):
        energy_strategy_tupel_list = ScheduleEnergySimulator(self.__storage).simulate(solution.slots)
        own_discharge_profile = np.array([tupel[0] if tupel[1] == Strategy.OWN_DISCHARGE else 0 for tupel in energy_strategy_tupel_list])
        saving_array = self.__target_profile - np.abs(own_discharge_profile - self.__target_profile)
        saving_money_array = [self.__energy_market.ask(saving_array[i], i) for i in range(len(saving_array))]
        return sum(saving_money_array) + prior_rating


class PeakDemandCuttingEvaluatorPlugin(implements(EvaluatorPlugin)):
    """
    Evaluates the solution w.r.t to the cost an user has to pay
    for high demand peaks. Only calulcates the saving coming from using a
    storage in respect to the power peaks.
    """

    def __init__(self, user_market, storage, household_profile, max_power_cost_mul=0.085):
        self.__houshold_profile = np.array(household_profile, dtype=np.int32)
        self.__user_market = user_market
        self.__storage = storage
        self.__max_power_cost_mul = max_power_cost_mul

    @overrides
    def eval_step(self, solution, prior_rating):
        energy_strategy_tupel_list = ScheduleEnergySimulator(self.__storage).simulate(solution.slots)
        storage_demand_profile = np.array([- tupel[0] if tupel[1] == Strategy.OWN_DISCHARGE else (tupel[0] if tupel[1].is_charge else 0) for tupel in energy_strategy_tupel_list], dtype=np.int32)
        demand_forecast = storage_demand_profile + self.__houshold_profile
        buy_add_cost = sum([self.__user_market.ask(max(0, storage_demand_profile[i]), i) for i in range(len(storage_demand_profile))])
        return (self.__houshold_profile.max() - demand_forecast.max()) * self.__max_power_cost_mul - buy_add_cost + prior_rating


class SpreadingEvaluatorPlugin(implements(EvaluatorPlugin)):

    def __init__(self, energy_market, storage):
        self.__energy_market = energy_market
        self.__storage = storage

    @overrides
    def eval_step(self, solution, prior_rating):
        copy_storage = copy.copy(self.__storage)

        value = 0
        for i in range(solution.size()):
            slot = solution.get_slot(i)
            energy = copy_storage.apply_load(slot.load_state)
            interval_num = int(slot.end / 15) - 1
            market_value = {
                Strategy.BUY  : - self.__energy_market.ask(energy, interval_num),
                Strategy.SELL : self.__energy_market.offer(energy, interval_num)
            }.get(slot.strategy, 0)
            value = value + market_value
        return value + prior_rating


class SpreadingEvaluator(EvaluatorModel):
    """
    Convenience Class for evaluation only with the objective spreading
    """

    def __init__(self, energy_market, storage, constraint_schedule=None, blacklist=[]):
        super().__init__(storage, evaluator_plugins=[SpreadingEvaluatorPlugin(energy_market, storage)], constraint_schedule=constraint_schedule, blacklist=blacklist)


class OwnConsumptionEvaluator(EvaluatorModel):
    """
    Convenience Class for evaluation only with the objective own consumption
    """

    def __init__(self, energy_market, storage, household_profile, der_profile, input_list=None, blacklist=[]):
        super().__init__(storage, evaluator_plugins=[OwnConsumptionEvaluatorPlugin(energy_market, storage, household_profile, der_profile)],
                                  input_list=input_list, blacklist=blacklist)


class PeakDemandCuttingEvaluator(EvaluatorModel):
    """
    Convenience Class for evaluation only with the objective peak demand cutting
    """

    def __init__(self, user_market, storage, household_profile, blacklist=[]):
        super().__init__(storage, evaluator_plugins=[PeakDemandCuttingEvaluatorPlugin(user_market, storage, household_profile)],
                                  blacklist=blacklist)