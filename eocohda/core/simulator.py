"""
Simulator to deal with energy_storages.
"""
from abc import ABC, abstractmethod
import copy

from overrides import overrides

from eocohda.core.schedule import Slot, Strategy

def execute_slot(energy_storage, energy_strategy_tupel):
    if energy_strategy_tupel[1].is_charge:
        energy_storage.charge(energy_strategy_tupel[0])
    else:
        energy_storage.discharge(energy_strategy_tupel[0])

class StorageSimulator(ABC):
    """
    Abstract ScheduleSimulator for an EnergyStorage. Simulates
    a whole schedule and calculates a specific value while doing this.
    Especially usefull to check constraints or calculate statistical values.
    """

    def __init__(self, energy_storage):
        self._energy_storage = copy.copy(energy_storage)

    def simulate(self, value_array):
        """
        Simulates the schedule on the energy_storage of this evaluator.

        :returns: the result value
        """
        # PRE-HOOK
        self._pre_simulate()

        for i in range(len(value_array)):
            value = value_array[i]
            new_value = self._simulate_intern(self._energy_storage, value)

            # ON-HOOK
            self._on_simulate(value, new_value, i)

        # POST-HOOK
        return self._post_simulate()

    @abstractmethod
    def _simulate_intern(self, energy_storage, value):
        """
        Processes a slot

        :param energy_storage: the energy_storage, which shall process the slot
        :param slot: slot to be processed
        :returns: the new load of the storage
        """
        pass

    @abstractmethod
    def _pre_simulate(self):
        """
        Invoked before simulation starts.
        """
        pass

    @abstractmethod
    def _post_simulate(self):
        """
        Invoked after simulation finishes.
        """
        pass

    @abstractmethod
    def _on_simulate(self, value, new_value, i):
        """
        Invoked after every simulation step.

        :param value: value which was processed
        :param new_value: value after applying to storage
        :param i: number of simulation step
        """
        pass

class StorageScheduleSimulator(StorageSimulator):
    """
    ScheduleSimulator for an EnergyStorage. Simulates
    a whole schedule and calculates a specific value while doing this.
    Especially usefull to check constraints or calculate statistical values.
    """

    def __init__(self, energy_storage):
        super().__init__(energy_storage)

    def _simulate_intern(self, energy_storage, slot):
        """
        Processes a slot

        :param energy_storage: the energy_storage, which shall process the slot
        :param slot: slot to be processed
        :returns: the new load of the storage
        """
        return energy_storage.apply_load(slot.load_state)


class StorageEnergySimulator(StorageSimulator):
    """
    Simulates a list of energy values
    """

    def __init__(self, energy_storage):
        super().__init__(energy_storage)

    def _simulate_intern(self, energy_storage, energy_strategy_tupel):
        """
        Processes a slot

        :param energy_storage: the energy_storage, which shall process the slot
        :param slot: slot to be processed
        :returns: the new load of the storage
        """
        execute_slot(energy_storage, energy_strategy_tupel)
        return energy_storage.load


class LoadStateEnergySimulator(StorageEnergySimulator):
    """
    Simulates a list of energy-strategy tupels
    """

    @overrides
    def _pre_simulate(self):
        self.__slots = []

    @overrides
    def _post_simulate(self):
        return self.__slots

    @overrides
    def _on_simulate(self, value, new_value, i):
        self.__slots.append(Slot(new_value, len(self.__slots) * 15,
                                 (len(self.__slots) + 1) * 15, value[1]))


class ScheduleEnergySimulator(StorageScheduleSimulator):
    """
    Implements a StorageScheduleSimulator. This simulator records the energy after each slot
    simulated.
    """

    @overrides
    def _pre_simulate(self):
        """
        Override
        """
        self.__energy = []

    @overrides
    def _post_simulate(self):
        """
        Override
        """
        return self.__energy

    @overrides
    def _on_simulate(self, value, new_value, i):
        """
        Override
        """
        self.__energy.append((new_value, value.strategy))


class SelfDischargeStorageSimulator(StorageScheduleSimulator):
    """
    Implements a StorageScheduleSimulator. The reason for this Simulator is
    to calculate the economic loss via self-discharge.
    """

    def __init__(self, energy_storage, energy_market):
        super().__init__(energy_storage)
        self.__energy_market = energy_market

    @overrides
    def _pre_simulate(self):
        """
        Override
        """
        self.__copy_storage = copy.copy(self._energy_storage)
        self.__copy_storage.set_self_discharge(0)
        self.__delta = 0
        self.__loss = 0

    @overrides
    def _post_simulate(self):
        """
        Override
        """
        return self.__loss

    @overrides
    def _on_simulate(self, value, new_value, i):
        """
        Override
        """
        slot = value
        energy = new_value
        execute_slot(self.__copy_storage, (energy, slot.strategy))
        interval_num = int(slot.end / 15) - 1
        self.__loss += self.__energy_market.offer((self.__copy_storage.load - self.__delta - self._energy_storage.load) * self._energy_storage.capacity, interval_num)
        self.__delta = self.__copy_storage.load - self._energy_storage.load


class ConstraintCheckingScheduleSimulator(StorageScheduleSimulator):
    """
    Implements a StorageScheduleSimulator. In every step this simulator checks whether
    the constraints are fullfiled.
    """

    COMPARE_TOLERANCE = 0.00001

    def __init__(self, energy_storage, input_list, blacklist):
        super().__init__(energy_storage)
        self._input_list = input_list
        self._blacklist = [] if blacklist is None else blacklist

    @overrides
    def _pre_simulate(self):
        """
        Override
        """
        self._valid = True
        self._counter = 0

    @overrides
    def _post_simulate(self):
        """
        Override
        """
        return self._valid

    @overrides
    def _on_simulate(self, value, new_value, i):
        """
        Override
        """
        slot = value
        energy = new_value
        if not self._valid:
            return

        abs_energy = abs(energy)
        self._valid = self._valid \
                      and 0 <= self._energy_storage.load <= 1 \
                      and ((not slot.strategy.is_charge) \
                            or abs_energy <=
                             self._energy_storage.max_charge_at(i) +
                            ConstraintCheckingScheduleSimulator.COMPARE_TOLERANCE) \
                      and (slot.strategy.is_charge \
                            or abs_energy <=
                            self._energy_storage.max_discharge_at(i) +
                            ConstraintCheckingScheduleSimulator.COMPARE_TOLERANCE) \

        if not self._valid:
            #print("Solution not valid because of too high energy output!")
            #print("Slot-Strategy: %s, but output %s with max discharge %s and max charge %s!\n" %  (str(slot.strategy), str(energy), str(self._energy_storage.max_discharge), str(self._energy_storage.max_charge)))
            pass

        self._valid = self._valid \
                      and abs_energy >= 0 \
                      and ((slot.strategy != Strategy.CHARGE) \
                           or abs_energy <= self._input_list[self._counter] + ConstraintCheckingScheduleSimulator.COMPARE_TOLERANCE) \
                      and ((not slot.strategy.is_charge) \
                            or energy >= 0) \
                      and (slot.strategy.is_charge \
                             or energy <= 0) \
                        and slot.strategy not in self._blacklist

        if (not self._valid):
            #print("Solution not valid because of wrong strategy or illegitim charging!")
            #print("Slot-Strategy: %s, but output %s! (LoadState: %s)\n" %  (str(slot.strategy), str(energy), str(slot.load_state)))
            pass

        self._counter += 1

    @overrides
    def _simulate_intern(self, energy_storage, slot):
        """
        Override
        """
        return energy_storage.apply_load(slot.load_state, lambda x: x)