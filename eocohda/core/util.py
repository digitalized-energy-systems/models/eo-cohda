"""
Some Utility classes and methods for the main-packages.
"""
from abc import ABC, abstractmethod
import copy
import math

from overrides import overrides
import numpy as np
import pickle

from eocohda.core.schedule import Strategy
from eocohda.core.simulator import ScheduleEnergySimulator

def convert_to_isaac(storage, eocohda_schedule):
    """
    Convert Schedule-Objekt to isaac-schedule
    :param eocohda_schedule: Schedule Object
    """
    energy_strategy_tupel_list = ScheduleEnergySimulator(storage).simulate(eocohda_schedule.slots)
    return np.array(list(map(lambda tupel: \
        (1 if tupel[1] == Strategy.DISCHARGE else 0) * tupel[0], energy_strategy_tupel_list)))

def read_profile(file):
    """
    Read the target profile-file and parse it into a simple energy-list

    :param file: file to be read
    :returns: energy-profile-list
    """
    profile_data = []
    with open(file) as file_handle:
        for line in file_handle:
            profile_data.append(int(line))
    return profile_data

def read_solutions(file):
    """
    Read solutions of the algorithm from a file

    :param file: the file to be read
    """
    with open(file, "rb") as file_handle:
        return pickle.load(file_handle)

def write_solutions(solutions, file):
    """
    Writes the solutions array to a file using pickle

    :param solutions:
    """
    with open(file, "wb") as file_handle:
        pickle.dump(solutions, file_handle)

def sigmoid(x, a=1, c=0):
    """
    Sigmoid-Function with the options to scale or translate x
    :param a: scale
    :param c: translation
    """
    return 1 / (1 + math.e ** (-a * (x - c)))
